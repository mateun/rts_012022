## Cursor rendering

A cursor needs to be rendered to show the user where buildings can be placed. 

The cursor is red when building is not allowed and it is green if the building is allowed. 

The cursor shall be rendered on the terrain.
It shall be unlit to be better visible. 

