#version 430

layout(binding = 0) uniform sampler2D diffuseTexture;
layout(binding = 1) uniform sampler2D shadowDepthTexture;

layout(location = 39) uniform bool isLit = true;
layout(location = 40) uniform bool useTexture = true;
layout(location = 41) uniform vec4 staticColor = vec4(1, 1, 1,1);

in VSOut {
    vec2 fs_uv;
    vec3 N;
    vec3 L;
    vec3 V;
    vec4 fs_light_pos;
} fs_in;

// Material properties
uniform vec3 diffuse_albedo = vec3(0.8, 0.8, 0.8);
uniform vec3 specular_albedo = vec3(0.1);
uniform float specular_power = 8;

out vec4 color;


bool isInShadow() {
    vec4 fpos_ndc = fs_in.fs_light_pos * 0.5 + 0.5;
    fpos_ndc.xyz /= fs_in.fs_light_pos.w;
    float shadow_map_z = texture(shadowDepthTexture, fpos_ndc.xy).r;
    return shadow_map_z < (fpos_ndc.z - 0.0005);
}


void main() {

    if (isLit) {
        vec3 N = normalize(fs_in.N);
        vec3 L = normalize(fs_in.L);
        vec3 V = normalize(fs_in.V);
        vec3 R = reflect(-L, N);
        float diffuseTerm = max(dot(N, L),0.2);
        vec3 diffuse = staticColor.xyz;

        if (useTexture) {
            diffuse = diffuseTerm * texture(diffuseTexture, fs_in.fs_uv).xyz;
        } else {
            diffuse = diffuseTerm * staticColor.rgb;
        }

        if (isInShadow()) {
            color = vec4(diffuse, 1.0) * 0.35;
        } else {
            vec3 specular = pow(max(dot(R, V), 0.0), specular_power) * specular_albedo;
            color = vec4(diffuse + specular, 1.0);
        }
    } else {
        if (useTexture) {
            color = texture(diffuseTexture, fs_in.fs_uv);
        } else {
            color = staticColor;
        }


    }


}