#version 430
layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 3) in mat4 instanceWorldMatrix;

layout(location = 0) uniform mat4 model;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 proj;

layout(location = 10) uniform bool tile;
layout(location = 11) uniform float tileOffsetLeft;
layout(location = 12) uniform float tileOffsetBottom;
layout(location = 13) uniform float tileWidth;
layout(location = 14) uniform float tileHeight;
layout(location = 15) uniform bool flipUV = true;
layout(location = 16) uniform bool instancedRendering = false;

uniform mat4 lightView;
uniform mat4 lightProj;


out VSOut {
    vec2 fs_uv;
    vec3 N;
    vec3 L;
    vec3 V;
    vec4 fs_light_pos;

} vsout;

uniform vec3 directionalLightPos = vec3(63, 200, 160);

void tiledRendering() {

    if (tile) {
        if (uv.x == 0) {
            vsout.fs_uv.x = tileOffsetLeft;
        }
        if (uv.x == 1) {
            vsout.fs_uv.x = tileOffsetLeft + tileWidth;
        }

        if (uv.y == 0) {
            vsout.fs_uv.y = 1 - tileOffsetBottom;
        }

        if (uv.y == 1) {
            vsout.fs_uv.y = 1 - (tileOffsetBottom + tileHeight);
        }

    } else {


    }
}

void main() {

    mat4 modelMatrix = model;

    if (instancedRendering) {
        modelMatrix = instanceWorldMatrix;
    }

    // model space position
    vec4 P =  modelMatrix * vec4(pos, 1);

    // Normal in view space
    vsout.N = mat3(modelMatrix) * normal;


    // Light vector
    // This "bug" here leads to a quite interesting
    // falloff effect in that things get darker
    // the farther away they are from the light.
    //vsout.L = directionalLightPos - P.xyz;
    vsout.L = directionalLightPos;

    // View vector
    vsout.V = -P.xyz;

    // TODO Get rid of this?!
    //tiledRendering();
    vsout.fs_uv = uv;
    if (flipUV) {
        vsout.fs_uv.y = 1- uv.y;
    }

    // Fragment position in light space
    // for shadow calcuation in fragment shader
    vsout.fs_light_pos = lightProj * lightView * modelMatrix * vec4(pos, 1);
    gl_Position  = proj * view * modelMatrix * vec4(pos, 1);




}