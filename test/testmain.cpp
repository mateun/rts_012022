//
// Created by mgrus on 17/01/2022.
//
#include <stdio.h>
#include "../lib/Shader.h"
#include "../lib/Window.h"
#include "../lib/GLContext.h"
#include "../lib/Texture.h"
#include "../lib/stb_image.h"
#include "../lib/core/FileUtils.h"
#include <SDL.h>
#include <cassert>


#define TEST_START(name) \
    printf(">>>>>>>>>>>>>>> test start: %s --------------------------------\n", #name);


#define TEST_END  \
    printf(">>>>>>>>>>>>>>> test end ---------------------------------- \n");

std::string getVertexShaderSource() {
    return "#version 430\n"

           "layout(location = 0) in vec3 pos;\n"
           "void main() { \n" \
           "gl_Position = vec4(pos, 1); \n"

           "}\n"
           ;
}

void testShader() {
    kx::Shader shader(getVertexShaderSource(),
                      kx::ShaderType::VERTEX);
    assert(shader.getHandle() != 0);
}

void testTexture() {
    kx::Texture tex(256, 256);
    kx::Texture texQuadratic(256);

    std::string basePath = SDL_GetBasePath();
    int imageChannels, w, h;
    uint8_t* imgBytes = stbi_load(
            (basePath + "../assets/textures/frogger1.bmp").c_str(), &w, &h,
            &imageChannels,
            4);
    kx::Texture texPixels(w, h, imgBytes);

}

void testShaderBuild() {
    kx::ShaderBuilder vsBuilder(kx::ShaderType::VERTEX);
    kx::Shader vs = vsBuilder.attributePosition()->attributeUV()->build();
}

void testFileRead() {
    TEST_START(fileRead)
    kx::FileUtils fileUtils;
    std::string vertexShaderContents = fileUtils.readFile("../test/resources/test_vertex_shader.glsl");
    printf("vertex shader contents: %s\n", vertexShaderContents.c_str() );
    TEST_END

}

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

int main(int argc, char** args) {
    kx::WindowGL win(800, 600, false);
    win.init();
    kx::GLContext ctx(&win);
    testShader();
    testTexture();
    testShaderBuild();
    testFileRead();
    return 0;
}
