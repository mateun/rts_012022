//
// Created by mgrus on 19/02/2022.
//
#include "../lib/rtstest.h"
#include "../lib/Framebuffer.h"
#include "../lib/Shader.h"
#include "../lib/Program.h"
#include "../lib/core/FileUtils.h"
#include "../lib/VertexArrayObject.h"
#include "../lib/VertexBuffer.h"
#include "../lib/Camera.h"
#include "../lib/Text.h"
#include "../lib/Renderer.h"
#include "../lib/MeshGrid.h"
#include "../lib/DebugGrid.h"
#include "../lib/ModelImporter.h"
#include "../lib/TerrainDescription.h"
#include "../lib/PathNode.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL_filesystem.h>
#include <algorithm>
#include <Windows.h>

static LARGE_INTEGER timerStart;
static LARGE_INTEGER timerEnd;
static int timerDiff;
static LARGE_INTEGER freq;

#define TSTART QueryPerformanceCounter(&timerStart);
#define TEND QueryPerformanceCounter(&timerEnd); \
    timerDiff = timerEnd.QuadPart - timerStart.QuadPart;

static const int BUILD = 0;
static const int HOUSE = 1;
static const int BARRACK = 2;

struct UI {
    kx::Text* buildButtonText = nullptr;
    kx::Text* houseButtonText = nullptr;
    kx::Text* barrackButtonText = nullptr;
    game_state* gameState = nullptr;
    std::vector<kx::WinEvent> frameEvents;
    kx::MouseState *mouseState = nullptr;

    kx::Text* textByType(int type) {
        switch (type) {
            case BUILD: return buildButtonText;
            case HOUSE: return houseButtonText;
            case BARRACK: return barrackButtonText;
            default: return nullptr;
        }
    }


};

static UI* ui = nullptr;
static bool uiInitialized = false;

void updateUI(std::vector<kx::WinEvent> frameEvents, kx::MouseState* mouseState) {
    ui->frameEvents = frameEvents;
    ui->mouseState = mouseState;
}

void initUI(game_state* gameState) {
    if (uiInitialized) return;

    ui = new UI();
    ui->buildButtonText = new kx::Text("Build");
    ui->houseButtonText = new kx::Text("House");
    ui->barrackButtonText = new kx::Text("Barrack");
    ui->gameState = gameState;

    uiInitialized = true;
}

bool hoverButton(const int type, int x, int y, bool armed = false) {
    kx::Text* text = ui->textByType(type);
    text->setScreenPosition(x, y);

    int buttonWidth = std::max<int>(text->getScreenDimensions().x + 8, 80) ;
    int buttonHeight = text->getScreenDimensions().y - 12;
    int buttonLeft = (x + buttonWidth/4);
    int buttonRight = buttonLeft + buttonWidth;
    int buttonTop = (y + buttonHeight / 2) + 8;
    int buttonBottom = buttonTop + buttonHeight;



    int mouseY = ui->gameState->screenHeight - ui->mouseState->mouseCoords.y;
    bool isOver =(ui->mouseState->mouseCoords.x >= buttonLeft - buttonWidth/2 && ui->mouseState->mouseCoords.x <= buttonRight
                  && mouseY >= buttonTop - buttonHeight/2 && mouseY <= buttonBottom - buttonHeight/2);

    kx::RenderData rd = {};

    // Override the color if we are armed
    if (armed) {
        rd.color = isOver ? glm::vec4{0.5, 0.5, 0.6, 1} : glm::vec4 {0.2, 0.4, .6, 0.8};
    } else {
        rd.color = isOver ? glm::vec4{0.8, 0.7, 0.7, 1} : glm::vec4{0.2, 0.2, 0.9, 1};
    }



    rd.diffuseTexture = nullptr;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = ui->gameState->defaultProgram;
    rd.cam = ui->gameState->uiCamera;
    rd.transparent = false;
    rd.vao = ui->gameState->quadVAO;
    rd.lit = false;
    rd.world = glm::translate(glm::mat4(1), {buttonLeft , buttonTop , -0.2}) * glm::scale(glm::mat4(1), {buttonWidth, buttonHeight, 1});
    kx::Renderer renderer;
    renderer.renderSingleColor(rd);
    renderer.renderTextured(text->getRenderData(ui->gameState->defaultProgram, ui->gameState->uiCamera));

    return isOver;
}

bool doButton(const int type, int x, int y, bool armed = false)
{

    bool lmbClicked = false;
    for (auto e : ui->frameEvents) {
        if (e.type == kx::WinEventType::MOUSEUP) {
            if (e.key == "LEFT") {
                lmbClicked = true; break;
            }
        }
    }

    return hoverButton(type, x, y, armed) && lmbClicked;

}


std::string getVertexShaderSource() {
    return kx::FileUtils::readFile("../assets/shaders/universal_vs.glsl");
}

std::string getFragmentShaderSource() {
    return kx::FileUtils::readFile("../assets/shaders/universal_fs.glsl");
}


kx::VertexArrayObject* createQuadVAO() {
    kx::VertexArrayObject* vao = new kx::VertexArrayObject();
    vao->bind();
    vao->setNumberOfIndices(6);
    std::vector<float> positions = {
            -0.5, -0.5, 0,
            0.5, -0.5, 0,
            0.5, 0.5, 0,
            -0.5, 0.5, 0

    };

    std::vector<float> normals = {
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
    };

    std::vector<float> uvs = {
            0, 0,
            1, 0,
            1, 1,
            0, 1
    };

    std::vector<uint32_t> indices = {
            0,1,2,
            0,2, 3
    };

    kx::PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    kx::UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    kx::NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    kx::IndexBuffer indexBuffer(indices);

    vao->unbind();
    return vao;
}

kx::RenderData* game_state::getCursorRenderData() {
    if (placementState == Allowed) {
        cursorRenderData->color = {0.2, 0.8, 0.1, 1};
    } else {
        cursorRenderData->color = {0.8, 0.1, 0.1, 1};
    }
    return cursorRenderData;
}

void game_state::tickUnits(float d) {
    for (int i = 0; i < unitsCount; i++) {
        unitsArray[i].tick(d, this);
    }
}


kx::RenderData Unit::getRenderData(game_state *gameState) {
    kx::RenderData rd = {};
    rd.color = {0.5, 0.6, 0.7, 1};
    rd.diffuseTexture = nullptr;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = gameState->defaultProgram;
    rd.cam = gameState->defaultCamera;
    rd.transparent = false;
    rd.vao = model;
    rd.lit = true;
    rd.directionalLightPos = gameState->sunPos;
    rd.lightcam = gameState->lightCam;

    glm::vec2 worldPosition = {0, 0};
    if (position.x < 0) {
        worldPosition.x = position.x + 0.5;
    } else {
        worldPosition.x = position.x - 0.5;
    }
    if (position.y < 0) {
        worldPosition.y = position.y + 0.5;
    } else {
        worldPosition.y = position.y - 0.5;
    }

    rd.world = glm::translate(glm::mat4(1), {worldPosition.x, 0, worldPosition.y}) * glm::scale(glm::mat4(1), {1.5, 1.5, 1.5});

    return rd;
}


#include <Windows.h>
Unit::Unit(game_state* gs) {
    printf("unit allocated\n");
}


kx::RenderData Building::getRenderData(game_state *gameState) {
    kx::RenderData rd = {};
    rd.color = {0.5, 0.6, 0.7, 1};
    rd.diffuseTexture = gameState->lowpolyAtlas;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = gameState->defaultProgram;
    rd.cam = gameState->defaultCamera;
    rd.transparent = false;
    rd.vao = model;
    rd.lit = true;
    rd.directionalLightPos = gameState->sunPos;
    rd.lightcam = gameState->lightCam;

    glm::vec2 worldPosition = {0, 0};
    if (tilePosition.x < 0) {
        worldPosition.x = tilePosition.x + 0.5;
    } else {
        worldPosition.x = tilePosition.x - 0.5;
    }
    if (tilePosition.y < 0) {
        worldPosition.y = tilePosition.y + 0.5;
    } else {
        worldPosition.y = tilePosition.y - 0.5;
    }


    rd.world = glm::translate(glm::mat4(1), {worldPosition.x, 0.25, worldPosition.y}) * glm::scale(glm::mat4(1), {0.25, 0.25, 0.25});

    return rd;
}

int maxPaths = 100;
kx::PathCalculation* activePaths = nullptr;
int pathCount = 0;
int pathIndex = 0;
kx::PathCalculation* createPathCalc(game_state* gs) {
    if (!activePaths) {
//        activePaths = (kx::PathCalculation*) malloc(maxPaths * sizeof(kx::PathCalculation));
        activePaths = new kx::PathCalculation[maxPaths];
//        memset(activePaths, 0, maxPaths * sizeof(kx::PathCalculation));
    }

    if (pathCount < maxPaths  - 1) {
        // Flat memory variant
        //kx::PathCalculation* createdPath = activePaths + pathIndex;

        // The conservative variant
        kx::PathCalculation* createdPath = activePaths + pathCount;
        createdPath->gameState = gs;
        pathCount++;
        //pathIndex += sizeof(kx::PathCalculation);
        return createdPath;
    }

    return nullptr;



}

void Unit::tick(float time, game_state* gameState) {
    elapsedTime += time;
    if (elapsedTime >= thinkTime) {

        if (!hasTarget) {
            // finding the closest resourceNodeArray
            {

                float closestDist = 999999;
                int closestNodeIndex = -1;
                for (int i = 0; i < gameState->numberOfResourceNodes; i++) {
                    float dist = glm::distance(this->position, gameState->resourceNodeArray[i].position);
                    if (dist < closestDist) {
                        closestDist = dist;
                        closestNodeIndex = i;
                    }
                }

                auto rn = gameState->resourceNodeArray[closestNodeIndex];
                this->targetTile = rn.position;
                hasTarget = true;
            }
        }
        if(!alreadyOnPath) {
            alreadyOnPath = true;
            elapsedTime = 0;
            pathCalculation = createPathCalc(gameState);
            if (pathCalculation) {
                pathCalculation->setStartPosition(position);
                pathCalculation->setEndPosition(targetTile);
                pathCalculation->setTerrainDescriptions(gameState->terrainDescriptions, gameState->terrainChunkDim * gameState->terrainChunkDim);
                pathCalculation->init();
            } else {
                printf("currently no path available!");
            }

            gameState->activePathCount++;
        }
    }


    if (alreadyOnPath && pathCalculation) {
        if (pathCalculation->getCalculationState() == kx::PathCalculationState::InProgress) {
            pathCalculation->step();
        }
        else if (pathCalculation->getCalculationState() == kx::PathCalculationState::FinishedOK) {
            std::vector<kx::PathNode*> p  = pathCalculation->getPath();
            pathWalkStep++;
            auto pn = pathCalculation->getPath()[pathWalkStep];
            this->position = pn->getTile()->position;

        }
        else {
            //printf("state for path: %d\n", pathCalculation->getCalculationState());
            //free(pathCalculation);
            pathCalculation = nullptr;
            alreadyOnPath = false;
            elapsedTime = 0;
            gameState->activePathCount--;
            pathCount--;
            pathIndex -= sizeof(kx::PathCalculation);
        }


    }
}


static Unit* unitsArrayGlobal = nullptr;
int unitIndex =0;
Unit* createNewUnit(game_state* gs, glm::vec2 pos) {
    Unit* result = nullptr;
    if (!unitsArrayGlobal) {
        unitsArrayGlobal = (Unit*) malloc(sizeof(Unit) * gs->maxUnitsAllowed);
        memset(unitsArrayGlobal, 0, sizeof(Unit) * gs->maxUnitsAllowed);
        gs->unitsArray = unitsArrayGlobal;
    }
    if (gs->unitsCount < gs->maxUnitsAllowed) {
        auto u = gs->unitsArray[gs->unitsCount];
        u.position = pos;
        result = &gs->unitsArray[gs->unitsCount];

        gs->unitsCount++;

    }

    return result;
}

void tickBuildings(float time, game_state* gameState) {
    auto buildings = gameState->buildingsArray;
    for (int i = 0; i < gameState->numberOfBuildings; i++) {
        buildings[i].elapsedTime += time;
        if (buildings[i].elapsedTime >= buildings[i].tickIntervalInSeconds && buildings[i].unitsSpawned < buildings[i].maxUnits) {
            buildings[i].unitsSpawned++;
            buildings[i].elapsedTime = 0;
            {
                TSTART
                auto unit = createNewUnit(gameState, buildings[i].tilePosition + glm::vec2{1, 1});
                unit->type = UnitType::Soldier;
                unit->position = buildings[i].tilePosition + glm::vec2(1, 1);
                unit->thinkTime = 10;
                unit->elapsedTime = 0;
                unit->model = gameState->soldierModel;
                unit->alreadyOnPath = false;
                TEND

            }
        }
    }

}

void Building::tick(float time, game_state* gameState) {
    elapsedTime += time;
    if (elapsedTime >= tickIntervalInSeconds && unitsSpawned < maxUnits) {
        unitsSpawned++;
        elapsedTime = 0;
        // Spawn a unit on a neighbouring tile
        // or on the next tile where there is place?
        // Prefer a road?
        {
//            auto unit = new Unit(gameState);
//            unit->type = UnitType::Soldier;
//            unit->position =  tilePosition + glm::vec2(1, 1);
//            unit->model = gameState->soldierModel;
//            gameState->units.push_back(unit);
        }
    }
}

static kx::RenderData getMiniMapRenderData(game_state* gameState) {
    kx::RenderData trd;
    trd.diffuseTexture = gameState->miniMap;
    trd.primitiveType = GL_TRIANGLES;
    trd.prog = gameState->defaultProgram;
    trd.cam = gameState->uiCamera;
    trd.transparent = true;
    trd.vao = gameState->quadVAO;
    trd.lit = false;
    trd.world = glm::translate(glm::mat4(1), {138, 138, -0.5}) * glm::scale(glm::mat4(1), {286, 286  , 1});
    return trd;
}

static kx::RenderData getTerrainDebugRenderData(game_state* gameState) {
    kx::RenderData trd;
    trd.diffuseTexture = gameState->terrainDebugTexture;
    trd.primitiveType = GL_TRIANGLES;
    trd.prog = gameState->defaultProgram;
    kx::Camera* orthoCam2 = new kx::OrthoCamera({0, 0, 5}, {0, 100, 0, 100 , 0.1, 100});
    orthoCam2->setPosition({0, 0, 5});
    orthoCam2->setLookAt({0, 0, 0});
    trd.cam = orthoCam2;
    trd.transparent = false;
    trd.vao = gameState->quadVAO;
    trd.lit = false;
    trd.world = glm::translate(glm::mat4(1), {gameState->miniMapPos.x, gameState->miniMapPos.y, 0}) * glm::scale(glm::mat4(1), {100.0 * gameState->miniMapZoom, 100 * gameState->miniMapZoom  , 1});
    return trd;
}

void init(game_memory* gameMemory, game_state* gameState, kx::Window* win) {
    QueryPerformanceCounter(&freq);
    gameState->screenWidth = win->getDimension().w;
    gameState->screenHeight = win->getDimension().h;
    gameState->screenResX = win->screenResX;
    gameState->screenResY = win->screenResY;
    gameState->isFullScreen = win->isFullscreen();
    gameState->sunPos = glm::vec3{-20, 25, 10};

    kx::Shader vshader(getVertexShaderSource(), kx::ShaderType::VERTEX);
    kx::Shader fshader(getFragmentShaderSource(), kx::ShaderType::FRAGMENT);
    kx::Program* program = new kx::Program(&vshader, &fshader);
    gameState->defaultProgram = program;

    gameState->quadVAO = createQuadVAO();

    kx::Image imgLowPoly (std::string(SDL_GetBasePath()) + "../assets/textures/lowpoly.png");
    gameState->lowpolyAtlas = new kx::Texture(&imgLowPoly);

    kx::Image imgMiniMap (std::string(SDL_GetBasePath()) + "../assets/textures/minimap_bg_512.png");
    gameState->miniMap = new kx::Texture(&imgMiniMap);


    kx::Camera* orthoCam = new kx::OrthoCamera({0, 0, 5}, {0, (float)win->getDimension().w, 0, (float)win->getDimension().h, 0.1, 100});
    orthoCam->setPosition({0, 0, 5});
    orthoCam->setLookAt({0, 0, 0});
    gameState->uiCamera = orthoCam;

    kx::Text* hpText = new kx::Text("HP: 100", {5, gameState->screenHeight-50}, 14);
    gameState->hpText = hpText;

    gameState->resourcesText = new kx::Text("Res:", {400, gameState->screenHeight-50}, 14);
    gameState->popupInfoText = new kx::Text("-", {400, gameState->screenHeight - 50}, 14);
    gameState->unitCountText = new kx::Text("Unit#:", {5, gameState->screenHeight-100}, 14);
    gameState->activePathCountText = new kx::Text("# paths:", {5, gameState->screenHeight-140}, 14);
    kx::Text* tileUnderMouseText = new kx::Text("tile", {5, gameState->screenHeight-80}, 14);
    gameState->tileUnderMouseText = tileUnderMouseText;

    gameMemory->isInitialized = true;

    gameState->colorBuffer = new kx::ColorBuffer(gameState->screenWidth, gameState->screenHeight);
    gameState->shadowDepthBuffer = new kx::DepthBuffer(gameState->screenWidth, gameState->screenHeight);

    kx::RenderData* fullscreenRenderData = new kx::RenderData();
    fullscreenRenderData->prog = gameState->defaultProgram;
    fullscreenRenderData->vao = gameState->quadVAO;
    kx::Camera* cam = new kx::OrthoCamera({0, 0, 5}, {0, (float)gameState->screenWidth, 0, (float)gameState->screenHeight, 0.1, 200});
    cam->setPosition({0, 0, 5});
    cam->setLookAt({0, 0, 0});
    fullscreenRenderData->cam = orthoCam;
    fullscreenRenderData->diffuseTexture = gameState->colorBuffer->getColorTexture();
    fullscreenRenderData->world = glm::translate(glm::mat4(1), {gameState->screenWidth/2, gameState->screenHeight/2, -1}) *
               glm::scale(glm::mat4(1), {gameState->screenWidth, gameState->screenHeight, 1});
    fullscreenRenderData->transparent = false;
    fullscreenRenderData->lit = false;
    fullscreenRenderData->flipUV = false;
    gameState->fullScreenQuadRenderData = fullscreenRenderData;



//    glm::vec3 camPos = {0, 10, 10};
//    kx::Camera* perspectiveCam = new kx::PerspectiveCamera(camPos, {glm::radians(45.0), (float)gameState->screenWidth/(float)gameState->screenHeight, 0.1, 100});
//    perspectiveCam->setLookAt({0, 0, -2});

    gameState->camWidth = 20;
    float cw = gameState->camWidth/2;
    float ar = (float) gameState->screenWidth / (float) gameState->screenHeight;
    kx::Camera* orthoCamGameplay = new kx::OrthoCamera({0, 10, 10}, {-cw , cw, -cw /ar, cw / ar, 1, 100});
    orthoCamGameplay->setPosition({100, 12, 4});
    orthoCamGameplay->setLookAt({orthoCamGameplay->getPos().x, 0, orthoCamGameplay->getPos().z -4});

    gameState->defaultCamera = orthoCamGameplay;

    kx::Camera* lightCam = new kx::OrthoCamera({0, 10, 10}, {-10, 10, -10, 10, 1, 300});
    lightCam->setPosition(gameState->sunPos);
    lightCam->setLookAt({0, 0, 0});

    gameState->lightCam = lightCam;
    gameState->houseModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/cube1.dae");
    gameState->castleModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/castle.dae");
    gameState->barrackModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/barrack.dae");
    gameState->coalResourceModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/barrack.dae");
    gameState->stoneResourceModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/stone_resource.dae");
    gameState->workerModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/worker.dae");
    gameState->soldierModel = kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/soldier.dae");
    gameState->houseRenderData = new kx::RenderData();
    gameState->houseRenderData->color = {0.5, 0.6, 0.7, 1};
    gameState->houseRenderData->diffuseTexture = nullptr;
    gameState->houseRenderData->primitiveType = GL_TRIANGLES;
    gameState->houseRenderData->flipUV = true;
    gameState->houseRenderData->prog = gameState->defaultProgram;
    gameState->houseRenderData->cam = gameState->defaultCamera;
    gameState->houseRenderData->transparent = false;
    gameState->houseRenderData->vao = gameState->houseModel;
    gameState->houseRenderData->lit = true;
    gameState->houseRenderData->directionalLightPos = gameState->sunPos;
    gameState->houseRenderData->lightcam = lightCam;
    gameState->houseRenderData->world = glm::translate(glm::mat4(1), {0, 1, 0});
    gameState->tileUnderMouse = {-2, -2};

    gameState->cursorRenderData = new kx::RenderData();
    gameState->cursorRenderData->color = {0.8, 0.8, 0.9, 1};
    gameState->cursorRenderData->diffuseTexture = nullptr;
    gameState->cursorRenderData->primitiveType = GL_TRIANGLES;
    gameState->cursorRenderData->flipUV = true;
    gameState->cursorRenderData->prog = gameState->defaultProgram;
    gameState->cursorRenderData->cam = gameState->defaultCamera;
    gameState->cursorRenderData->transparent = false;
    gameState->cursorRenderData->vao = gameState->quadVAO;
    gameState->cursorRenderData->lit = false;
    gameState->cursorRenderData->directionalLightPos = gameState->sunPos;
    gameState->cursorRenderData->lightcam = lightCam;
    gameState->cursorRenderData->world = glm::translate(glm::mat4(1), {gameState->tileUnderMouse.x, 0.1, gameState->tileUnderMouse.y});
    gameState->undefinedOccupations.insert(gameState->undefinedOccupations.end(), {new Occupation{{-1, 1}}, new Occupation{{-1,-1}}, new Occupation{{1, 1}}, new Occupation{{1, -1}}});
    gameState->currentPopupInfo = new PopupInfo { "Info", {0, 0}};

    gameState->terrainCells = 100;
    gameState->terrainChunkDim = 5;
    int chunks = gameState->terrainChunkDim * gameState->terrainChunkDim;

    // Our terrain consists of 5x5 chunks = 25
    {
        gameState->terrainDescriptions = static_cast<kx::TerrainDescription *>(malloc(sizeof(kx::TerrainDescription) * chunks));
        gameState->meshGrids = static_cast<kx::MeshGrid*>(malloc(sizeof(kx::MeshGrid) * chunks));
        for (int chunkRow = 0; chunkRow < 5; chunkRow++) {
            for (int chunkCol = 0; chunkCol < 5; chunkCol++) {
                gameState->terrainDescriptions[chunkCol + chunkRow * gameState->terrainChunkDim] = {gameState->terrainCells};
                gameState->meshGrids[chunkCol + chunkRow * gameState->terrainChunkDim] = kx::MeshGrid(glm::vec3(chunkCol * gameState->terrainCells, 0, chunkRow * gameState->terrainCells ), 1, &gameState->terrainDescriptions[chunkCol + chunkRow * gameState->terrainChunkDim]);
            }
        }
    }
    gameState->debugGrid = new kx::DebugGrid(gameState->terrainCells * gameState->terrainChunkDim, 1);

//    gameState->terrainDescription = new kx::TerrainDescription(terrainCells);
//    gameState->terrain = new kx::MeshGrid({-terrainCells/2, 0, terrainCells/2}, 1, gameState->terrainDescription);
//    //gameState->terrain = new kx::MeshGrid({-terrainCells/2, 0, terrainCells/2}, 1, terrainCells);
//    kx::Image* terrainDebugImage = new kx::Image(gameState->terrainDescription->getDebugImage(), terrainCells, terrainCells);
//    gameState->terrainImage = terrainDebugImage;
//    gameState->terrainDebugTexture = new kx::Texture(terrainDebugImage);

//    gameState->miniMapZoom= 1;

    // We pre-generate a bunch of worldMatrices for later use,
    // so we do not need to malloc during the frame.
    auto bwms = (glm::mat4*) malloc(gameState->maxBuildingsAllowed * sizeof(glm::mat4));
    for (int i = 0; i < gameState->maxBuildingsAllowed; i++) {
        bwms[i] = glm::mat4{1};
    }
    gameState->buildingWorldMatrices = bwms;
    gameState->buildingsArray = (Building*) malloc(sizeof(Building) * gameState->maxBuildingsAllowed);

    gameState->unitWorldMatrices = new glm::mat4[gameState->maxUnitsAllowed];
    for (int i = 0; i < gameState->maxBuildingsAllowed; i++) {
        gameState->unitWorldMatrices[i] = glm::mat4{1};
    }


    // Resourcenodes
    {
        gameState->numberOfResourceNodes = 100;
        gameState->resourceNodeArray = (ResourceNode*) malloc(sizeof(ResourceNode)* gameState->numberOfResourceNodes);
        for (int i = 0; i < gameState->numberOfResourceNodes; i++) {
            gameState->resourceNodeArray[i] = {};
        }
        auto rns = (glm::mat4*) malloc(gameState->numberOfResourceNodes * sizeof(glm::mat4));
        for (int i = 0; i < gameState->numberOfResourceNodes ; i++) {
            rns[i] = glm::mat4{1};
        }
        gameState->resourceNodeWorldMatrices = rns;
        for (int i = 0;  i  < gameState->numberOfResourceNodes; i++) {
            gameState->resourceNodeArray[i].position = glm::vec2{i * 5, i * 5};
            gameState->resourceNodeArray[i].capacity = 100;
            gameState->resourceNodeArray[i].left = gameState->resourceNodeArray[i].capacity;
        }
    }

    initUI(gameState);
}


// Ray intersection
Ray createRayFromScreenCoordinates(glm::vec2 mouseCoords, kx::OrthoCamera* camera, int screenWidth, int screenHeight)
{
    Ray ray = {};
    mouseCoords.y = screenHeight - mouseCoords.y;
    float x = (2.0f * mouseCoords.x) / screenWidth - 1.0f;
    float y = (2.0f * mouseCoords.y) / screenHeight - 1.0f;
    float z = 1.0f;
    glm::vec3 ray_nds = glm::vec3(x, y, z);
    glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
    glm::vec4 ray_eye = glm::inverse(camera->getProj()) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

    glm::vec3 fn = glm::normalize(camera->getFwd());

    glm::vec3 camRight = glm::normalize(glm::cross(fn, {0, 1, 0}));
    glm::vec3 camUp = glm::normalize(glm::cross(camRight, fn));

    ray.origin = camera->getPos() + glm::normalize(camRight) * ray_eye.x + glm::normalize(camUp) * ray_eye.y;
    ray.dir = glm::normalize(camera->getFwd());

    return ray;

}

bool intersectsPlane(Ray ray, glm::vec3 planeNormal, glm::vec3 planePoint, glm::vec3* intersectionPoint)
{
    float rayPlaneOrientation = glm::dot(ray.dir, planeNormal);
    if (abs(rayPlaneOrientation) <= 0.0001f) {
        //SDL_Log("ray plane orientation too small. no collision! %f\n", rayPlaneOrientation);
        return false;
    }

    float t = (glm::dot((planePoint - ray.origin), planeNormal)) / rayPlaneOrientation;
    *intersectionPoint = ray.origin + t * ray.dir;

    return t > 0;
}

void testRayIntersection() {
    Ray ray = {{0, 100, 0}, {0, -1, 0}};
    glm::vec3 intersectionPoint;
    if (intersectsPlane(ray, {0, 1, 0}, {0, 0, 0}, &intersectionPoint)) {
        assert(intersectionPoint.x == 0 && intersectionPoint.y == 0 && intersectionPoint.z == 0);
    }

    kx::OrthoCamera orthoCamera({10, 100, 0}, {-20, 20, -20, 20, 0.1, 100});
    orthoCamera.setLookAt({10, 0, 0});
    Ray rayFromCamera = createRayFromScreenCoordinates({ 400, 300}, &orthoCamera, 800, 600);
    if (intersectsPlane(rayFromCamera, {0, 1, 0}, {0, 0, 0}, &intersectionPoint)) {
        assert(intersectionPoint.x == 10 && intersectionPoint.y == 0 && intersectionPoint.z == 0);
    }
}

// End Ray interesection

void updateHouseCostInfo(game_state* gs) {
    char buf[300];
    int wood = 0, stone = 0, gold = 0;
    for (auto c : gs->costForBuilding(BuildingType::House)) {
        if (c.resourceType == Wood) {
            wood = c.amount;
        }
        if (c.resourceType == Stone) {
            stone = c.amount;
        }
        if (c.resourceType == Gold) {
            gold = c.amount;
        }
    }
    sprintf_s(buf, 200, "Wood: %d \nStone: %d\n Gold: %d", wood, stone, gold);
    gs->currentPopupInfo->text = buf;
    gs->currentPopupInfo->position = {620 + 70, 465 - 10};
}

void updateResourceText(game_state* gs) {
    char buf[300];
    sprintf_s(buf, 200, "Wood: %d Stone: %d Gold: %d", gs->resources[Wood].amount, gs->resources[Stone].amount, gs->resources[Gold].amount);
    gs->resourcesText->setText(buf);

}

void updateArbitraryTexts(game_state* gameState, kx::MouseState* mouseState) {
    char buf[200];
    sprintf_s(buf, 200, "mouse: %d/%d", (int) mouseState->mouseCoords.x, (int) mouseState->mouseCoords.y);
    gameState->hpText->setText(buf);

    memset(buf, 0, 200);
    sprintf_s(buf, 200, "# units: %d", gameState->unitsCount);
    gameState->unitCountText->setText(buf);

    memset(buf, 0, 200);
    sprintf_s(buf, 200, "# paths: %d", gameState->activePathCount);
    gameState->activePathCountText->setText(buf);


}

void testEnoughResources() {
    game_state gs = {};

    assert(gs.hasEnoughResources({{ResourceType::Wood, 50}}));
    assert(gs.hasEnoughResources({{ResourceType::Wood, 50}, {ResourceType::Gold, 50}}));
    assert(!gs.hasEnoughResources({{ResourceType::Wood, 50}, {ResourceType::Gold, 51}}));
    assert(!gs.hasEnoughResources({{ResourceType::Wood, 250}}));
}
/**
 * Here we do some kind of unit tests before the program starts.
 * This should not go to the final version, but is handy during development,
 * so we do not need to run through a complicated sequence in the game itself.
 */
void startupTests() {
    testRayIntersection();
    testEnoughResources();
}

glm::vec2 getTileUnderMouse(game_state* gameState, kx::MouseState* ms) {
    Ray rayFromCamera = createRayFromScreenCoordinates(ms->mouseCoords, (kx::OrthoCamera*) gameState->defaultCamera, gameState->screenWidth, gameState->screenHeight);
    glm::vec3 intersectionPoint;
    if (intersectsPlane(rayFromCamera, {0, 1, 0}, {0, 0, 0}, &intersectionPoint)) {
        int tileX = intersectionPoint.x < 0 ? std::round (intersectionPoint.x - 0.5) : (std::round) (intersectionPoint.x + 0.5);
        int tileY = intersectionPoint.z < 0 ? std::round (intersectionPoint.z - 0.5) : std::round (intersectionPoint.z + 0.5);
        gameState->tileUnderMouse = glm::vec2{tileX, tileY};
        char buf[200];
        sprintf_s(buf, 200, "tile: %d/%d", tileX, tileY);
        gameState->tileUnderMouseText->setText(buf);
        return {tileX, tileY};
    }

    return {0, 0};


}

void update(float time, game_state* gs, kx::MouseState* mouseState, std::vector<kx::WinEvent> frameEvents) {

    for (auto e : frameEvents) {
        if (e.type == kx::WinEventType::EXIT) {
            endGame();
        }

        if (e.type == kx::WinEventType::KEYDOWN) {
            if (e.key == "ESCAPE") {
                endGame();
            }

            if (e.keyCode == kx::KeyCode::f11) {
                gs->renderDebugGrid = !gs->renderDebugGrid;
            }

            if (e.key == "PLUS") {
                if (gs->miniMapZoom < 50) {
                    gs->miniMapZoom *= 2;
                }
       }

            if (e.key == "MINUS") {
                if (gs->miniMapZoom > 0) {
                    gs->miniMapZoom /= 2;
                }
            }

            if (e.key == "KP_8") {
                gs->miniMapPos.y -= 2;
            }

            if (e.key == "KP_2") {
                gs->miniMapPos.y += 2;
            }

            if (e.key == "KP_6") {
                gs->miniMapPos.x -= 2;
            }

            if (e.key == "KP_4") {
                gs->miniMapPos.x += 2;
            }


        }

        if (gs->buildState == BuildState::Placement && gs->placementState == Allowed) {
            if (e.type == kx::WinEventType::MOUSEUP) {
                if (e.key == "LEFT") {
                    gs->createBuilding(gs->currentBuildingToPlace);
                    gs->buildState = BuildState::None;
                }
            }
        }

        if (gs->buildState == BuildState::Placement) {
            if (e.type == kx::WinEventType::MOUSEUP && e.key == "RIGHT") {
                gs->buildState = BuildState::None;
            }
        }

    }

    if (gs->popupInfoState == PopupInfoState::Show) {
        updateHouseCostInfo(gs);
    }
    gs->tickUnits(time);
    tickBuildings(time, gs);
    updateResourceText(gs);
    updateArbitraryTexts(gs, mouseState);
    getTileUnderMouse(gs, mouseState);
    float factorX = gs->tileUnderMouse.x < 0 ?  0.5 : -0.5;
    float factorZ = gs->tileUnderMouse.y < 0 ?  0.5 : -0.5;
    gs->cursorRenderData->world = glm::translate(glm::mat4(1), {gs->tileUnderMouse.x + factorX , 0.02, gs->tileUnderMouse.y  + factorZ}) * glm::rotate<float>(glm::mat4(1), glm::radians(-90.0), {1, 0, 0});
    updateUI(frameEvents, mouseState);
    if (gs->buildState == BuildState::Placement) {
        gs->calculatePlacementState(gs->currentBuildingToPlace);
    }


}



void initResourceNodes(game_state* gs, kx::VertexArrayObject* model) {
   
}

void testCache(game_state* gs, float ft) {

    static bool ranOnce = false;

    if (ranOnce) {
        return;
    }

    ranOnce = true;
    printf("ft: %f\n", ft);
    LARGE_INTEGER freq;
    QueryPerformanceCounter(&freq);

    LARGE_INTEGER start;
    LARGE_INTEGER end;
    int diff;

    int count = gs->maxBuildingsAllowed;
    for (int i = 0; i < count; i++) {
        gs->buildingsArray[i].elapsedTime = 0;
        gs->buildingsArray[i].unitsSpawned = 0;
        gs->buildingsArray[i].lastTick = 0;
        gs->buildingsArray[i].maxUnits = 10;
        gs->buildingsArray[i].tickIntervalInSeconds = 15 ;
        gs->buildingsArray[i].tilePosition = glm::vec2(-10 + i, 5);
        gs->numberOfBuildings++;
    }


    QueryPerformanceCounter(&start);
    tickBuildings(ft, gs);
    QueryPerformanceCounter(&end);
    diff = end.QuadPart - start.QuadPart;
    printf("tick with separate function and slightly optimized?!: (%d items) %d\n", count, diff);

}

void renderResourceNodes(game_state* gameState, kx::VertexArrayObject* model, bool shadowPass = false) {
    for (int i = 0; i < gameState->numberOfResourceNodes; i++) {

        glm::vec2 worldPosition = {0, 0};
        if (gameState->resourceNodeArray[i].position.x < 0) {
            worldPosition.x = gameState->resourceNodeArray[i].position.x + 0.5;
        } else {
            worldPosition.x = gameState->resourceNodeArray[i].position.x - 0.5;
        }
        if (gameState->resourceNodeArray[i].position.y < 0) {
            worldPosition.y = gameState->resourceNodeArray[i].position.y + 0.3;
        } else {
            worldPosition.y = gameState->resourceNodeArray[i].position.y - 0.3;
        }

        glm::mat4 mtrans = glm::translate(glm::mat4(1), {worldPosition.x, 0.5, worldPosition.y});
        glm::mat4 mscale = glm::scale(glm::mat4(1), {0.75, 0.75, 0.75});
        gameState->resourceNodeWorldMatrices[i] = mtrans * mscale;
    }

    kx::RenderData rd = {};
    rd.diffuseTexture = gameState->lowpolyAtlas;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = gameState->defaultProgram;
    rd.cam = gameState->defaultCamera;
    rd.transparent = false;
    rd.vao = model;
    rd.lit = true;
    rd.directionalLightPos = gameState->sunPos;
    rd.lightcam = gameState->lightCam;

    if (shadowPass) {
        rd.prog->setBoolUniform("useTexture", false);
    }

    kx::Renderer renderer;
    if (shadowPass) {
        renderer.renderShadowPassInstanced(rd, gameState->resourceNodeWorldMatrices, gameState->numberOfResourceNodes);
    } else {
        renderer.renderTexturedInstanced(rd, gameState->resourceNodeWorldMatrices, gameState->numberOfResourceNodes);
    }
}

void renderUnits(game_state* gameState, bool shadowPass = false) {
    if (gameState->unitsCount == 0) {
        return;
    }
    for (int i = 0; i < gameState->unitsCount; i++) {

        glm::vec2 worldPosition = {0, 0};
        if (gameState->unitsArray[i].position.x < 0) {
            worldPosition.x = gameState->unitsArray[i].position.x + 0.5;
        } else {
            worldPosition.x = gameState->unitsArray[i].position.x - 0.5;
        }
        if (gameState->unitsArray[i].position.y < 0) {
            worldPosition.y = gameState->unitsArray[i].position.y + 0.5;
        } else {
            worldPosition.y = gameState->unitsArray[i].position.y - 0.5;
        }

        glm::mat4 mtrans = glm::translate(glm::mat4(1), {worldPosition.x, 0, worldPosition.y});
        glm::mat4 mscale = glm::scale(glm::mat4(1), {1.3, 1.3, 1.3});
        gameState->unitWorldMatrices[i] = mtrans * mscale;
    }

    kx::RenderData rd = {};
    rd.diffuseTexture = gameState->lowpolyAtlas;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = gameState->defaultProgram;
    rd.cam = gameState->defaultCamera;
    rd.transparent = false;
    rd.vao = gameState->soldierModel;
    rd.lit = true;
    rd.directionalLightPos = gameState->sunPos;
    rd.lightcam = gameState->lightCam;

    if (shadowPass) {
        rd.prog->setBoolUniform("useTexture", false);
    }

    kx::Renderer renderer;
    if (shadowPass) {
        renderer.renderShadowPassInstanced(rd, gameState->unitWorldMatrices, gameState->unitsCount);
    } else {
        renderer.renderTexturedInstanced(rd, gameState->unitWorldMatrices, gameState->unitsCount);
    }
}

void renderBuildings(game_state* gameState, kx::VertexArrayObject* model, bool shadowPass = false) {
    for (int i = 0; i < gameState->numberOfBuildings; i++) {

        glm::vec2 worldPosition = {0, 0};
        if (gameState->buildingsArray[i].tilePosition.x < 0) {
            worldPosition.x = gameState->buildingsArray[i].tilePosition.x + 0.5;
        } else {
            worldPosition.x = gameState->buildingsArray[i].tilePosition.x - 0.5;
        }
        if (gameState->buildingsArray[i].tilePosition.y < 0) {
            worldPosition.y = gameState->buildingsArray[i].tilePosition.y + 0.5;
        } else {
            worldPosition.y = gameState->buildingsArray[i].tilePosition.y - 0.5;
        }

        glm::mat4 mtrans = glm::translate(glm::mat4(1), {worldPosition.x, 0, worldPosition.y});
        glm::mat4 mscale = glm::scale(glm::mat4(1), {0.25, 0.25, 0.25});
        gameState->buildingWorldMatrices[i] = mtrans * mscale;
    }

    kx::RenderData rd = {};
    rd.diffuseTexture = gameState->lowpolyAtlas;
    rd.primitiveType = GL_TRIANGLES;
    rd.flipUV = true;
    rd.prog = gameState->defaultProgram;
    rd.cam = gameState->defaultCamera;
    rd.transparent = false;
    rd.vao = model;
    rd.lit = true;
    rd.directionalLightPos = gameState->sunPos;
    rd.lightcam = gameState->lightCam;

    if (shadowPass) {
        rd.prog->setBoolUniform("useTexture", false);
    }

    kx::Renderer renderer;
    if (shadowPass) {
        renderer.renderShadowPassInstanced(rd, gameState->buildingWorldMatrices, gameState->numberOfBuildings);
    } else {
        renderer.renderTexturedInstanced(rd, gameState->buildingWorldMatrices, gameState->numberOfBuildings);
    }

}

void renderTerrain(game_state* gs) {
    glm::vec3 camPos = gs->defaultCamera->getPos();
    glm::vec2 chunk = {round(camPos.x / 100), round(camPos.z / 100)};
    int chunkLeft = chunk.x < 1 ? 0 : chunk.x - 1;
    int chunkRight = chunkLeft > 3 ? 4 : chunkLeft + 2;
    int chunkTop = chunk.y < 1 ? 0 : chunk.y - 1;
    int chunkBot = chunkTop > 3 ? 4 : chunkTop + 2;

    kx::Renderer renderer;
    for (int r = chunkTop ; r < chunkBot; r++) {
        for (int c = chunkLeft; c < chunkRight ; c++) {
            renderer.renderTextured(gs->meshGrids[c + r*5].getRenderDataTextured(gs, gs->lowpolyAtlas));
        }
    }

}

void renderMiniMap(game_state* gameState) {


        // First the minimap background image



        auto image = (uint8_t*)gameState->terrainImage->getPixels();
        auto cells = gameState->terrainDescription->getDimension();
        //float ar = (float) gameState->screenWidth / (float) gameState->screenHeight;
        auto camWidth = gameState->camWidth;
        auto camHeight = camWidth;
        size_t size = cells * cells * 4;

        uint8_t* imageCopy = (uint8_t*) malloc(size);
        memcpy_s(imageCopy, size, image, size);

        // Draw the rect for the camera
        {

            auto campos = gameState->defaultCamera->getPos() + glm::vec3(cells/2- camWidth/2, 0, (cells/2 + camWidth/2)  );
            auto camposR = campos + glm::vec3(camWidth, 0, 0);

            for (int i = campos.x; i < campos.x + camWidth; i++) {
                int coord = (4*i) + ((int)campos.z * cells * 4);
                int coordTop = coord - (camHeight * cells * 4);

//                        if (coord > ((cells*4)-4) && coord < ((cells* cells *4) - 4)) {
//                            imageCopy[coord+0] = 2;
//                            imageCopy[coord+1] = 2;
//                            imageCopy[coord+2] = 2;
//                            imageCopy[coord+3] = 255;
//
//                        }

                if (coordTop > 0 && coordTop < ((cells* cells *4) - 4)) {
                    imageCopy[coordTop+0] = 255;
                    imageCopy[coordTop+1] = 2;
                    imageCopy[coordTop+2] = 2;
                    imageCopy[coordTop+3] = 255;
                }

                // Draw debug points (center, 5 away etc.)
                {
                    int centerCoord = 50 * 4 + ((int) 50 * 4 * 100);
                    imageCopy[centerCoord + 0] = 2;
                    imageCopy[centerCoord + 1] = 2;
                    imageCopy[centerCoord + 2] = 2;
                    imageCopy[centerCoord + 3] = 255;

                    centerCoord = 45 * 4 + ((int) 50 * 4 * 100);
                    imageCopy[centerCoord + 0] = 2;
                    imageCopy[centerCoord + 1] = 2;
                    imageCopy[centerCoord + 2] = 2;
                    imageCopy[centerCoord + 3] = 255;

                    centerCoord = 55 * 4 + ((int) 50 * 4 * 100);
                    imageCopy[centerCoord + 0] = 2;
                    imageCopy[centerCoord + 1] = 2;
                    imageCopy[centerCoord + 2] = 2;
                    imageCopy[centerCoord + 3] = 255;

                    for (int y =  45; y < 45 + 10; y++) {
                        centerCoord = 50 * 4 + ((int) y * 4 * 100);
                        imageCopy[centerCoord + 0] = 20;
                        imageCopy[centerCoord + 1] = 20;
                        imageCopy[centerCoord + 2] = 20;
                        imageCopy[centerCoord + 3] = 255;
                    }

                }



                for (int row = 0; row < camHeight; row++) {
                    int coordCenter = coord - ((camHeight-row) * cells * 4);
                    if (coordCenter > ((cells*4) -4) && coordCenter < ((cells * cells* 4) -4)) {
                        imageCopy[coordCenter+0] = 155;
                        imageCopy[coordCenter+1] = 155;
                        imageCopy[coordCenter+2] = 250;
                        imageCopy[coordCenter+3] = 10;
                    }

                }


            }


            gameState->terrainDebugTexture->updateData(imageCopy);
        }

        kx::Renderer renderer;
        renderer.renderTextured(getMiniMapRenderData(gameState));
        glViewport(60, 70, 145, 145);
        renderer.renderTextured(getTerrainDebugRenderData(gameState));
        glViewport(0, 0, gameState->screenWidth, gameState->screenHeight);
        free(imageCopy);
        imageCopy = nullptr;


}


void gameUpdateAndRender(float time, kx::Window* window, kx::KeyboardState* keyboardState, kx::MouseState* mouseState, std::vector<kx::WinEvent> winEvents, game_memory* gameMemory) {
    static game_state* gameState = new game_state();
            //(game_state*) gameMemory->permanent_storage;
    if (!gameMemory->isInitialized) {
        init(gameMemory, gameState, window);
#ifdef _DEBUG
        startupTests();
#endif
    }
  //  testCache(gameState, time);

    update(time, gameState, mouseState, winEvents);

    window->clearBackbuffer(0.6, 0, 0, 1);

    gameState->defaultCamera->updateMovement(winEvents);


    kx::Renderer renderer;

    // Shadow pass
    {
        gameState->shadowDepthBuffer->bind();
        gameState->shadowDepthBuffer->clear();
        glViewport(0, 0, gameState->screenWidth, gameState->screenHeight);
        renderer.renderShadowPass(*gameState->houseRenderData);

        // render resourcd nodes
        {
            glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 3, -1, "Resource Nodes shadwopass");
            if (gameState->numberOfResourceNodes > 0) {
                renderResourceNodes(gameState, gameState->stoneResourceModel, true);
            }
            glPopDebugGroup();
        }

        // render all buildings
        {
            glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 1, -1, "Buildings shadowpass");
            if (gameState->numberOfBuildings > 0) {
                renderBuildings(gameState, gameState->houseModel, true);
            }
            glPopDebugGroup();
        }

        // Render all units
        {
            glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 2, -1, "Units shadowpass");
            renderUnits(gameState, true);
            glPopDebugGroup();
        }
        gameState->shadowDepthBuffer->unbind();


    }

    // Render to framebuffer
    {
        gameState->colorBuffer->bind();
        gameState->colorBuffer->clear();

        {
            renderer.renderSingleColor(*gameState->houseRenderData, gameState->shadowDepthBuffer->getDepthTexture());

//            renderer.renderSingleColor(gameState->terrain->getRenderDataSingleColor(gameState,{0, 0.5, 0, 1}), gameState->shadowDepthBuffer->getDepthTexture());
            glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 5, -1, "Terrain");
//            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
            renderTerrain(gameState);
            //renderer.renderTextured(gameState->terrain->getRenderDataTextured(gameState, gameState->lowpolyAtlas));
//            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
            glPopDebugGroup();
            renderer.renderTextured(gameState->hpText->getRenderData(gameState->defaultProgram, gameState->uiCamera));
            renderer.renderTextured(gameState->unitCountText->getRenderData(gameState->defaultProgram, gameState->uiCamera));
            renderer.renderTextured(gameState->activePathCountText->getRenderData(gameState->defaultProgram, gameState->uiCamera));
            renderer.renderTextured(gameState->tileUnderMouseText->getRenderData(gameState->defaultProgram, gameState->uiCamera));
            renderer.renderTextured(gameState->resourcesText->getRenderData(gameState->defaultProgram, gameState->uiCamera));

            // UI buttons
            {
                if (doButton(BUILD, gameState->screenWidth - 200, 150, gameState->buildMenuState == MenuState::Open))
                {
                    gameState->buildMenuState = gameState->buildMenuState == MenuState::Open ? MenuState::Closed : MenuState::Open;
                }

                if (gameState->buildMenuState == MenuState::Open)
                {

                    if (hoverButton(HOUSE, 620, 465)) {
                        gameState->popupInfoState = PopupInfoState::Show;

                    } else {
                        gameState->popupInfoState = PopupInfoState::Hide;
                    }

                    if (gameState->popupInfoState == PopupInfoState::Show) {
                        gameState->popupInfoText->setText(gameState->currentPopupInfo->text);
                        gameState->popupInfoText->setScreenPosition(gameState->currentPopupInfo->position.x, gameState->currentPopupInfo->position.y);
                        renderer.renderTextured(gameState->popupInfoText->getRenderData(gameState->defaultProgram, gameState->uiCamera));
                    }

                    if (doButton(HOUSE, 620, 465)) {
                        gameState->buildState = BuildState::Placement;
                        gameState->currentBuildingToPlace = BuildingType::House;
                    }
                    else if (doButton(BARRACK, 620, 430)) {
                        gameState->buildState = BuildState::Placement;
                        gameState->currentBuildingToPlace = BuildingType::Barrack;
                    }
                }
            }

            // Render resource nodes
            {
                glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 5, -1, "Resource Nodes");
                if (gameState->numberOfResourceNodes > 0) {
                    renderResourceNodes(gameState, gameState->stoneResourceModel);
                }
                glPopDebugGroup();
            }


            // render all buildings
            {
                glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 3, -1, "Buildings");
                if (gameState->numberOfBuildings > 0) {
                    renderBuildings(gameState, gameState->houseModel);
                }
                glPopDebugGroup();
            }

            // Render all units
            {
                glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 4, -1, "Units");
//                for (auto b : gameState->units){
//                    renderer.renderSingleColor(b->getRenderData(gameState));
//                }
                renderUnits(gameState);

                glPopDebugGroup();
            }


            if (gameState->renderDebugGrid) {
                renderer.renderSingleColor(gameState->debugGrid->getRenderData(gameState->defaultProgram, gameState->defaultCamera,{0, 0, 0}, {0.02, .3, .01, 1.0}));
                renderer.renderSingleColor(gameState->hpText->getRenderDataDebug(gameState->defaultProgram, gameState->uiCamera));
            }

            if (gameState->buildState == BuildState::Placement) {
                glDisable(GL_DEPTH_TEST);
                renderer.renderSingleColor(*gameState->getCursorRenderData(), nullptr);
                glEnable(GL_DEPTH_TEST);
            }

        }



//        renderMiniMap(gameState);




        gameState->colorBuffer->unbind();
    }

    if ((gameState->screenResX > gameState->screenWidth || gameState->screenResY > gameState->screenHeight) && gameState->isFullScreen) {
        glViewport(gameState->screenResX/2 - gameState->screenWidth/2, gameState->screenResY/2 - gameState->screenHeight/2, gameState->screenWidth, gameState->screenHeight);
    } else {
        glViewport(0, 0, gameState->screenWidth, gameState->screenHeight);
    }

    renderer.renderTextured(*(gameState->fullScreenQuadRenderData));



    window->present();
}

