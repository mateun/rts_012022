#version 450

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;

layout(location = 6) uniform mat4 model;
layout(location = 7) uniform mat4 view;
layout(location = 8) uniform mat4 proj;

out vec2 fs_uv;

void main() {
    gl_Position = proj * view * model * vec4(pos, 1);
    if (!isDepthPass) {
        fs_normal = normalize(vec3(model * vec4(normal, 0.0)));
        fs_uv = uv;

        // Test uv atlas calculation:
        if (isTextureAtlas) {

            /*fs_uv.y *= (1 / atlasRows);
            fs_uv.x *= (1 / atlasCols);*/

            float factorX = 1.0f / atlasCols;
            float factorY = 1.0f / atlasRows;
            fs_uv.x *= factorX;
            fs_uv.y *= factorY;


            //fs_uv.x += factorX * atlasX;
            fs_uv.x += factorX * atlasX;
            //fs_uv.y += (1 - (factorY * atlasY));
            fs_uv.y += 1-(factorY * (atlasY+1));


            //fs_uv.y += (1 - max(factorY, factorY * atlasY));
        }

        if (flipUVs) {
            fs_uv.y = 1 - fs_uv.y;
        }

        fragment_pos_light = mat_proj_light * mat_view_light * model * vec4(pos, 1);
    }


}
