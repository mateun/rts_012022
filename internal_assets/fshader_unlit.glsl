#version 430

layout(binding = 0) uniform sampler2D diffuseTexture;

layout(location = 100) uniform bool useSingleColor;
layout(location = 101) uniform vec4 singleColor;


out vec4 frag_color;
in vec2 fs_uv;


void main() {
    if (useSingleColor) {
        frag_color = singleColor;
    }
    else {
        frag_color = texture(diffuseTexture, fs_uv);

    }

}
