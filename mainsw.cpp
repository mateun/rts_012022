//
// Created by mgrus on 30/01/2022.
//


#include "lib/Window.h"
#include "lib/RenderPipeline.h"
#include <glm/gtc/matrix_transform.hpp>
#include <SDL.h>

int screenWidth = 800;
int screenHeight = 600;

int main(int argc, char** args) {

    kx::WindowPlain win(screenWidth, screenHeight, false);
    win.init();


    kx::RenderPipeline renderPipeline(&win);
    renderPipeline.setVertexPositions({-1, 0, -1, 1, -0, -1, 1, -0, -3});
    glm::mat4 proj = glm::perspective<float>(glm::radians(45.0f), (float) 800/ (float)600, 1, 100);

    glm::mat4 scale = glm::scale(glm::mat4(1), {1, 1, 1});
    glm::mat4 trans = glm::translate(glm::mat4(1), {0, 0, 0});
    glm::mat4 world = trans * scale;
    glm::mat4 trans2 = glm::translate(glm::mat4(1), {4, 0, -2});
    glm::mat4 world2 = trans2 * scale;

    // Perspective matrix test
    glm::vec4 v = glm::vec4(1, -1, 0.5, 1);
    glm::vec4 vp = proj * v;

    // End perspective matrix test

    kx::RenderPipeline pl2(&win);
    pl2.setVertexPositions({-1, 0, -100, -1, 0, 2.5,
                            1, 0, -100, 1, 0, 2.5,
                            -10, 0, -100, 10, 0, -100,
                            -10, 0, -70, 10, 0, -70,
                            -10, 0, -40, 10, 0, -40,
                            -10, 0, -20, 10, 0, -20,
                            -10, 0, -0, 10, 0, -0,
                            -10, 0, 1, 10, 0, 1,
                            -10, 0, 1.1, 10, 0, 1.1,
                            -10, 0, 1.5, 10, 0, 1.5,
                            -10, 0, 1.75, 10, 0, 1.75,


                            });
    //pl2.setVertexPositions({1, 0, -10, 1, 0, 2.5});
    //pl2.setVertexPositions({-10, 0, -10, 10, 0, -10});
//   pl2.setVertexPositions({0, 0, 12,
//                            0, 0, -12,
//                            -1, 0, 7,
//                            -1, 0, -12,
//                            -2, 0, 7,
//                            -2, -0, -12,
//                            1, 0, 7,
//                            1, 0, -12,
//                            2, 0, 7,
//                            2, 0, -12,
//                            3, 0, 7,
//                            3, 0, -12,
//                            // horizontal
//                            -7, 0, 0, 7, 0, 0,
//                            -7, 0, -1, 7, 0, -1,
//                            -7, 0, -2, 7, 0, -2,
//                            -7, 0, -3, 7, 0, -3,
//                            -7, 0, -4, 7, 0, -4,
//                            -7, 0, -5, 7, 0, -5,
//                            -7, 0, -6, 7, 0, -6,
//                            -7, 0, 1, 7, 0, 1,
//                            -7, 0, 2, 7, 0, 2,
//                            -7, 0, 3, 7, 0, 3,
//                            -7, 0, 4, 7, 0, 4,
//                            -7, 0, 5, 7, 0, 5,
//                            });



    bool running = true;

    float camZ = 5;
    while (running) {
        kx::WinEvent event = win.pollWindow();
        if (event.type == kx::WinEventType::EXIT) {
            running = false;
        }

        win.clearBackbuffer(0.1, 0.1, 0.1, 1);


        camZ -= 0.002f;
        glm::mat4 view = glm::lookAt(glm::vec3{0, 1, camZ}, {0, 0, -camZ}, {0, 1, 0});

        pl2.setColor(100, 120, 130, 255);
        pl2.renderLines(proj, view, glm::mat4(1));

        renderPipeline.setColor(200, 180, 25, 255);
        renderPipeline.renderTriangles(proj, view, world);



        win.present();

    }
    return 0;
}
