//
// Created by mgrus on 22/01/2022.
//

#include "Sprite.h"
#include "VertexBuffer.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

static kx::VertexArrayObject* quadVAO = nullptr;

kx::VertexArrayObject* getQuadVAO()
{
    if (quadVAO) {
        return quadVAO;
    }

    quadVAO = new kx::VertexArrayObject();
    quadVAO->bind();
    quadVAO->setNumberOfIndices(6);
    std::vector<float> positions = {
//            -0.5, -0.5, 0,
//            0.5, -0.5, 0,
//            0.5, 0.5, 0,
//            -0.5, 0.5, 0

            0, 0, 0,
            1, 0, 0,
            1, 1, 0,
            0, 1, 0

    };

    std::vector<float> uvs = {
            0, 0,
            1, 0,
            1, 1,
            0, 1
    };

    std::vector<uint32_t> indices = {
            0,1,2,
            0,2, 3
    };


    kx::PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    kx::UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    kx::IndexBuffer indexBuffer(indices);

    quadVAO->unbind();
    return quadVAO;
}


kx::Sprite::Sprite(int x, int y, kx::Texture *texture) : posX(x), posY(y), tex(texture) {

}

glm::mat4 kx::Sprite::getWorldTransform() {
    glm::mat4 ms= glm::scale(glm::mat4(1), {tex->getWidth(), tex->getHeight(), 1});
    if (tile) {
        int w = tileTR.x - tileBL.x;
        int h = tileTR.y - tileBL.y;

        ms = glm::scale(glm::mat4(1), {w, h, 1});
    }

    glm::mat4 mt = glm::translate(glm::mat4(1), {posX + pivot.x, posY + pivot.y, -2});
    return mt * ms;
}

kx::Texture *kx::Sprite::getTexture() {
    return tex;
}

kx::VertexArrayObject *kx::Sprite::getVertexArrayObject() {
    return getQuadVAO();
}

kx::Sprite::Sprite(int x, int y, kx::Texture *texture, glm::vec2 tileBottomLeft, glm::vec2 tileTopRight) : posX(x), posY(y), tex(texture), tileBL(tileBottomLeft), tileTR(tileTopRight) {
    tile = true;

}

bool kx::Sprite::isTile() {
    return tile;
}

float kx::Sprite::getTileOffsetLeft() {
    int wPixels = tileTR.x - tileBL.x;
    int hPixels = tileTR.y - tileBL.y;
    float tileWidth = (float) wPixels / (float) tex->getWidth();
    float tileHeight = (float) hPixels / (float) tex->getHeight();
    return tileBL.x / tex->getWidth();

}

float kx::Sprite::getTileWidth() {
    int wPixels = tileTR.x - tileBL.x;
    float tileWidth = (float) wPixels / (float) tex->getWidth();
    return tileWidth;
}

float kx::Sprite::getTileHeight() {
    int hPixels = tileTR.y - tileBL.y;
    float tileHeight = (float) hPixels / (float) tex->getHeight();
    return tileHeight;
}

float kx::Sprite::getTileOffsetBottom() {
    float val =  (float) tileBL.y / (float) tex->getHeight();
    return val;
}

void kx::Sprite::setPosition(glm::vec2 pos) {
    posX = pos.x;
    posY = pos.y;
}

glm::vec2 kx::Sprite::getPosition() {
    return {posX, posY};
}


