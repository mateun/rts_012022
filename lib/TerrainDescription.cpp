//
// Created by mgrus on 13.03.2022.
//

#include "TerrainDescription.h"

kx::TerrainDescription::TerrainDescription(int dimension) : _dimension(dimension){
    // For now we just create a simple terrain as an island in mid of the sea.

    tiles = new std::vector<TerrainTile*>(dimension * dimension);
    for (int r = 0; r < dimension; r++ ){
        for (int c = 0; c < dimension; c++) {
            tiles->data()[c + r* dimension] = new TerrainTile { WaterSalt, {c, r}};
        }
    }

    // small islands in the middle
    for (int r = 2; r < dimension - 1; r++) {
        for (int i = 2; i < dimension - 1; i++) {
            tiles->data()[i + r * dimension] = new TerrainTile {Grass, {i, r}};
        }
    }

//    for (int r = 6; r < 9; r++) {
//        for (int i = 4; i < 8; i++) {
//            tiles->data()[i + r * dimension] = new TerrainTile {Grass, {i, r}};
//        }
//    }


}

uint8_t *kx::TerrainDescription::getDebugImage() {
    uint8_t* pixels = (uint8_t*) malloc(_dimension * _dimension * 4);

    int counter = 0;
    for (int r = 0; r < _dimension; r++ ){
        for (int c = 0; c < _dimension; c++) {
            TerrainTile* t = tiles->data()[c + r * _dimension];
            glm::vec4 color = {0, 0, 0, 255};
            switch (t->terrainType) {
                case WaterSalt: color.r = 20; color.b = 255; color.g = 20; break;
                case Grass: color.r = 20; color.g = 250; color.b = 20; break;
            }
            pixels[counter+0] = color.r;
            pixels[counter+1] = color.g;
            pixels[counter+2] = color.b;
            pixels[counter+3] = color.a;
            counter += 4;
        }
    }

    return pixels;
}

std::vector<kx::TerrainTile*>* kx::TerrainDescription::getTiles() {
    return tiles;
}

int kx::TerrainDescription::getDimension() {
    return _dimension;
}
