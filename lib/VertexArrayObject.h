//
// Created by mgrus on 18/01/2022.
//

#ifndef KOENIG2022_1_VERTEXARRAYOBJECT_H
#define KOENIG2022_1_VERTEXARRAYOBJECT_H


#include <GL/glew.h>

namespace  kx {
    class VertexArrayObject {

    public:
        VertexArrayObject();
        void bind();
        void unbind();
        int getNumberOfIndices();
        void setNumberOfIndices(int val);
    private:
        GLuint handle;
        int numberOfIndices = -1;
    };

}


#endif //KOENIG2022_1_VERTEXARRAYOBJECT_H
