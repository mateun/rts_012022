//
// Created by mgrus on 28/02/2022.
//

#include "PathNode.h"
#include "TerrainDescription.h"
#include <list>


/**
 * A PathCalculation builds the basis for path finding. It receives a TerrainDescription
 * and builds internal data structures which enable efficient path finding.
 * @param terrainDescription This is the "source of truth" and the basis for the navigation grid.
 */
//kx::PathCalculation::PathCalculation(kx::TerrainDescription *terrainDescription, glm::vec2 startPosition, glm::vec2 endPosition) : _terrainDescription(terrainDescription),
//                                                                                                                                   startPos(startPosition), endPos(endPosition){
//
//    init();
//
//}



void kx::PathCalculation::step() {

    calculationStep++;
    if (calculationStep  >= maxStepsAllowed) {
        pathCalculationState = PathCalculationState::MaxStepsExceeded;
        return;
    }

    if (openList->empty()) {
        pathCalculationState = PathCalculationState::NoPathFound;
        return;
    }

    if (pathCalculationState == PathCalculationState::FinishedOK) {
        return;
    }

    // Local helper functions
    auto getCheapestNode = [] (std::vector<PathNode*>& theList, bool remove = false) -> PathNode* {
        float mincost = 9999;
        PathNode* bestNode = nullptr;
        int index = 0;
        int bestIndex = 0;
        for (auto n : theList) {
            if (n->f() < mincost) {
                bestNode = n;
                mincost = n->f();
                bestIndex = index;
            }
            index++;
        }
        if (remove && theList.size() > 0) {
            theList.erase(theList.begin() + bestIndex);
        }
        return bestNode;
    };
    auto getNeighborsOf = [this] (PathNode* node, TerrainDescription terrainDescription) -> std::vector<PathNode*> {
        std::vector<PathNode*> pns;
        auto nodePosNormalized = node->getTile()->position; //+ glm::vec2{_terrainDescription->getDimension() / 2, _terrainDescription->getDimension() / 2};
        glm::vec2 coordsLeft = {nodePosNormalized.x-1, nodePosNormalized.y};
        if (coordsLeft.x >= 0) {
            pns.push_back(nodes->data()[(int)coordsLeft.x + (int) coordsLeft.y * terrainDescription.getDimension()]);
        }
        glm::vec2 coordsRight = {nodePosNormalized.x+1, nodePosNormalized.y};
        if (coordsRight.x < terrainDescription.getDimension()) {
            pns.push_back(nodes->data()[(int)coordsRight.x + (int) coordsRight.y * terrainDescription.getDimension()]);
        }
        glm::vec2 coordsUp = {nodePosNormalized.x, nodePosNormalized.y-1};
        if (coordsUp.y >= 0) {
            pns.push_back(nodes->data()[(int)coordsUp.x + (int) coordsUp.y * terrainDescription.getDimension()]);
        }

        glm::vec2 coordsDown = {nodePosNormalized.x, nodePosNormalized.y+1};
        if (coordsDown.y >= 0) {
            pns.push_back(nodes->data()[(int)coordsDown.x + (int) coordsDown.y * terrainDescription.getDimension()]);
        }

        return pns;
    };
    auto findInList = [] (std::vector<PathNode*>& theList, PathNode* test) -> bool {
        return  std::find(theList.begin(), theList.end(), test) != theList.end();
    };
    auto buildPath = [] (PathNode* targetNode) -> std::vector<PathNode*> {
        std::vector<PathNode*> pathReversed;
        pathReversed.push_back(targetNode);
        while (auto parent = targetNode->getParent()) {
            pathReversed.push_back(parent);
            targetNode = parent;
        }
        std::vector<PathNode*> path;
        for (auto n : pathReversed) {
            path.push_back(n);
        }
        return path;
    };

    // The meat of the A* algorithm
    {
        auto currentNode = getCheapestNode(*openList, true);

        if (currentNode == endNode) {
            finalPath = buildPath(currentNode);
            pathCalculationState = PathCalculationState::FinishedOK;
            return;
        }

        // TODO actually calculate the current terrainDescription we calculate the path for
        // TODO (instead of _terrainDescrpitions[0])
        auto neighbors = getNeighborsOf(currentNode, _terrainDescriptions[0]);

        //auto bestNeighbor = getCheapestNode(neighbors, true);
        //while (bestNeighbor) {
            for (auto bestNeighbor : neighbors) {

            if (findInList(*openList, bestNeighbor) || findInList(*closedList, bestNeighbor)) {
                int potentialGRunning = currentNode->g() + bestNeighbor->getTraversalCost();
                // We found a better path to this tile, so update the
                // gRunning cost and set the parent to the current node.
                if (bestNeighbor->g() > potentialGRunning) {
                    bestNeighbor->updateG(currentNode->g());
                    bestNeighbor->setParent(currentNode);
                }
            } else {
                // This node has not been visited before so we update its
                // g value and set the parent to the current node.
                bestNeighbor->updateG(currentNode->g());
                bestNeighbor->setParent(currentNode);
                openList->push_back(bestNeighbor);
            }
           // bestNeighbor = getCheapestNode(neighbors, true);
        }
        closedList->push_back(currentNode);
    }

}

kx::PathNode *kx::PathCalculation::getEndNode() {
    return endNode;
}

void kx::PathCalculation::setStartPosition(glm::vec2 pos) {
    this->startPos = pos;
}

void kx::PathCalculation::setEndPosition(glm::vec2 pos) {
    this->endPos = pos;
}

void kx::PathCalculation::setTerrainDescriptions(kx::TerrainDescription *pDescription, int numberOfChunks) {
    this->_terrainDescriptions = pDescription;
}

void kx::PathCalculation::init() {

    int dim = 0;
    for (int i = 0; i < gameState->terrainChunkDim * gameState->terrainChunkDim; i++) {
        dim += _terrainDescriptions[i].getDimension();
    }


    auto tiles = _terrainDescriptions[0].getTiles();
    auto ts = (*tiles);

    if (!nodes) {
        nodes = new std::vector<PathNode*>(dim * dim);
        for (int r = 0; r < dim; r++ ){
            for (int c = 0; c < dim; c++) {
                auto node = new PathNode(ts[c + r * dim], this);
                node->updateG(0);
                nodes->data()[c + r * dim] = node;

            }
        }
    }


    openList = new std::vector<PathNode*>(dim*dim);
    closedList = new std::vector<PathNode*>(dim*dim);


    openList->clear();
    closedList->clear();

    // TODO fix bug when startPos coming as a parameter is actually negative.
    // Within this path calculation we only know only positive growing coords, but this is not
    // correct.
    auto startPosNormalized = startPos + glm::vec2(dim/2, dim/2);
    auto endPosNormalized = endPos + glm::vec2(dim/2, dim/2);


    // TODO actually calculate the "real" current terrain description (chunk)
    // How do we support path finding over several chunks?
    auto terrainDescription = _terrainDescriptions[0];
    auto startNode = (*nodes)[startPosNormalized.x + terrainDescription.getDimension() * startPosNormalized.y];
    endNode = (*nodes)[endPosNormalized.x + terrainDescription.getDimension() * endPosNormalized.y];

    openList->push_back(startNode);
    pathCalculationState = PathCalculationState::InProgress;
}

kx::PathCalculation::~PathCalculation() {
    for (auto n : *nodes) {
        delete(n);

    }
    nodes->clear();
    nodes = nullptr;
    delete(nodes);
}

kx::PathNode::PathNode(kx::TerrainTile *tile, PathCalculation* grid) : _tile(tile), _grid(grid) {

}

void kx::PathNode::setParent(kx::PathNode *parent) {
    this->pathParent = parent;
}

void kx::PathNode::updateG(int preCost) {
    gRunning = preCost + getTraversalCost();
}

int kx::PathNode::g() {
    return gRunning;
}

float kx::PathNode::getHeuristicCost() {

    float distance = glm::distance(this->_tile->position, _grid->getEndNode()->getTile()->position);
  //  printf("distance: %f\n", distance);
    return distance;
}

float kx::PathNode::f() {
    return getHeuristicCost();
}

kx::PathNode *kx::PathNode::getParent() {
    return pathParent;
}

int kx::PathNode::getTraversalCost() {
    switch (_tile->terrainType) {
        case TerrainType::WaterSalt: return 999;
        case TerrainType::Grass: return 2;
        case TerrainType::Sand: return 4;
        case TerrainType::Swamp: return 10;
        case TerrainType::Mud: return 8;
        case TerrainType::WaterSweet: return 999;
        case TerrainType::Rock: return 6;
    }
    return 9999;
}
