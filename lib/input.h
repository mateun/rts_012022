//
// Created by mgrus on 19/02/2022.
//

#ifndef KOENIG2022_1_INPUT_H
#define KOENIG2022_1_INPUT_H

#include <glm/glm.hpp>

namespace kx {

    enum KeyCode {
        a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,
        strg,shift,k0,k1,k2,k3,k4,k5,k6,k7,k8,k9,
        f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, esc
    };

    enum MouseKey {
        left, right, middle
    };

    struct KeyboardState {

        bool isDown(KeyCode keyCode);
        const uint8_t * state = nullptr;

    };

    struct MouseState {
        bool isDown(MouseKey mouseKey);
        glm::vec2 mouseCoords;
    };

};

#endif //KOENIG2022_1_INPUT_H
