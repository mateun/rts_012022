//
// Created by mgrus on 06/02/2022.
//

#ifndef KOENIG2022_1_FILEUTILS_H
#define KOENIG2022_1_FILEUTILS_H


#include <string>

namespace kx {

    class FileUtils {
    public:
        static std::string readFile(const std::string& fileName);
    };


}




#endif //KOENIG2022_1_FILEUTILS_H
