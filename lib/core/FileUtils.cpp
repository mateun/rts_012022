//
// Created by mgrus on 06/02/2022.
//

#include <sstream>
#include <fstream>
#include "FileUtils.h"

std::string kx::FileUtils::readFile(const std::string &fileName) {
    std::ifstream t(fileName);
    std::stringstream buffer;
    buffer << t.rdbuf();
    return buffer.str();
}
