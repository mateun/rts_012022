//
// Created by mgrus on 28/02/2022.
//

#ifndef KOENIG2022_1_PATHNODE_H
#define KOENIG2022_1_PATHNODE_H


#include <vector>
#include <glm/vec2.hpp>
#include "TerrainDescription.h"
#include "rtstest.h"

class game_state;

namespace kx {
    class TerrainDescription;
    class PathCalculation;


    class PathNode {

    public:
        PathNode(TerrainTile* tile, PathCalculation* grid);
        float getHeuristicCost();
        int getTraversalCost();
        int g();
        float f();
        TerrainTile* getTile() { return _tile; }

        void updateG(int preCost);
        void setParent(PathNode *pNode);

        PathNode *getParent();

    private:
        // TODO do we even store this? Maybe we just only calculate in the respective getter function.
        int heuristicCost = 0;
        int traversalCost = 0;
        PathCalculation *_grid = nullptr;
        PathNode* pathParent = nullptr;
        int gRunning = 0;
        glm::vec2 position;
        TerrainTile* _tile;


    };

    enum class PathCalculationState {
        NotStarted,
        InProgress,
        MaxStepsExceeded,
        FinishedOK,
        NoPathFound
    };

    /**
     * This represents a single path finding execution.
     * This is e.g. spawned for every unit that has been itself spawned by a building,
     * and now searches for the nearest workplace.
     * All the path nodes are specifically created for this path.
     * This is necessary as the criteria for pathing change dynamically, e.g.
     * terrain forms can change and buildings and units move.
     * We need to check the memory and performance footprints.
     * This class is meant to be threadsafe and be able to work in one transaction (in a separate thread?!) and
     * be single stepped (from the main thread?!).
     */
    class PathCalculation {
    public:
        //PathCalculation(TerrainDescription* terrainDescription, glm::vec2 startPosition, glm::vec2 endPosition);
        virtual ~PathCalculation();
        void step();
        std::vector<PathNode*> calculatePathAtOnce();
        glm::vec2 getStartPosition() { return startPos; }
        glm::vec2 getEndPosition() { return endPos; }
        PathCalculationState getCalculationState() { return pathCalculationState; }
        std::vector<PathNode*> getPath() { return finalPath; }

        PathNode *getEndNode();

        void setStartPosition(glm::vec2 vec1);
        void setEndPosition(glm::vec2 vec1);

        void setTerrainDescriptions(TerrainDescription *pDescription, int numberOfChunks);

        void init();

    public:
        game_state* gameState = nullptr;

    private:
        std::vector<PathNode*>* nodes = nullptr;
        TerrainDescription* _terrainDescriptions = nullptr;
        glm::vec2 startPos;
        glm::vec2 endPos;
        PathNode *endNode = nullptr;
        int calculationStep = 0;
        int maxStepsAllowed = 2000;
        std::vector<PathNode*>* openList= nullptr;
        std::vector<PathNode*>* closedList = nullptr;
        std::vector<PathNode*> finalPath;
        PathCalculationState pathCalculationState = PathCalculationState::NotStarted;

    };




}

#endif //KOENIG2022_1_PATHNODE_H
