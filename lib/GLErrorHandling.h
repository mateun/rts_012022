//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_GLERRORHANDLING_H
#define KOENIG2022_1_GLERRORHANDLING_H

#define GL_ERROR_FAIL \
    GLenum err = glGetError(); \
    if (err != 0) {       \
       printf("gl error: %d\n", err);  \
       exit(2); \
    }

#endif //KOENIG2022_1_GLERRORHANDLING_H
