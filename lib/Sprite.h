//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_SPRITE_H
#define KOENIG2022_1_SPRITE_H

#include "Texture.h"
#include "Scene.h"

namespace kx {

    class Sprite : public Renderable2D {
    public:
        Sprite(int x, int y, Texture* texture);
        Sprite(int x, int y, Texture* texture, glm::vec2 tileBottomLeft, glm::vec2 tileTopRight);
        glm::mat4 getWorldTransform() override;
        Texture * getTexture() override;
        VertexArrayObject * getVertexArrayObject() override;
        void setPosition(glm::vec2 pos);
        glm::vec2 getPosition();


    public:
        // Renderable2D
        bool isTile() override;
        virtual float getTileOffsetLeft() override;
        virtual float getTileWidth() override;
        virtual float getTileOffsetBottom() override;
        virtual float getTileHeight() override;

    private:
        int posX;
        int posY;
        glm::vec2 pivot = {0, 0};
        Texture *tex;
        bool tile= false;
        glm::vec2 tileBL;
        glm::vec2 tileTR;
    };

}



#endif //KOENIG2022_1_SPRITE_H
