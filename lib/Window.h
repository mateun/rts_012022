//
// Created by mgrus on 16/01/2022.
//

#ifndef KOENIG2022_1_WINDOW_H
#define KOENIG2022_1_WINDOW_H
#include <string>
#include "input.h"

class SDL_Window;
class SDL_Renderer;

namespace kx {

    struct WinDimension {
        int w;
        int h;
    };

    enum class WinEventType {
        NONE,
        EXIT,
        KEYDOWN,
        KEYUP,
        TEXTINPUT,
        MOUSEDOWN,
        MOUSEUP
    };

    struct WinEvent {
        WinEventType type;
        std::string key;
        kx::KeyCode keyCode;
    };

    class Window {
    public:
        Window(int w, int h, bool fullscreen);
        WinEvent pollWindow() const;
        virtual void clearBackbuffer(float r, float g, float b, float a) = 0;
        virtual void present() = 0;
        void* getHandle();
        WinDimension getDimension();
        virtual void init();

        bool isFullscreen();

    public:
        int screenResX;
        int screenResY;

    protected:
        virtual uint32_t provideWindowFlags() = 0;


    protected:
        SDL_Window* win;
        int w;
        int h;


        bool fullScreen;
    };

    class WindowPlain : public Window {
    public:
        WindowPlain(int w, int h, bool fullscreen);
        void clearBackbuffer(float r, float g, float b, float a) override;
        void init() override;
        void present() override;
        void renderPoint(int x, int y);
        void renderLine(int xs, int xy, int endX, int endY, uint8_t r, uint8_t g, uint8_t b, uint8_t a) ;

    protected:
        uint32_t provideWindowFlags() override;

    private:
        SDL_Renderer* renderer;
    };

    class WindowGL : public Window {
    public:
        WindowGL(int w, int h, bool fullscreen);
        void clearBackbuffer(float r, float g, float b, float a) override;
        void init() override;
        void present() override;


    protected:

        uint32_t provideWindowFlags() override;

    };

}



#endif //KOENIG2022_1_WINDOW_H
