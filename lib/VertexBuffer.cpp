//
// Created by mgrus on 18/01/2022.
//

#include "VertexBuffer.h"

kx::PositionBuffer::PositionBuffer(std::vector<float> data) : VertexBuffer() {

    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);
}

void kx::PositionBuffer::enableAttribute(int attributeNumber) {
    glVertexAttribPointer(attributeNumber, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(attributeNumber);

}

void kx::PositionBuffer::updateData(std::vector<float> newPositions) {
    glBindBuffer(GL_ARRAY_BUFFER, handle);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * newPositions.size(), newPositions.data());
}

void kx::VertexBuffer::bind() {
    glBindBuffer(GL_ARRAY_BUFFER, handle);
}

kx::VertexBuffer::VertexBuffer() {
    glCreateBuffers(1, &handle);
    glBindBuffer(GL_ARRAY_BUFFER, handle);
}

kx::UVBuffer::UVBuffer(std::vector<float> data) :VertexBuffer() {
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);
}

void kx::UVBuffer::enableAttribute(int attributeNumber) {
    glVertexAttribPointer(attributeNumber, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(attributeNumber);
}

kx::IndexBuffer::IndexBuffer(std::vector<uint32_t> indices) {
    glCreateBuffers(1, &handle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * indices.size(), indices.data(), GL_STATIC_DRAW);


}

kx::NormalBuffer::NormalBuffer(std::vector<float> normals) {
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * normals.size(), normals.data(), GL_STATIC_DRAW);
}

void kx::NormalBuffer::enableAttribute(int attributeNumber) {
    glVertexAttribPointer(attributeNumber, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(attributeNumber);
}
