//
// Created by mgrus on 22/01/2022.
//

#include "Image.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

kx::Image::Image(const std::string &fileName)  {
    int imageChannels;
    pixels = stbi_load(
            fileName.c_str(), &w, &h,
            &imageChannels,
            4);

}

int kx::Image::getWidth() {
    return w;
}

int kx::Image::getHeight() {
    return h;
}

const void *kx::Image::getPixels() {
    return pixels;
}

kx::Image::Image(uint8_t *pixelsInput, int w, int h) : w(w), h(h) {
    this->pixels = pixelsInput;
}

