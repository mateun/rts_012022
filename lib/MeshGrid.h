//
// Created by mgrus on 13/02/2022.
//

#ifndef KOENIG2022_1_MESHGRID_H
#define KOENIG2022_1_MESHGRID_H

#include <glm/glm.hpp>
#include "VertexArrayObject.h"
#include "Renderer.h"
#include "TerrainDescription.h"

namespace kx {

    class MeshGrid {
    public:
        MeshGrid(glm::vec3 position, int cellSize, int numberOfCells);
        MeshGrid(glm::vec3 position, int cellSize, int numberOfCells, uint8_t* heightMap, bool useCubes = false);
        MeshGrid(glm::vec3 position, int cellSize, kx::TerrainDescription *terrainDesc);

        glm::vec3 getPosition() const { return position;}
        VertexArrayObject* getVAO() const;

        RenderData getRenderDataSingleColor(kx::Program *pProgram, kx::Camera *pCamera, kx::Camera* lightCam, glm::vec4 color, glm::vec3 lightPos);
        RenderData getRenderDataSingleColor(RenderDataProvider* rdp, glm::vec4 color = {0.9, 0.9,0.9, 1});
        RenderData getRenderDataTextured(RenderDataProvider* rdp, kx::Texture* terrainDiffuseTexture);

    private:
        void initWithSharedVertices(glm::vec3 position, int cellSize, int numberOfCells);
        void initWithSeparateVertices(glm::vec3 position, int cellSize, int numberOfCells);
        void initWithSeparateVerticesAndTerrain(glm::vec3 vec1, int size, TerrainDescription *pDescription);
        void initWithSeparateVerticesAndHeightMap(glm::vec3 position, int cellSize, int numberOfCells, uint8_t* heightMap);
        void initWithCubesAndHeightMap(glm::vec3 position, int cellSize, int numberOfCells, uint8_t* heightMap);
        void initWithCubes(glm::vec3 position, int cellSize, int numberOfCells);


    private:
        VertexArrayObject* vao;
        glm::vec3 position;


    };
}



#endif //KOENIG2022_1_MESHGRID_H
