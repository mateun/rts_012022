//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_TEXTURE_H
#define KOENIG2022_1_TEXTURE_H


#include <GL/glew.h>
#include "Image.h"

namespace kx {

    class Texture {
    public:
        Texture() {}
        Texture(int w, int h);
        Texture(int wh);
        Texture(int w, int h, void* pixels);
        Texture(Image* image);
        void bind(int index = 0);
        void unbind();

        int getWidth();
        int getHeight();
        GLuint getHandle() { return handle;}
        void updateData(uint8_t* pixels);

    protected:
        virtual void init();
        virtual void prepareStorage();


    protected:
        int width;
        int height;
        GLuint handle;
    };

    class DepthTexture : public Texture {
    public:
        DepthTexture(int w, int h);
    protected:
        void prepareStorage() override;
    };


}



#endif //KOENIG2022_1_TEXTURE_H
