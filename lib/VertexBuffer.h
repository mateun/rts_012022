//
// Created by mgrus on 18/01/2022.
//

#ifndef KOENIG2022_1_VERTEXBUFFER_H
#define KOENIG2022_1_VERTEXBUFFER_H


#include <vector>
#include <gl/glew.h>

namespace kx {

    class IndexBuffer {
    public:
        IndexBuffer(std::vector<uint32_t> indices);

    protected:
        GLuint handle;
    };

    class VertexBuffer {
    public:
        VertexBuffer();
        void bind();
        virtual void enableAttribute(int attributeNumber) = 0;

    protected:
        GLuint handle;

    };

    class NormalBuffer : public VertexBuffer {
    public:
        NormalBuffer(std::vector<float> normals);
        void enableAttribute(int attributeNumber) override;
    };

    class PositionBuffer : public VertexBuffer {
    public:
        PositionBuffer(std::vector<float> data);
        void enableAttribute(int attributeNumber) override;

        void updateData(std::vector<float> vector1);
    };

    class UVBuffer : public VertexBuffer {
    public:
        UVBuffer(std::vector<float> data);
        void enableAttribute(int attributeNumber) override;
    };



}


#endif //KOENIG2022_1_VERTEXBUFFER_H
