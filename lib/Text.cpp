//
// Created by mgrus on 15/02/2022.
//

#include "Text.h"
#include "stb_truetype.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

extern void setPixel(uint8_t* pixel, float r, float g, float b, float a);
extern std::map<std::string, kx::Program*>* getShaderStore();

kx::Text::Text(const std::string &initialText, glm::vec2 position, float pixelHeight, const char* fontFileName) : position(position) {

    sprite = new kx::Sprite(position.x, position.y, texture);
    this->text = initialText;
    this->pixelHeight = pixelHeight;

    fread(ttf_buffer, 1, 1<<20, fopen(fontFileName, "rb"));
    stbtt_InitFont(&fontinfo, ttf_buffer, stbtt_GetFontOffsetForIndex(ttf_buffer,0));
    stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs
    bakeResult = stbtt_BakeFontBitmap(ttf_buffer,0, pixelHeight, temp_font_bitmap,512,512,32,96,cdata);
    if (bakeResult <= 0) {
        printf("text is not fitting!\n");
        return;
    }

    renderText();
}

kx::RenderData kx::Text::getRenderData(kx::Program* program, kx::Camera* cam) {
    RenderData rd = {};
    if (!program)
    {
        rd.prog = (*getShaderStore())["default2D"];
    }
    else
    {
        rd.prog = program;
    }
    rd.cam = cam;
    rd.vao =((Renderable2D*)sprite)->getVertexArrayObject();
    rd.world = glm::translate(glm::mat4(1), {position.x, position.y, -0.1}) *
            glm::scale(glm::mat4(1), {fontTexture->getWidth(), fontTexture->getHeight(), 1})  ;
    rd.diffuseTexture = fontTexture;
    rd.lit = false;
    rd.transparent = true;
    rd.flipUV = true;
    return rd;

}

kx::RenderData kx::Text::getRenderDataDebug(kx::Program *program, kx::Camera *cam) {
    RenderData rd = {};
    if (!program)
    {
        rd.prog = (*getShaderStore())["default2D"];
    }
    else
    {
        rd.prog = program;
    }
    rd.cam = cam;
    rd.vao =((Renderable2D*)sprite)->getVertexArrayObject();
    rd.world = glm::translate(glm::mat4(1), {position.x, position.y, -0.09}) *
               glm::scale(glm::mat4(1), {fontTexture->getWidth(), fontTexture->getHeight(), 1})  ;
    rd.diffuseTexture = nullptr;
    rd.lit = false;
    rd.transparent = false;
    rd.flipUV = true;
    rd.color = {1, 1, 0, 1};
    rd.primitiveType = GL_TRIANGLES;

    return rd;
}

void kx::Text::setText(const std::string &text) {
    this->text = text;
    renderText();
}

void calcTextDimension(const char* text, int& width, int& h, stbtt_fontinfo* fontinfo, float pixelHeight) {

    float x = 0;
    int ascent = 0;

    stbtt_GetFontVMetrics(fontinfo, &ascent,0,0);
    float scale =stbtt_ScaleForPixelHeight(fontinfo, pixelHeight);

    int maxHeight = 0;
    while (*text) {
        int advance,lsb,x0,y0,x1,y1;
        int w=0, h=0, xoff=0, yoff=0;
        stbtt_GetCodepointHMetrics(fontinfo, *text, &advance, &lsb);
        uint8_t* glyphbitmap = stbtt_GetCodepointBitmap(fontinfo, 0, scale, *text, &w, &h, 0, &yoff);
        int gWidthInPixels = w;
        int gHeightInPixels = h;

        x += (advance * scale);
        if (*(text+1)) {
            x += scale * stbtt_GetCodepointKernAdvance(fontinfo, *text, *(text + 1));
        }

        width += w;
        if (h > maxHeight) {
            maxHeight = h;
        }
        ++text;

    }
    width = x;
    h = maxHeight;
}

void kx::Text::renderText() {

    int tw = 0;
    int th = 0;
    calcTextDimension(text.c_str(), tw, th, &fontinfo, pixelHeight);

//    tw += 10;

    font_pixels = (uint8_t*) malloc(tw * 40 * 4);
    memset(font_pixels, 0, tw*40*4);

    const char* text = this->text.c_str();

    int ascent = 0;
    stbtt_GetFontVMetrics(&fontinfo, &ascent,0,0);
    float scale =stbtt_ScaleForPixelHeight(&fontinfo, pixelHeight);
    int baseline = (int) (ascent*scale);

    float x = 0;
    float y = baseline + 10;

    while (*text) {
        int advance,lsb,x0,y0,x1,y1;
        int w=0, h=0, yoff=0;
        stbtt_GetCodepointHMetrics(&fontinfo, *text, &advance, &lsb);
        uint8_t* glyphbitmap = stbtt_GetCodepointBitmap(&fontinfo, 0, scale, *text, &w, &h, 0, &yoff);
        int gWidthInPixels = w;
        int gHeightInPixels = h;
        for (int r = 0; r <  gHeightInPixels; r++) {
            for (int c = 0; c  < gWidthInPixels; c++) {
                uint8_t valt = glyphbitmap[c + r * w];
                int startPixel = (((int)x+c) * 4) + (((int)y+r + yoff)* tw * 4);
                setPixel(&font_pixels[startPixel], (float) valt / 255.0, (float) 1, (float) 1, (float) valt/ (float) 255.0);
            }
        }

        x += (advance * scale);
        if (*(text+1)) {
            x += scale * stbtt_GetCodepointKernAdvance(&fontinfo, *text, *(text + 1));
        }

        ++text;

    }

    if (fontTexture) {
        delete(fontTexture);
        fontTexture = nullptr;
    }
    fontTexture = new kx::Texture(tw, 40, font_pixels);

    if (font_pixels) {
        free(font_pixels);
    }
}

glm::vec2 kx::Text::getScreenDimensions() {
    return {fontTexture->getWidth(), fontTexture->getHeight()};
}
