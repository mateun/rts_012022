//
// Created by mgrus on 17/01/2022.
//

#include "GLContext.h"
#include <gl/glew.h>
#include <SDL.h>
#include <string>

kx::GLContext::GLContext(kx::Window *window) {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GLContext glContext = SDL_GL_CreateContext((SDL_Window*)window->getHandle());
    SDL_GL_SetSwapInterval(1);

    GLenum r = glewInit();
    if (r != GLEW_OK) {
        std::string glew_error = std::string((char*)glewGetErrorString(r));
        SDL_Log(("error while initializing glew" + glew_error).c_str());
        exit(1);
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    glViewport(0, 0, window->getDimension().w, window->getDimension().h);
}
