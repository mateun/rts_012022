//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_TERRAIN_H
#define KOENIG2022_1_TERRAIN_H

#include "Camera.h"
#include "Texture.h"
#include "Sprite.h"

namespace kx {
    class Terrain {

    public:
        Terrain(int widthInTiles);
        void render(Camera* camera);
        void debugUpdateTerrain();

    private:
        Texture* chunk1 = nullptr;
        Sprite* chunk1Sprite = nullptr;
        uint8_t* chunk1Pixels = nullptr;

        std::vector<Sprite*> terrainChunks;

        // Debug
        float erosionXStart = 16;
        float erosionXEnd = 16;
        // End Debug


    };

}



#endif //KOENIG2022_1_TERRAIN_H
