//
// Created by mgrus on 05/02/2022.
//

#ifndef KOENIG2022_1_RENDERER_H
#define KOENIG2022_1_RENDERER_H

#include "VertexArrayObject.h"
#include "Camera.h"
#include "Program.h"
#include "Texture.h"

namespace kx {


    class RenderDataProvider {
    public:
        virtual Camera* getCamera() = 0;
        virtual Camera* getLightCamera() = 0;
        virtual glm::vec3 getLightPosition() = 0;
        virtual Program* getProgram() = 0;
    };


    struct RenderData {
        VertexArrayObject* vao = nullptr;
        Camera* cam = nullptr;
        Camera* lightcam = nullptr;
        Program* prog = nullptr;
        Texture* diffuseTexture = nullptr;  // TODO default checkerboard texture here?!
        Texture* shadowTexture = nullptr;
        glm::vec4 color = {1, 1, 1, 1 };
        glm::mat4 world = glm::mat4(1);
        glm::vec3 directionalLightPos;
        bool transparent = false;
        bool lit = true;
        bool flipUV = true;
        unsigned int primitiveType;
        bool wireFrame = false;
    };

    class Renderer {
    public:
        void renderShadowPass(RenderData renderData);
        void renderShadowPassInstanced(kx::RenderData renderData, glm::mat4* worldMatrices, int wmSize);
        void renderTextured(RenderData renderData, kx::DepthTexture* shadowDepthTexture = nullptr);
        void renderSingleColor(RenderData renderData, kx::DepthTexture* shadowDepthTexture = nullptr);
        void renderTexturedInstanced(RenderData renderData, glm::mat4* worldMatrices, int wmSize, kx::DepthTexture* shadowDepthTexture = nullptr);
        void renderWireFrame(RenderData renderData);

    };

}



#endif //KOENIG2022_1_RENDERER_H
