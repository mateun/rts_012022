//
// Created by mgrus on 06/02/2022.
//

#ifndef KOENIG2022_1_MODELIMPORTER_H
#define KOENIG2022_1_MODELIMPORTER_H

#include <string>
#include "VertexArrayObject.h"

namespace kx {

    class ModelImporter {

    public:
         static VertexArrayObject* importFromFile(const std::string& modelFileName);



    };

};


#endif //KOENIG2022_1_MODELIMPORTER_H
