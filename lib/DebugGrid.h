//
// Created by mgrus on 22/02/2022.
//

#ifndef KOENIG2022_1_DEBUGGRID_H
#define KOENIG2022_1_DEBUGGRID_H


#include "VertexArrayObject.h"
#include "Renderer.h"

namespace  kx {

    class Program;
    class Camera;

/**
 * A line based grid which can have a world position,
 * variable cell size, rotation.
 */
    class DebugGrid {

    public:
        DebugGrid(int cols, int cellSize);
        kx::RenderData getRenderData(kx::Program* program, kx::Camera* camera, glm::vec3 position = {0, 0, 0}, glm::vec4  color = {1, 1,1, 1});

    private:
        VertexArrayObject *vao;
    };

}


#endif //KOENIG2022_1_DEBUGGRID_H
