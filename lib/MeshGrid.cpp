//
// Created by mgrus on 13/02/2022.
//

#include "MeshGrid.h"
#include "VertexBuffer.h"
#include "TerrainDescription.h"
#include <string>
#include <glm/gtc/matrix_transform.hpp>



kx::MeshGrid::MeshGrid(glm::vec3 position, int cellSize, kx::TerrainDescription* terrainDesc) : position(position) {
    initWithSeparateVerticesAndTerrain(position, cellSize, terrainDesc);
}

kx::MeshGrid::MeshGrid(glm::vec3 position, int cellSize, int numberOfCells) : position(position) {
    initWithSeparateVertices(position, cellSize, numberOfCells);
}


kx::MeshGrid::MeshGrid(glm::vec3 position, int cellSize, int numberOfCells, uint8_t* heightMap, bool useCubes) : position(position) {
    if (useCubes) {
        initWithCubesAndHeightMap(position, cellSize, numberOfCells, heightMap);
    } else {
        initWithSeparateVerticesAndHeightMap(position, cellSize, numberOfCells, heightMap);
    }

}


kx::VertexArrayObject *kx::MeshGrid::getVAO() const {
    return vao;
}

kx::RenderData kx::MeshGrid::getRenderDataSingleColor(kx::RenderDataProvider* rdp, glm::vec4 color) {
   Program* prog = rdp->getProgram();
   return this->getRenderDataSingleColor(prog, rdp->getCamera(), rdp->getLightCamera(), color, rdp->getLightPosition());
}

kx::RenderData kx::MeshGrid::getRenderDataSingleColor(kx::Program *pProgram, kx::Camera *pCamera, kx::Camera* lightCam, glm::vec4 color,
                                                      glm::vec3 lightPos) {
    RenderData rd = {};
    if (!pProgram)
    {
        //rd.prog = (*getShaderStore())["default2D"];
    }
    else
    {
        rd.prog = pProgram;
    }
    rd.cam = pCamera;
    rd.vao =vao;
    rd.world = glm::translate(glm::mat4(1), {position.x, position.y, position.z});
    rd.diffuseTexture = nullptr;
    rd.lit = true;
    rd.directionalLightPos = lightPos;
    rd.transparent = false;
    rd.color = color;
    rd.lightcam = lightCam;
    rd.primitiveType = GL_TRIANGLES;
    rd.wireFrame = false;

    return rd;
}

kx::RenderData kx::MeshGrid::getRenderDataTextured(kx::RenderDataProvider *rdp, kx::Texture* terrainDiffuseTexture) {
    RenderData rd = {};
    rd.prog = rdp->getProgram();
    rd.cam = rdp->getCamera();
    rd.vao =vao;
    rd.world = glm::translate(glm::mat4(1), {position.x, position.y, position.z});
    rd.diffuseTexture = terrainDiffuseTexture;
    rd.lit = true;
    rd.transparent = false;
    rd.lightcam = rdp->getLightCamera();
    rd.directionalLightPos = rdp->getLightPosition();
    rd.primitiveType = GL_TRIANGLES;

    return rd;

}


void kx::MeshGrid::initWithSeparateVertices(glm::vec3 position, int cellSize, int numberOfCells) {
    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;


    auto uvCreateTiled = [&uvs] (int r, int c) -> void {
        float grass_startU = 0.62;
        float grass_endU = 0.64;
        float grass_startV = 1- 0.02;
        float grass_endV = 1-0.015;

        float w_startU = 0.71;
        float w_endU = 0.72;
        float w_startV = 1- 0.35;
        float w_endV = 1-0.36;

        if (c != 3 && c !=4) {
            // left top
            uvs.push_back(grass_startU);
            uvs.push_back(grass_startV);

            // right bot
            uvs.push_back(grass_endU);
            uvs.push_back(grass_endV);

            // right top
            uvs.push_back(grass_endU);
            uvs.push_back(grass_startV);

            // left top
            uvs.push_back(grass_startU);
            uvs.push_back(grass_endV);
        } else {
            // left top
            uvs.push_back(w_startU);
            uvs.push_back(w_startV);

            // right bot
            uvs.push_back(w_endU);
            uvs.push_back(w_endV);

            // right top
            uvs.push_back(w_endU);
            uvs.push_back(w_startV);

            // left top
            uvs.push_back(w_startU);
            uvs.push_back(w_endV);
        }

    };

    // We create 2 triangles per cell, i.e. a full quad.
    // So we can have separate uvs per cell.
    for (int row = 0; row < numberOfCells; row++) {
        for (int col = 0; col < numberOfCells; col++) {
            // Left top
            positions.push_back( col * cellSize);
            positions.push_back(0);
            positions.push_back(-row * cellSize);

            // Right bottom
            positions.push_back((col + 1) * cellSize);
            positions.push_back(0);
            positions.push_back((-row - 1) * cellSize);

            // Right Top
            positions.push_back((col + 1) * cellSize);
            positions.push_back(0);
            positions.push_back((-row) * cellSize);

            // Left bottom
            positions.push_back( col * cellSize);
            positions.push_back(0);
            positions.push_back((-row - 1) * cellSize);

            //uvCreateFractioned(row, col);
            uvCreateTiled(row, col);

            for (int i = 0; i < 4; i++) {
                normals.push_back(0);
                normals.push_back(1);
                normals.push_back(0);
            }

        }
    }

    std::vector<uint32_t> indices;

    for (int row = 0; row < numberOfCells; row ++ ) {
        for (int col = 0; col < numberOfCells; col++) {

            uint32_t leftTop = (col * 4) + (row * numberOfCells * 4);
            uint32_t rightBot = leftTop + 1;
            uint32_t rightTop = leftTop + 2;
            uint32_t leftBot = leftTop + 3;

            glm::vec3 lt = { positions[leftTop*3], positions[(leftTop*3) + 1], positions[(leftTop*3)+2]};
            glm::vec3 rb = { positions[(rightBot*3)], positions[(rightBot*3) + 1], positions[(rightBot*3)+2]};
            glm::vec3 rt = { positions[(rightTop*3)], positions[(rightTop*3) + 1], positions[(rightTop*3)+2]};
            glm::vec3 lb = { positions[(leftBot*3)], positions[(leftBot*3) + 1], positions[(leftBot*3)+2]};

            indices.push_back(leftTop);
            indices.push_back(rightTop);
            indices.push_back(rightBot);
            indices.push_back(leftTop);
            indices.push_back(rightBot);
            indices.push_back(leftBot);
        }
    }

    // Now lets pack everything into a VAO

    vao = new VertexArrayObject();
    vao->bind();
    PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());

    vao->unbind();
}

void kx::MeshGrid::initWithSeparateVerticesAndHeightMap(glm::vec3 position, int cellSize, int numberOfCells, uint8_t* heightMap) {
    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;

    auto uvCreateTiled = [&uvs] (int r, int c) -> void {
        float grass_startU = 0.62;
        float grass_endU = 0.64;
        float grass_startV = 1- 0.02;
        float grass_endV = 1-0.015;

        float w_startU = 0.71;
        float w_endU = 0.72;
        float w_startV = 1- 0.35;
        float w_endV = 1-0.36;

        if (c != 3 && c !=4) {
            // left top
            uvs.push_back(grass_startU);
            uvs.push_back(grass_startV);

            // right bot
            uvs.push_back(grass_endU);
            uvs.push_back(grass_endV);

            // right top
            uvs.push_back(grass_endU);
            uvs.push_back(grass_startV);

            // left top
            uvs.push_back(grass_startU);
            uvs.push_back(grass_endV);
        } else {
            // left top
            uvs.push_back(w_startU);
            uvs.push_back(w_startV);

            // right bot
            uvs.push_back(w_endU);
            uvs.push_back(w_endV);

            // right top
            uvs.push_back(w_endU);
            uvs.push_back(w_startV);

            // left top
            uvs.push_back(w_startU);
            uvs.push_back(w_endV);
        }

    };

    // We create 2 triangles per cell, i.e. a full quad.
    // So we can have separate uvs per cell.
    for (int row = 0; row < numberOfCells; row++) {
        for (int col = 0; col < numberOfCells; col++) {

            float heightScale = 5;
            float heightValue = heightScale * (float) heightMap[col * 4 + (row * numberOfCells * 4)] / 255.0;
            float heightValue1 = heightScale * (float) heightMap[(col + 1) * 4 + ((row+1) * numberOfCells * 4)] / 255.0;
            float heightValue2 = heightScale *  (float) heightMap[(col + 1) * 4 + (row * numberOfCells * 4)] / 255.0;
            float heightValue3 = heightScale * (float) heightMap[(col) * 4 + ((row + 1) * numberOfCells * 4)] / 255.0;

            // temp
//            heightValue = 0;
//            heightValue1 = 0; heightValue2 = 0; heightValue3= 0;
            // end temp

            float gap = 0.1;

            // Left top
            auto lt = glm::vec3 {col * cellSize * (1 + gap), heightValue, (row * cellSize) * (1 + gap) };
            positions.push_back( lt.x);
            positions.push_back(heightValue);
            positions.push_back(lt.z);



            // Right bottom
            auto rb = lt + glm::vec3{1, 0, 1};
            rb.y = heightValue1;
            positions.push_back(rb.x);
            positions.push_back(heightValue1);
            positions.push_back(rb.z);


            // Right Top
            auto rt = lt + glm::vec3(1, 0, 0);
            rt.y= heightValue2;
            positions.push_back(rt.x);
            positions.push_back(heightValue2);
            positions.push_back(rt.z);

            // Left bottom
            auto lb = lt + glm::vec3(0, 0, 1);
            lb.y = heightValue3;
            positions.push_back(lb.x);
            positions.push_back(heightValue3);
            positions.push_back(lb.z);

            //uvCreateFractioned(row, col);
            uvCreateTiled(row, col);

            // normal left top
            auto nlt = glm::normalize(glm::cross(lb -lt, rt - lt)) ;
            auto nrt = glm::normalize(glm::cross(lt-rt, rb -rt ));
            auto nlb = glm::normalize(glm::cross(rb - lb, lt - lb));
            auto nrb = glm::normalize(glm::cross( rt - rb, lb - rb));

            normals.push_back(nlt.x);normals.push_back(nlt.y);normals.push_back(nlt.z);
            normals.push_back(nrb.x);normals.push_back(nrb.y);normals.push_back(nrb.z);
            normals.push_back(nrt.x);normals.push_back(nrt.y);normals.push_back(nrt.z);
            normals.push_back(nlb.x);normals.push_back(nlb.y);normals.push_back(nlb.z);



        }
    }

    std::vector<uint32_t> indices;

    for (int row = 0; row < numberOfCells; row ++ ) {
        for (int col = 0; col < numberOfCells; col++) {

            uint32_t leftTop = (col * 4) + (row * numberOfCells * 4);
            uint32_t rightBot = leftTop + 1;
            uint32_t rightTop = leftTop + 2;
            uint32_t leftBot = leftTop + 3;

            glm::vec3 lt = { positions[leftTop*3], positions[(leftTop*3) + 1], positions[(leftTop*3)+2]};
            glm::vec3 rb = { positions[(rightBot*3)], positions[(rightBot*3) + 1], positions[(rightBot*3)+2]};
            glm::vec3 rt = { positions[(rightTop*3)], positions[(rightTop*3) + 1], positions[(rightTop*3)+2]};
            glm::vec3 lb = { positions[(leftBot*3)], positions[(leftBot*3) + 1], positions[(leftBot*3)+2]};

            indices.push_back(leftTop);
            indices.push_back(rightBot);
            indices.push_back(rightTop);
            indices.push_back(leftTop);
            indices.push_back(leftBot);
            indices.push_back(rightBot);
        }
    }

    // Now lets pack everything into a VAO

    vao = new VertexArrayObject();
    vao->bind();
    PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());

    vao->unbind();
}

void kx::MeshGrid::initWithSharedVertices(glm::vec3 position, int cellSize, int numberOfCells) {
    // We construct the data structure for the mesh grid,
    // that is we form a triangle mesh based on the given parameters.

    int numVerts = numberOfCells + 1;

    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;
    float uvFraction = 1.0 / (float) numberOfCells;

    // Here we can employ different uv creation strategies
    auto uvCreateFractioned = [uvFraction, &uvs] (int r, int c) -> void {
        uvs.push_back((float)c * uvFraction);
        uvs.push_back((float) r * uvFraction);
    };


    auto uvCreateTiled = [&uvs] (int r, int c) -> void {
        float grass_startU = 0.62;
        float grass_endU = 0.64;
        float grass_startV = 1- 0.02;
        float grass_endV = 1-0.015;

        float w_startU = 0.71;
        float w_endU = 0.72;
        float w_startV = 1- 0.02;
        float w_endV = 1-0.015;

        if (c % 2 == 0) {
            uvs.push_back(grass_startU);
        } else {
            uvs.push_back(grass_endU);
        }
        if (r % 2 == 0) {
            uvs.push_back(grass_startV);
        } else {
            uvs.push_back(grass_endV);
        }
    };

    for (int row = 0; row < numVerts; row++) {
        for (int col = 0; col < numVerts; col++) {
            positions.push_back( col * cellSize);
            positions.push_back(0);
            positions.push_back(-row * cellSize);

            //uvCreateFractioned(row, col);
            uvCreateTiled(row, col);

            normals.push_back(0);
            normals.push_back(1);
            normals.push_back(0);

        }
    }

    std::vector<uint32_t> indices;

    for (int row = 0; row < numberOfCells; row ++ ) {
        for (int col = 0; col < numberOfCells; col++) {

            uint32_t p1 = col + row * numVerts;
            uint32_t p2 = (col + 1) + row * numVerts;
            uint32_t p3 = (col + 1) + (row + 1) * numVerts;
            uint32_t p4 = col + (row + 1) * numVerts;

            indices.push_back(p1);
            indices.push_back(p2);
            indices.push_back(p3);
            indices.push_back(p1);
            indices.push_back(p3);
            indices.push_back(p4);
        }
    }

    // Now lets pack everything into a VAO

    vao = new VertexArrayObject();
    vao->bind();
    PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());

    vao->unbind();
}

void kx::MeshGrid::initWithSeparateVerticesAndTerrain(glm::vec3 vec1, int cellSize, kx::TerrainDescription *terrainDesc) {
    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;

    int numberOfCells = terrainDesc->getDimension();

    auto uvCreateTiled = [&uvs, numberOfCells, terrainDesc ] (int r, int c) -> void {
        float grass_startU = 0.62;
        float grass_endU = 0.64;
        float grass_startV = 1- 0.02;
        float grass_endV = 1-0.015;

        float sand_startU = 0.62;
        float sand_endU = 0.64;
        float sand_startV = 1- 0.02;
        float sand_endV = 1-0.015;

        float startU = 0.71;
        float endU = 0.72;
        float startV = 1- 0.35;
        float endV = 1-0.36;

        auto tile = terrainDesc->getTiles()->data()[c + r*numberOfCells];

        // (Salt)Water is implicit currently...
        if (tile->terrainType == TerrainType::Grass) {
            startU = grass_startU;
            endU = grass_endU;
            startV = grass_startV;
            endV = grass_endV;
        }
        else if (tile->terrainType == TerrainType::Sand) {
            startU = sand_startU;
            endU = sand_endU;
            startV = sand_startV;
            endV = sand_endV;
        }


        // left top
        uvs.push_back(startU);
        uvs.push_back(startV);

        // right bot
        uvs.push_back(endU);
        uvs.push_back(endV);

        // right top
        uvs.push_back(endU);
        uvs.push_back(startV);

        // left top
        uvs.push_back(startU);
        uvs.push_back(endV);

    };

    // We create 2 triangles per cell, i.e. a full quad.
    // So we can have separate uvs per cell.
    for (int row = 0; row < numberOfCells; row++) {
        for (int col = 0; col < numberOfCells; col++) {
            // Left top
            positions.push_back( col * cellSize);
            positions.push_back(0);
           // positions.push_back(-row * cellSize);
            positions.push_back(row * cellSize);

            // Right bottom
            positions.push_back((col + 1) * cellSize);
            positions.push_back(0);
            //positions.push_back((-row - 1) * cellSize);
            positions.push_back((row + 1) * cellSize);

            // Right Top
            positions.push_back((col + 1) * cellSize);
            positions.push_back(0);
            //positions.push_back((-row) * cellSize);
            positions.push_back((row) * cellSize);

            // Left bottom
            positions.push_back( col * cellSize);
            positions.push_back(0);
            //positions.push_back((-row - 1) * cellSize);
            positions.push_back((row + 1) * cellSize);

            //uvCreateFractioned(row, col);
            uvCreateTiled(row, col);

            for (int i = 0; i < 4; i++) {
                normals.push_back(0);
                normals.push_back(1);
                normals.push_back(0);
            }

        }
    }

    std::vector<uint32_t> indices;

    for (int row = 0; row < numberOfCells; row ++ ) {
        for (int col = 0; col < numberOfCells; col++) {

            uint32_t leftTop = (col * 4) + (row * numberOfCells * 4);
            uint32_t rightBot = leftTop + 1;
            uint32_t rightTop = leftTop + 2;
            uint32_t leftBot = leftTop + 3;

//            glm::vec3 lt = { positions[leftTop*3], positions[(leftTop*3) + 1], positions[(leftTop*3)+2]};
//            glm::vec3 rb = { positions[(rightBot*3)], positions[(rightBot*3) + 1], positions[(rightBot*3)+2]};
//            glm::vec3 rt = { positions[(rightTop*3)], positions[(rightTop*3) + 1], positions[(rightTop*3)+2]};
//            glm::vec3 lb = { positions[(leftBot*3)], positions[(leftBot*3) + 1], positions[(leftBot*3)+2]};

            indices.push_back(leftTop);
            indices.push_back(rightBot);
            indices.push_back(rightTop);

            indices.push_back(leftTop);

            indices.push_back(leftBot);
            indices.push_back(rightBot);
        }
    }

    // Now lets pack everything into a VAO

    vao = new VertexArrayObject();
    vao->bind();
    PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());

    vao->unbind();
}

enum Side {
    top,
    bottom,
    left,
    right,
    front,
    back
};

void storePlaneVertexAttributes(std::vector<float>& positions, std::vector<float>& uvs, std::vector<float>& normals, std::vector<uint32_t>& indices, Side side,
                                int col, int row, float height, int numberOfCells) {

//    int c = col;
//    int r = row;
    float gap = 0.35;

    glm::vec3 lt, rt, rb, lb;

    auto absoluteLt = glm::vec3(col * ( 1 + gap), height, row * ( 1+ gap)) ;

    if  (side == top) {
        lt = absoluteLt;
        rt = lt + glm::vec3{1, 0, 0};
        rb = lt + glm::vec3{1, 0, 1};
        lb = lt + glm::vec3{0, 0, 1};
    }

    else if  (side == left) {
        lt = glm::vec3(col * ( 1 + gap), height, row * ( 1+ gap)) ;
        rt = lt + glm::vec3{0, 0, 1};
        rb = lt + glm::vec3{0, -1, 1};
        lb = lt + glm::vec3{0, -1, 0};
    }

    else if  (side == right) {
        lt = absoluteLt + glm::vec3(1, 0, 1);
        rt = lt + glm::vec3{0, 0, -1};
        rb = lt + glm::vec3{0, -1, -1};
        lb = lt + glm::vec3{0, -1, 0};
    }

    else if  (side == front) {
        lt = absoluteLt + glm::vec3(0, 0, 1);
        rt = lt + glm::vec3{1, 0, 0};
        rb = lt + glm::vec3{1, -1, 0};
        lb = lt + glm::vec3{0, -1, 0};
    }
    else if  (side == back) {
        lt = absoluteLt + glm::vec3(1, 0, 0);
        rt = absoluteLt;
        rb = lt + glm::vec3{-1, -1, 0};
        lb = lt + glm::vec3{0, -1, 0};
    }

    positions.push_back(lt.x); positions.push_back(lt.y); positions.push_back(lt.z);
    positions.push_back(rt.x); positions.push_back(rt.y); positions.push_back(rt.z);
    positions.push_back(rb.x); positions.push_back(rb.y); positions.push_back(rb.z);
    positions.push_back(lb.x); positions.push_back(lb.y); positions.push_back(lb.z);

    for (int i = 0; i < 4; i++) {
        if (side == top) {
            normals.push_back(0);normals.push_back(1);normals.push_back(0);
        }
        else if (side == left) {
            normals.push_back(-1);normals.push_back(0);normals.push_back(0);
        }
        else if (side == right) {
            normals.push_back(1);normals.push_back(0);normals.push_back(0);
        }
        else if (side == front) {
            normals.push_back(0);normals.push_back(0);normals.push_back(1);
        }
        else if (side == back) {
            normals.push_back(0);normals.push_back(0);normals.push_back(-1);
        }

    }

    auto ult = glm::vec2(0, 0);
    auto urt = ult + glm::vec2(1, 0);
    auto urb = ult + glm::vec2(1,1);
    auto ulb = ult + glm::vec2(0, 1);

    uvs.push_back(ult.x); uvs.push_back(ult.y);
    uvs.push_back(urt.x); uvs.push_back(urt.y);
    uvs.push_back(urb.x); uvs.push_back(urb.y);
    uvs.push_back(ulb.x); uvs.push_back(ulb.y);


    uint32_t leftTop = (col * 4) + (row * numberOfCells * 20);
    uint32_t rightTop = leftTop + 1;
    uint32_t rightBot = leftTop + 2;
    uint32_t leftBot = leftTop + 3;

    indices.push_back(leftTop);
    indices.push_back(rightBot);
    indices.push_back(rightTop);
    indices.push_back(leftTop);
    indices.push_back(leftBot);
    indices.push_back(rightBot);
}

void kx::MeshGrid::initWithCubesAndHeightMap(glm::vec3 position, int cellSize, int numberOfCells, uint8_t *heightMap) {
    std::vector<float> positions;
    std::vector<float> uvs;
    std::vector<float> normals;
    std::vector<uint32_t> indices;

    for (int r = 0; r < numberOfCells; r++) {
        for (int c = 0; c < numberOfCells; c++) {
            float height = 10 * (heightMap[(c*4) + (r * 4 * numberOfCells)] / 255.0);

            // TODO consider position!


            storePlaneVertexAttributes(positions, uvs, normals,indices, Side::top, c, r, height, numberOfCells);
            storePlaneVertexAttributes(positions, uvs, normals,indices, Side::left, c, r, height, numberOfCells);
            storePlaneVertexAttributes(positions, uvs, normals,indices, Side::right, c, r, height, numberOfCells);
            storePlaneVertexAttributes(positions, uvs, normals,indices, Side::front, c, r, height, numberOfCells);
            storePlaneVertexAttributes(positions, uvs, normals,indices, Side::back, c, r, height, numberOfCells);



//            float gap = 0.25;
//            auto lt = glm::vec3(c * ( 1 + gap), height, r * ( 1+ gap)) ;
//            auto rt = lt + glm::vec3{1, 0, 0};
//            auto rb = lt + glm::vec3{1, 0, 1};
//            auto lb = lt + glm::vec3{0, 0, 1};
//
//            positions.push_back(lt.x); positions.push_back(lt.y); positions.push_back(lt.z);
//            positions.push_back(rt.x); positions.push_back(rt.y); positions.push_back(rt.z);
//            positions.push_back(rb.x); positions.push_back(rb.y); positions.push_back(rb.z);
//            positions.push_back(lb.x); positions.push_back(lb.y); positions.push_back(lb.z);
//
//            for (int i = 0; i < 4; i++) {
//                normals.push_back(0);normals.push_back(1);normals.push_back(0);
//            }
//
//            auto ult = glm::vec2(0, 0);
//            auto urt = ult + glm::vec2(1, 0);
//            auto urb = ult + glm::vec2(1,1);
//            auto ulb = ult + glm::vec2(0, 1);
//
//            uvs.push_back(ult.x); uvs.push_back(ult.y);
//            uvs.push_back(urt.x); uvs.push_back(urt.y);
//            uvs.push_back(urb.x); uvs.push_back(urb.y);
//            uvs.push_back(ulb.x); uvs.push_back(ulb.y);
//
//
//            uint32_t leftTop = (c * 4) + (r * numberOfCells * 4);
//            uint32_t rightTop = leftTop + 1;
//            uint32_t rightBot = leftTop + 2;
//            uint32_t leftBot = leftTop + 3;
//
//            indices.push_back(leftTop);
//            indices.push_back(rightBot);
//            indices.push_back(rightTop);
//            indices.push_back(leftTop);
//            indices.push_back(leftBot);
//            indices.push_back(rightBot);


        }
    }



    vao = new VertexArrayObject();
    vao->bind();
    PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());
    vao->unbind();
}
