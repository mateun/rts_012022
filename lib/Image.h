//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_IMAGE_H
#define KOENIG2022_1_IMAGE_H

#include <string>

namespace kx {
    class Image {
    public:
        Image(const std::string& fileName);
        Image(uint8_t* pixels, int w, int h);

        int getWidth();

        int getHeight();

        const void *getPixels();

    private:
        int w;
        int h;
        uint8_t* pixels;

    };

}


#endif //KOENIG2022_1_IMAGE_H
