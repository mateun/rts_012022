//
// Created by mgrus on 22/01/2022.
//

#include "Terrain.h"
#include "Program.h"
#include <string>
#include <map>
#include <glm/gtc/type_ptr.hpp>


extern std::map<std::string, kx::Program*>* getShaderStore();

// TODO remove this from here, it should not be in Terrain.cpp
void setPixel(uint8_t* pixel, float r, float g, float b, float a) {
    *pixel = r * 255;
    *(pixel + 1) = g * 255;
    *(pixel + 2) = b * 255;
    *(pixel + 3) = a * 255;
}

/**
 * The idea is to render a terrain
 * with fixed sized terrain chunks.
 * Every chunk is a pre-rendered texture.
 * If on the rare occasion, the terrain itself
 * would change (which may happen through
 * terraforming), the texture will be recreated.
 *
 * @param widthInTiles
 */
kx::Terrain::Terrain(int widthInTiles) {
    chunk1Pixels = (uint8_t*) malloc (512* 512*4);
    int pixel = 0;
    bool nextIsLight = false;
    bool nextIsLightVertical = false;
    int nextLightRow = 0;
    for (int r = 0; r < 512; r++) {
        for (int c = 0; c < 512; c++) {
            setPixel(&chunk1Pixels[pixel], 0.1, 0.6, 0.1, 1);

            if (nextIsLight) {
                setPixel(&chunk1Pixels[pixel], 0.4, 0.6, 0.4, 1);
                nextIsLight = false;
            }

            if (nextLightRow == r) {
                setPixel(&chunk1Pixels[pixel], 0.4, 0.6, 0.4, 1);
                nextIsLightVertical = false;
            }

            // Render some grid like lines
            if (c % 32 == 0 ) {
                setPixel(&chunk1Pixels[pixel], 0.1, 0.5, 0.1, 1);
                nextIsLight = true;
            }

            if (r % 32 == 0) {
                setPixel(&chunk1Pixels[pixel], 0.1, 0.5, 0.1, 1);
                nextIsLightVertical = true;
                nextLightRow = r+2;
            }





            pixel += 4;
        }
    }
    chunk1 = new kx::Texture(512, 512, chunk1Pixels);
    chunk1Sprite = new kx::Sprite(400, 300, chunk1);

    for (int i = 0; i < 3; i++) {
        for (int k = 0; k < 3; k++) {
            auto tc = new kx::Sprite(k * 512, i * 512, chunk1);
            terrainChunks.push_back(tc);
        }

    }
}



void kx::Terrain::debugUpdateTerrain() {

    erosionXEnd += 1 ;
    for (int i = erosionXStart; i < erosionXEnd; i+=4) {
       setPixel(&chunk1Pixels[i * 4], 1, 1, 0, 1);
        setPixel(&chunk1Pixels[i *4 + 512*4 ], 1, 1, 0, 1);
        setPixel(&chunk1Pixels[i *4+ 512 * 2 * 4 ], 1, 1, 0, 1);
    }

    chunk1->bind();
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
                    chunk1->getWidth(),
                    chunk1->getHeight(),
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    chunk1Pixels);
    chunk1->unbind();
    erosionXStart = erosionXEnd;



}

void kx::Terrain::render(kx::Camera *camera) {
    kx::Program* prog = (*getShaderStore())["default2D"];

    prog->use();
    ((Renderable2D*)chunk1Sprite)->getVertexArrayObject()->bind();
    chunk1->bind();

    for (auto s : terrainChunks) {
        s->getTexture()->bind();

        glm::mat4 ms= glm::scale(glm::mat4(1), {s->getTexture()->getWidth(), s->getTexture()->getHeight(), 1});

        // TODO: get screen dimensions from somewhere
        glm::mat4 mt = glm::translate(glm::mat4(1), {s->getPosition().x, s->getPosition().y, -10});
        glm::mat4 mw = mt * ms;

        glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(mw));
        glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(camera->getView()));
        glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(camera->getProj()));
        glUniform1i(10, 0);

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }


}


