//
// Created by mgrus on 22/01/2022.
//

#include "Texture.h"
#include <gl/glew.h>
#include "GLErrorHandling.h"
#include "stb_image.h"

kx::Texture::Texture(int w, int h) : width(w), height(h) {
    init();
    prepareStorage();
}

kx::Texture::Texture(int wh) : width(wh), height(wh) {
    init();
    prepareStorage();
}

kx::Texture::Texture(int w, int h, void *pixels) : width(w), height(h) {
    init();
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA, width, height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE, pixels);
    GL_ERROR_FAIL

}

kx::Texture::Texture(kx::Image *image) {
    width = image->getWidth();
    height = image->getHeight();
    init();
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA, width, height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE, image->getPixels());
}


void kx::Texture::init() {
    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);

//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);




}

void kx::Texture::prepareStorage() {
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
}

void kx::Texture::bind(int index) {

    GLenum bindpoint = GL_TEXTURE0;
    if (index == 1) {
        bindpoint = GL_TEXTURE1;
    }
    glActiveTexture(bindpoint);
    glBindTexture(GL_TEXTURE_2D, handle);
}

void kx::Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

int kx::Texture::getWidth() {
    return width;
}

int kx::Texture::getHeight() {
    return height;
}

void kx::Texture::updateData(uint8_t *pixels) {
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
}


void kx::DepthTexture::prepareStorage() {
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, width, height);
}

kx::DepthTexture::DepthTexture(int w, int h)  {
    this->width = w;
    this->height = h;
    init();
    prepareStorage();
}
