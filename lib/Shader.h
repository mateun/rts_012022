//
// Created by mgrus on 16/01/2022.
//

#ifndef KOENIG2022_1_SHADER_H
#define KOENIG2022_1_SHADER_H

#include <string>
#include <gl/glew.h>
#include <vector>

namespace kx {

    enum class ShaderType {
        VERTEX,
        FRAGMENT
    };

    class Shader {
    public:
        Shader(const std::string& source, ShaderType shaderType);

        int getHandle();

    private:
        GLuint handle;
    };

    class ShaderBuilder {
    public:
        ShaderBuilder(ShaderType type);
        ShaderBuilder* diffuseTexture();
        ShaderBuilder* attributePosition();
        ShaderBuilder* attributeUV();
        ShaderBuilder* attributeNormal();
        ShaderBuilder* uniformProjectionMatrix();
        ShaderBuilder* uniformViewMatrix();
        ShaderBuilder* uniformModelMatrix();
        Shader build();

    protected:
        void prepareSource();
        void buildVertexShader();
        void buildFragmentShader();

    private:
        ShaderType type;
        std::string source;
        std::vector<std::string> attributes;
        std::vector<std::string> uniforms;
        std::vector<std::string> attributesOut;
    };

}


#endif //KOENIG2022_1_SHADER_H
