//
// Created by mgrus on 23/01/2022.
//

#ifndef KOENIG2022_1_CONSOLE_H
#define KOENIG2022_1_CONSOLE_H


#include "Texture.h"
#include "Sprite.h"
#include "stb_truetype.h"
#include "Window.h"

namespace kx {

    void addBuilding(int x, int y);

    class Console {

    public:
        Console();
        void render();
        void update(float deltaTime, kx::WinEvent);
        void hide();

    private:
        void renderCursor();
        void renderText(const std::string& theText, bool resetCursor);
        void executeCommand(const std::string cmd);

    private:
        Texture* texture = nullptr;
        Sprite* sprite = nullptr;
        uint8_t* pixels = nullptr;
        uint8_t* font_pixels = nullptr;
        stbtt_fontinfo fontinfo = {};

        Texture* fontTexture = nullptr;
        int consoleEndY = 200;
        int consoleCurrentY = 0;

        std::string prompt = "$> ";
        std::string commandBuffer = "";

        // Cursor
        int cursorX = 0;
        int cursorY = 0;
        float blinkDuration = 0.5;
        float currentBlinkTime = 0;
        bool showCursor = true;
    };
}



#endif //KOENIG2022_1_CONSOLE_H
