//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_CAMERA_H
#define KOENIG2022_1_CAMERA_H

#include <glm/glm.hpp>
#include <vector>
#include "Window.h"

namespace kx {



    enum class CameraType {
        ORTHO,
        PERSPECTIVE
    };

    class Camera {
    public:
        Camera(glm::vec3 position);
        Camera(glm::vec3 position, glm::vec3 lookAt);
        void setPosition(glm::vec3 pos);
        void setLookAt(glm::vec3 at);
        glm::mat4 getProj();
        glm::mat4 getView();
        glm::vec3 getFwd();
        glm::vec3 getPos();

        void updateMovement(int foo);
        void updateMovement(std::vector<kx::WinEvent> winEvents);

    private:
        /**
         * This updates the view matrix
         * based on the current
         * pos and lookAt fields.
         */
        void updateView();
        void updatePosition(glm::vec3 p);

    protected:
        glm::mat4 proj;
        glm::mat4 view;
        glm::vec3 pos;
        glm::vec3 lookAt;
        glm::vec3 fwd;

    };

    struct OrthoData {
        float left,
        right,
        bottom,
        top,
        nearClip,
        farClip;
    };

    struct PerspectiveData {
        float fovyRad;
        float ar;
        float nearZ;
        float farZ;
    };

    class OrthoCamera : public Camera {
    public:
        OrthoCamera(glm::vec3 position, kx::OrthoData orthoData);
    };

    class PerspectiveCamera : public Camera {
    public:
        PerspectiveCamera(glm::vec3 position, kx::PerspectiveData data);
    };

};



#endif //KOENIG2022_1_CAMERA_H
