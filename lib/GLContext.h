//
// Created by mgrus on 17/01/2022.
//

#ifndef KOENIG2022_1_GLCONTEXT_H
#define KOENIG2022_1_GLCONTEXT_H

#include "Window.h"

namespace  kx {

    class GLContext {
    public:
        GLContext(Window* window);

    };
}


#endif //KOENIG2022_1_GLCONTEXT_H
