//
// Created by mgrus on 23/01/2022.
//

#include <cstdint>
#include <map>
#include "Console.h"
#include "Program.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
#include "Window.h"

extern void setPixel(uint8_t* pixel, float r, float g, float b, float a);
extern std::map<std::string, kx::Program*>* getShaderStore();
extern int screenWidth, screenHeight;

static unsigned char ttf_buffer[1<<20];
static unsigned char temp_font_bitmap[512*512];

kx::Console::Console() {
    pixels = (uint8_t*) malloc (512* 512*4);
    int pixel = 0;
    bool nextIsLight = false;
    bool nextIsLightVertical = false;
    int nextLightRow = 0;
    for (int r = 0; r < 512; r++) {
        for (int c = 0; c < 512; c++) {
            setPixel(&pixels[pixel], 0.1, 0.0, 0.0, 0.6);
            pixel += 4;
        }
    }

    texture = new kx::Texture(512, 512, pixels);
    sprite = new kx::Sprite(screenWidth / 2, 300, texture);

    // Init the font rendering
    fread(ttf_buffer, 1, 1<<20,
          fopen("c:/windows/fonts/consola.ttf", "rb"));
    stbtt_InitFont(&fontinfo, ttf_buffer, stbtt_GetFontOffsetForIndex(ttf_buffer,0));
    stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs
    stbtt_BakeFontBitmap(ttf_buffer,0, 16.0, temp_font_bitmap,512,512,32,96,cdata); // no guarantee this fits!

    font_pixels = (uint8_t*) malloc(512 * 512 * 4);
    memset(font_pixels, 0, 512*512*4);

    const char* text = prompt.c_str();
    float x = 5;
    float y = 10;


    memset(font_pixels, 0, 512*512*4);

    int ascent = 0;
    stbtt_GetFontVMetrics(&fontinfo, &ascent,0,0);
    float scale =stbtt_ScaleForPixelHeight(&fontinfo, 16);
    int baseline = (int) (ascent*scale);
    y = baseline + 10;
    while (*text) {
        int advance,lsb,x0,y0,x1,y1;
        int w=0, h=0, xoff=0, yoff=0;
        stbtt_GetCodepointHMetrics(&fontinfo, *text, &advance, &lsb);
        uint8_t* glyphbitmap = stbtt_GetCodepointBitmap(&fontinfo, 0, scale, *text, &w, &h, 0, &yoff);
        int gWidthInPixels = w;
        int gHeightInPixels = h;
        for (int r = 0; r <  gHeightInPixels; r++) {
            for (int c = 0; c  < gWidthInPixels; c++) {

                uint8_t valt = glyphbitmap[c + r * w];

                int startPixel = (((int)x+c) * 4) + (((int)y+r + yoff)* 512 * 4);
                //printf("start pixeL: %d rest: %d\n", startPixel, (startPixel % 4));
                setPixel(&font_pixels[startPixel], (float) valt / 255.0, (float) 1, (float) 1, (float) valt/ (float) 255.0);

            }
        }

        x += (advance * scale);
        if (*(text+1)) {
            x += scale * stbtt_GetCodepointKernAdvance(&fontinfo, *text, *(text + 1));
        }

        ++text;
    }


    fontTexture = new kx::Texture(512, 512, font_pixels);

    cursorX = x;
    cursorY = y;
}

void kx::Console::render() {

    std::string renderableText = prompt + commandBuffer;
    if (showCursor) {
        renderableText += "|";
    }
    renderText(renderableText.c_str(), false);

    if (consoleCurrentY < consoleEndY) {
        consoleCurrentY+=8;
    }
    (*getShaderStore())["default2D"]->use();
    ((Renderable2D*)sprite)->getVertexArrayObject()->bind();
    texture->bind();

    glm::mat4 ms= glm::scale(glm::mat4(1), {512, 512, 1});

    // TODO: get screen dimensions from somewhere
    glm::mat4 mt = glm::translate(glm::mat4(1), {screenWidth / 2, consoleCurrentY, -1.1});
    glm::mat4 mw = mt * ms;

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(mw));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(glm::lookAt(glm::vec3{0, 0, 5}, {0, 0, 0}, {0, 1, 0})));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(glm::ortho<float>(0, screenWidth, 0, screenHeight, 0.01, 100)));
    glUniform1i(10, 0);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    texture->unbind();

    mt = glm::translate(glm::mat4(1), {screenWidth / 2, consoleCurrentY, -1});
    mw = mt * ms;
    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(mw));

    fontTexture->bind();
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


}

void kx::Console::hide() {
    consoleCurrentY = 0;

}

void kx::Console::update(float deltaTime, kx::WinEvent winEvent) {


    if (winEvent.type == kx::WinEventType::TEXTINPUT) {
        commandBuffer += winEvent.key;
    }

    if (winEvent.type == kx::WinEventType::KEYDOWN) {
        if (winEvent.key == "RETURN") {
            executeCommand(commandBuffer);
        }

        if (winEvent.key == "BACKSPACE") {
            if (!commandBuffer.empty()) {
                commandBuffer.pop_back();
            }

        }
    }

    // Cursor blinking
    {
        currentBlinkTime += deltaTime;
        if (currentBlinkTime >= blinkDuration) {
            currentBlinkTime = 0;
            showCursor = !showCursor;
        }
    }

}



void kx::Console::renderText(const std::string& theText, bool resetCursor) {

    memset(font_pixels, 0, 512 * 512* 4);
    int cursorAtStartOfFunction = cursorX;
    int ascent = 0;
    stbtt_GetFontVMetrics(&fontinfo, &ascent,0,0);
    float scale =stbtt_ScaleForPixelHeight(&fontinfo, 16);
    int baseline = (int) (ascent*scale);

    const char* text = theText.c_str();

    float x = 5;
    float y = baseline + 10;

    while (*text) {
        int advance,lsb,x0,y0,x1,y1;
        int w=0, h=0, xoff=0, yoff=0;
        stbtt_GetCodepointHMetrics(&fontinfo, *text, &advance, &lsb);
        uint8_t* glyphbitmap = stbtt_GetCodepointBitmap(&fontinfo, 0, scale, *text, &w, &h, 0, &yoff);
        int gWidthInPixels = w;
        int gHeightInPixels = h;
        for (int r = 0; r <  gHeightInPixels; r++) {
            for (int c = 0; c  < gWidthInPixels; c++) {
                uint8_t valt = glyphbitmap[c + r * w];
                int startPixel = (((int)x+c) * 4) + (((int)y+r + yoff)* 512 * 4);
                //printf("start pixeL: %d rest: %d\n", startPixel, (startPixel % 4));
                setPixel(&font_pixels[startPixel], (float) valt / 255.0, (float) 1, (float) 1, (float) valt/ (float) 255.0);
            }
        }

        x += (advance * scale);
        if (*(text+1)) {
            x += scale * stbtt_GetCodepointKernAdvance(&fontinfo, *text, *(text + 1));
        }
        ++text;
    }

    if (resetCursor) {
        cursorX = cursorAtStartOfFunction;
    }

    fontTexture->bind();
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
                    fontTexture->getWidth(),
                    fontTexture->getHeight(),
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    font_pixels);
    fontTexture->unbind();

}

void kx::Console::renderCursor() {
    // TODO actually render the cursor blinking at the end of the current line

    /*if (showCursor) {
        renderText("_", true);
    } else {
        renderText(" ", true);
    }
     */
}

void kx::Console::executeCommand(const std::string cmd) {
    if (cmd.empty()) {
        // TODO print out an error string
        // below the entry line
        return;
    }

    std::string cmdCopy = commandBuffer;
    commandBuffer.clear();

    char c = 0;

    std::string commandWord = "";
    int index = 0;
    // parse the command word
    while (c != '(') {
        c = cmdCopy[index++];
        commandWord += c;

    }


    commandWord.pop_back();
    if (commandWord == "addbuilding") {
        std::string paramX = "";
        // parse the coords
        while (c != ',') {
            c = cmdCopy[index++];
            paramX += c;
        }
        paramX.pop_back();
        int pX = atoi(paramX.c_str());

        std::string paramY = "";
        while (c != ')') {
            c = cmdCopy[index++];
            paramY += c;
        }
        paramY.pop_back();
        int pY = atoi(paramY.c_str());

        addBuilding(pX, pY);

    }





    // TODO
    // first parse the command, do we actually understand it?
    // next execute it...
}

void kx::addBuilding(int x, int y) {
    printf("in the addbuilding command! %d/%d\n", x, y);
}
