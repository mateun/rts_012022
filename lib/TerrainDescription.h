//
// Created by mgrus on 13.03.2022.
//

#ifndef KOENIG2022_1_TERRAINDESCRIPTION_H
#define KOENIG2022_1_TERRAINDESCRIPTION_H

#include <glm/glm.hpp>
#include <string>
#include <vector>

namespace kx {

    enum TerrainType {
        Grass,
        Sand,
        WaterSalt,
        WaterSweet,
        Mud,
        Rock,
        Swamp,
    };

    class TerrainTile {
    public:
        TerrainType terrainType;
        glm::vec2 position;

    };

    class TerrainDescription {
    public:
        TerrainDescription(int dimension);
        std::vector<TerrainTile*>* getTiles();

        // Writes an image to the given path where each pixel
        // is representing the terrain type.
        void writeDebugImgge(const std::string& outputFilePath);

        // Returns the raw rgba bytes of the terrain, each pixel representing
        // one tile.
        uint8_t * getDebugImage();

        int getDimension();

    private:
        std::vector<TerrainTile*>* tiles;
        int _dimension;
    };

}




#endif //KOENIG2022_1_TERRAINDESCRIPTION_H
