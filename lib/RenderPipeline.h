//
// Created by mgrus on 30/01/2022.
//

#ifndef KOENIG2022_1_RENDERPIPELINE_H
#define KOENIG2022_1_RENDERPIPELINE_H

#include <vector>
#include <glm/glm.hpp>
#include "Window.h"
#include "Texture.h"
#include "Program.h"
#include "VertexArrayObject.h"
#include "VertexBuffer.h"
#include "Camera.h"

/**
 * The idea of the RenderPipeline is to encapsulate all necessary things to
 * render geometry.
 * We want to make it easy to combine different rendering setups in your program
 * with this coarse grained interface.
 * It offers the possibility to provide settings for the
 * - provide the geometry
 * - the texture(s) to use (diffuse, normal)
 * - the shader program
 * - current viewport to use
 * - whether to render to a texture (framebuffer)
 *
 *
 */
namespace kx {


    class RenderPipeline {
    public:
        virtual void setVertexPositions(std::vector<float> positions) = 0;
        virtual void setVertexUVs(std::vector<float> uvs) = 0;
        virtual void renderTriangles(glm::mat4 proj, glm::mat4 view, glm::mat4 world) = 0;
        virtual void renderTriangles(glm::mat4 world) = 0;
        virtual void renderLines(glm::mat4 proj, glm::mat4 view, glm::mat4 world) = 0;
        virtual void renderLines(glm::mat4 world) = 0;
        virtual void setCamera(Camera* cam) { this->camera = cam;};

    protected:
        Camera *camera = nullptr;
    };

    class SWRenderPipeline : public RenderPipeline {

    public:
        SWRenderPipeline(kx::WindowPlain* window);
        virtual void setVertexPositions(std::vector<float> positions) override;
        virtual void setVertexNormals(std::vector<float> normals);
        void setVertexUVs(std::vector<float> uvs) override {}
        virtual void setIndicies(std::vector<uint32_t> indices);
        virtual void renderTriangles(glm::mat4 proj, glm::mat4 view, glm::mat4 world) override;
        void renderTriangles(glm::mat4 world) override {  };
        virtual void renderLines(glm::mat4 proj, glm::mat4 view, glm::mat4 world) override;
        void renderLines(glm::mat4 world) override {}
        void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

    public:
        // TODO remove this?!
        // This was just meant as a temporary debug information
        // in the default software render pipeline
        std::vector<glm::vec4> getPositionsNDC();

    private:
        std::vector<glm::vec4> positionsBeforeDivide;
        std::vector<glm::vec4> positionsNDC;
        std::vector<glm::vec3> positionsClipped;
        std::vector<glm::vec3> positionsScreen;
        uint8_t colRed, colGreen, colBlue, colAlpha;
        WindowPlain *window;

    protected:
        std::vector<float> positions;
        std::vector<float> normals;
        std::vector<float> uvs;



    };

    struct Viewport {
        int w;
        int h;
    };

    struct Color {
        float r, g, b, a;
        float* data() {
            return &r;
        }
    };

    class PipelineState {

    public:
        void setViewport(Viewport vp);
        Viewport getViewport() { return viewport;}
        void enableWireFrame();
        void disableWireFrame();
        void enableTransparancy();
        void disableTransparancy();
        bool isRenderransparent() { return transparency;}
        bool isRenderWireframe() { return wireFrame;}
        void enableColor(Color col);
        void disableColor();
        Color getColor() { return color;}
        bool isUseTexture() { return useTexture;}

    private:
        Viewport viewport;
        bool wireFrame = false;
        bool transparency = false;
        bool useTexture = true;

        Color color = {};
    };

    class GLPipelineData {

    public:

        bool isValid() {
            return shaderProgram && camera && !initialPositions.empty() && !initialIndices.empty() && pipelineState;
        }

        PipelineState* pipelineState = nullptr;
        Program* shaderProgram = nullptr;
        Camera* camera = nullptr;
        PipelineState* state = nullptr;
        Texture* initialDiffuseTexture = nullptr;
        std::vector<float> initialPositions;
        std::vector<float> initialUVs;
        std::vector<uint32_t> initialIndices;


    };

    class GLPipeline : public  RenderPipeline {
    public:
        GLPipeline(GLPipelineData* data);
        GLPipeline();

        void setShaderProgram(Program* program);
        void setVertexPositions(std::vector<float> positions) override;
        void setVertexUVs(std::vector<float> uvs) override;
        void setIndicies(std::vector<uint32_t> indices);
        void renderTriangles(glm::mat4 proj, glm::mat4 view, glm::mat4 world) override;
        void renderTriangles(glm::mat4 world) override;
        void renderLines(glm::mat4 proj, glm::mat4 view, glm::mat4 world) override;
        void renderLines(glm::mat4 world) override;
        void setTexture(Texture* t);
        void setNormalMap(Texture* t);

    private:
        void initVAO();
        bool checkRenderPrerequisites();

        WindowGL* getWindow();
        Program* getShaderProgram();
        VertexArrayObject* getVAO();


    private:
        GLPipelineData* pipelineData = nullptr;
        PipelineState* state = nullptr;

        Program* shaderProgram = nullptr;
        Texture* diffuseTexture = nullptr;
        Texture* normalMap = nullptr;
        VertexArrayObject* vao = nullptr;


    };

}



#endif //KOENIG2022_1_RENDERPIPELINE_H
