//
// Created by mgrus on 16/01/2022.
//

#include "Shader.h"
#include <gl/glew.h>
#include <vector>

kx::Shader::Shader(const std::string &source, kx::ShaderType shaderType) {
    GLenum glshadertype = GL_VERTEX_SHADER;
    switch(shaderType) {
        case kx::ShaderType::FRAGMENT: glshadertype = GL_FRAGMENT_SHADER;
    };

    handle = glCreateShader(glshadertype);
    const GLchar* cs = source.c_str();
    glShaderSource(handle, 1, &cs, nullptr);
    glCompileShader(handle);

    GLint compileStatus;
    glGetShaderiv(handle, GL_COMPILE_STATUS, &compileStatus);
    if (GL_FALSE == compileStatus) {
        GLint logSize = 0;
        glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetShaderInfoLog(handle, logSize, &logSize, &errorLog[0]);
        printf(&errorLog[0]);
        glDeleteShader(handle);
        exit(1);
    }

}

int kx::Shader::getHandle() {
    return handle;
}

kx::ShaderBuilder::ShaderBuilder(kx::ShaderType type) {
    this->type = type;


}

void kx::ShaderBuilder::prepareSource() {


}

kx::ShaderBuilder *kx::ShaderBuilder::diffuseTexture() {
    return this;
}

kx::ShaderBuilder *kx::ShaderBuilder::attributePosition() {
    attributes.push_back("layout(location = 0) in vec3 pos;\n");
    return this;
}

kx::Shader kx::ShaderBuilder::build() {
    if (type == kx::ShaderType::VERTEX) {
        buildVertexShader();
    }

    printf("source: %s\n", source.c_str());

    return Shader(source, type);
}

void kx::ShaderBuilder::buildVertexShader() {

    source = "";
    std::string header = "#version 430\n";
    source += header;

    for (auto a : attributes) {
        source += a;
    }

    for (auto u : uniforms) {
        source += u;
    }

    // TODO body
}

kx::ShaderBuilder *kx::ShaderBuilder::attributeUV() {
    attributes.push_back("layout(location = 1) in vec2 uv;\n");
    attributesOut.push_back("out vec2 fs_uv;\n");
    return this;

}

kx::ShaderBuilder *kx::ShaderBuilder::attributeNormal() {

    return this;
}
