//
// Created by mgrus on 18/01/2022.
//

#include "VertexArrayObject.h"
#include <gl/glew.h>

kx::VertexArrayObject::VertexArrayObject() {
    glGenVertexArrays(1, &handle);
}

void kx::VertexArrayObject::bind() {
    glBindVertexArray(handle);
}

void kx::VertexArrayObject::unbind() {
    glBindVertexArray(0);

}

int kx::VertexArrayObject::getNumberOfIndices() {
    return numberOfIndices;
}

void kx::VertexArrayObject::setNumberOfIndices(int val) {
    numberOfIndices = val;
}
