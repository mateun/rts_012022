//
// Created by mgrus on 30/01/2022.
//

#include "RenderPipeline.h"
#include <SDL.h>

void kx::SWRenderPipeline::setVertexPositions(std::vector<float> pos) {
    this->positions = pos;
}

void kx::SWRenderPipeline::renderLines(glm::mat4 proj, glm::mat4 view, glm::mat4 world) {
    positionsNDC.clear();
    positionsScreen.clear();

    for (int i = 0; i < positions.size(); i+=3) {
        float x = positions[i];
        float y = positions[i+1];
        float z = positions[i+2];

        glm::vec4 pvec = glm::vec4(x, y, z, 1);
        auto pndc = proj * view * world * pvec;

        positionsBeforeDivide.push_back(pndc);

        pndc.x /= pndc.w;
        pndc.y /= pndc.w;
        pndc.z /= pndc.w;
        positionsNDC.push_back(pndc);
    }

    for (int i = 0; i < positionsBeforeDivide.size();i+=2) {
        glm::vec4 pointA = positionsBeforeDivide[i];
        glm::vec4 pointB = positionsBeforeDivide[i+1];

        // We only clip against nearZ
        // for the moment,
        // as this causes the most troubles
        // if omitted. Points would get
        // projected in front even though they are back
        // due to the sign switch after perspective divide.
        float nearZ = 1;
        if (pointA.z < nearZ && pointB.z < nearZ) {
            // fully reject as both points are behind the camera
        }

        if (pointA.w > nearZ && pointB.w < nearZ) {
            // Clip nearZ
            printf("clipping point B against the nearZ plane!\n");
        }

    }

    for (auto pndc : positionsNDC) {
        int xScreen = ((pndc.x )+ 1) * 400.0;
        int yScreen = 600 -  ((pndc.y ) + 1) * 300.0;
        positionsScreen.push_back({xScreen, yScreen, pndc.z});
    }

    for (int i = 0; i < positionsScreen.size();i+=2) {
        glm::vec3 pointA = positionsScreen[i];
        glm::vec3 pointB = positionsScreen[i+1];


        window->renderLine(pointA.x, pointA.y, pointB.x, pointB.y,
                           colRed, colGreen, colBlue, colAlpha);

        window->renderPoint(pointA.x, pointA.y);
        window->renderPoint(pointB.x, pointB.y);

    }

}


void kx::SWRenderPipeline::renderTriangles(glm::mat4 proj, glm::mat4 view, glm::mat4 world) {

    positionsNDC.clear();
    positionsScreen.clear();

    for (int i = 0; i < positions.size(); i+=3) {
        float x = positions[i];
        float y = positions[i+1];
        float z = positions[i+2];

        glm::vec4 pvec = glm::vec4(x, y, z, 1);
        auto pndc = proj * view * world * pvec;
        pndc.x /= pndc.w;
        pndc.y /= pndc.w;
        pndc.z /= pndc.w;
        positionsNDC.push_back(pndc);
    }


    for (auto pndc : positionsNDC) {
        int xScreen = ((pndc.x )+ 1) * 400.0;
        int yScreen = 600 -  ((pndc.y ) + 1) * 300.0;

        //printf("xScreen: %d/%d\n", xScreen, yScreen);
        positionsScreen.push_back({xScreen, yScreen, pndc.z});

        //window->renderPoint(xScreen, yScreen);
    }

    for (int i = 0; i < positionsScreen.size();i+=3) {
        glm::vec3 pointA = positionsScreen[i];
        glm::vec3 pointB = positionsScreen[i+1];
        glm::vec3 pointC = positionsScreen[i+2];

        window->renderLine(pointA.x, pointA.y, pointB.x, pointB.y,
                           colRed, colGreen, colBlue, colAlpha);
        window->renderLine(pointB.x, pointB.y, pointC.x, pointC.y,
                           colRed, colGreen, colBlue, colAlpha);
        window->renderLine(pointC.x, pointC.y, pointA.x, pointA.y,
                           colRed, colGreen, colBlue, colAlpha);

        window->renderPoint(pointA.x, pointA.y);
        window->renderPoint(pointB.x, pointB.y);
        window->renderPoint(pointC.x, pointC.y);


    }

}

std::vector<glm::vec4> kx::SWRenderPipeline::getPositionsNDC() {
    return positionsNDC;
}

kx::SWRenderPipeline::SWRenderPipeline(kx::WindowPlain* win) : window(win){

}

void kx::SWRenderPipeline::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    colRed = r;
    colGreen = g;
    colBlue = b;
    colAlpha = a;
}




