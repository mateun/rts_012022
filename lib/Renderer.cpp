//
// Created by mgrus on 05/02/2022.
//

#include "Renderer.h"
#include <glm/gtc/type_ptr.hpp>


void kx::Renderer::renderWireFrame(kx::RenderData renderData) {

}

void kx::Renderer::renderTextured(kx::RenderData renderData, kx::DepthTexture* shadowDepthTexture) {

    if (renderData.transparent) {
        glEnable(GL_BLEND);
    } else {
        glDisable(GL_BLEND);
    }

    renderData.prog->use();
    renderData.vao->bind();

    renderData.prog->setBoolUniform(("flipUV"), renderData.flipUV);
    renderData.prog->setBoolUniform("useTexture", true);
    renderData.prog->setBoolUniform("isLit", renderData.lit);
    if (renderData.lit) {
        renderData.prog->setVec3Uniform("directionalLightPos", renderData.directionalLightPos);
        if (shadowDepthTexture || renderData.shadowTexture) {
            renderData.prog->setMat4Uniform("lightProj", renderData.lightcam->getProj());
            renderData.prog->setMat4Uniform("lightView", renderData.lightcam->getView());
            renderData.shadowTexture ? renderData.shadowTexture->bind(1) : shadowDepthTexture->bind(1);
        }
    }
    renderData.diffuseTexture->bind();
    renderData.prog->setBoolUniform("tile", false);

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(renderData.world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(renderData.cam->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(renderData.cam->getProj()));

    glDrawElements(GL_TRIANGLES, renderData.vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0);

}

void kx::Renderer::renderShadowPassInstanced(kx::RenderData renderData, glm::mat4* worldMatrices, int wmSize) {
    renderData.prog->use();
    renderData.vao->bind();
    renderData.prog->setBoolUniform("tile", false);

    renderData.prog->setBoolUniform("isLit", renderData.lit);
    renderData.prog->setBoolUniform("useTexture", false);
    renderData.prog->setBoolUniform("instancedRendering", true);

    // Set world matrices
    // TODO unify with non-shadow pass, should be identical functionality.
    {
        // We need to assign the world matrix "instance" attribute here
        unsigned int buffer;
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, wmSize * sizeof(glm::mat4), worldMatrices, GL_STATIC_DRAW);

        // vertex attributes
        std::size_t vec4Size = sizeof(glm::vec4);
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
        glEnableVertexAttribArray(7);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
        glEnableVertexAttribArray(8);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);
    }

   // glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(renderData.world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(renderData.lightcam->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(renderData.lightcam->getProj()));
    glDrawElementsInstanced(GL_TRIANGLES, renderData.vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0,  wmSize);
    renderData.prog->setBoolUniform("instancedRendering", false);
}

void kx::Renderer::renderShadowPass(kx::RenderData renderData) {
    renderData.prog->use();
    renderData.vao->bind();
    renderData.prog->setBoolUniform("tile", false);

    renderData.prog->setBoolUniform("isLit", renderData.lit);
    renderData.prog->setBoolUniform("useTexture", false);

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(renderData.world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(renderData.lightcam->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(renderData.lightcam->getProj()));
    glDrawElements(GL_TRIANGLES, renderData.vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0);
}

void kx::Renderer::renderSingleColor(kx::RenderData renderData, kx::DepthTexture* shadowDepthTexture) {
    if (renderData.transparent) {
        glEnable(GL_BLEND);
    } else {
        glDisable(GL_BLEND);
    }

    renderData.prog->use();
    renderData.vao->bind();

    renderData.prog->setVec4Uniform("staticColor", renderData.color);
    renderData.prog->setBoolUniform("flipUV", renderData.flipUV);
    renderData.prog->setBoolUniform("useTexture", false);
    renderData.prog->setBoolUniform("tile", false);
    renderData.prog->setBoolUniform("isLit", renderData.lit);
    if (renderData.lit) {
        renderData.prog->setVec3Uniform("directionalLightPos", renderData.directionalLightPos);
       if (shadowDepthTexture || renderData.shadowTexture) {
            renderData.prog->setMat4Uniform("lightProj", renderData.lightcam->getProj());
            renderData.prog->setMat4Uniform("lightView", renderData.lightcam->getView());
            // Seems this is never unbound and therefore shadow texture is always bound after we come here for the first time!
            renderData.shadowTexture ? renderData.shadowTexture->bind(1) : shadowDepthTexture->bind(1);
        }
    }

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(renderData.world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(renderData.cam->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(renderData.cam->getProj()));

    if (renderData.wireFrame) {
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }

    glDrawElements(renderData.primitiveType, renderData.vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0);

    if (renderData.wireFrame) {
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }

}

void kx::Renderer::renderTexturedInstanced(kx::RenderData renderData, glm::mat4* worldMatrices, int wmSize, kx::DepthTexture *shadowDepthTexture) {
    if (renderData.transparent) {
        glEnable(GL_BLEND);
    } else {
        glDisable(GL_BLEND);
    }

    renderData.prog->use();
    renderData.vao->bind();

    // Setting the instance matrices
    {

        // We need to assign the world matrix "instance" attribute here
        unsigned int buffer;
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, wmSize * sizeof(glm::mat4), worldMatrices, GL_STATIC_DRAW);

        // vertex attributes
        std::size_t vec4Size = sizeof(glm::vec4);
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);


    }

    renderData.prog->setBoolUniform(("flipUV"), renderData.flipUV);
    renderData.prog->setBoolUniform("useTexture", true);
    renderData.prog->setBoolUniform("isLit", renderData.lit);
    renderData.prog->setBoolUniform("instancedRendering", true);
    if (renderData.lit) {
        renderData.prog->setVec3Uniform("directionalLightPos", renderData.directionalLightPos);
        if (shadowDepthTexture || renderData.shadowTexture) {
            renderData.prog->setMat4Uniform("lightProj", renderData.lightcam->getProj());
            renderData.prog->setMat4Uniform("lightView", renderData.lightcam->getView());
            renderData.shadowTexture ? renderData.shadowTexture->bind(1) : shadowDepthTexture->bind(1);
        }
    }
    renderData.diffuseTexture->bind();
    renderData.prog->setBoolUniform("tile", false);

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(renderData.world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(renderData.cam->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(renderData.cam->getProj()));
    glDrawElementsInstanced(GL_TRIANGLES, renderData.vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0, wmSize);
    renderData.prog->setBoolUniform("instancedRendering", false);

}



