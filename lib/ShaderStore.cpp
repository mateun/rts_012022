//
// Created by mgrus on 22/01/2022.
//

#include "ShaderStore.h"
#include "Program.h"
#include <map>
#include <string>

static std::map<std::string, kx::Program*>* shaderStore = nullptr;

std::map<std::string, kx::Program*>* getShaderStore() {
    if (!shaderStore) {
        shaderStore = new std::map<std::string, kx::Program*>;
    }

    return shaderStore;
}




