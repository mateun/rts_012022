//
// Created by mgrus on 16/01/2022.
//

#include <xstring>
#include "Window.h"
#include "SDL.h"
#include "GLContext.h"

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")

kx::Window::Window(int w, int h, bool fullscreen) :w(w), h(h), fullScreen(fullscreen)  {
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        SDL_Log("error during SDL init: %s", SDL_GetError());
        exit(1);
    }

    SDL_DisplayMode dm;
    SDL_GetCurrentDisplayMode(0, &dm);
    this->screenResX = dm.w;
    this->screenResY = dm.h;

}

kx::WindowPlain::WindowPlain(int w, int h, bool fullscreen) : Window(w, h, fullscreen) {

}



void getCharFromKeyCode(SDL_Keycode keyCode, kx::WinEvent& e) {



    if (keyCode == SDLK_ESCAPE) {
        e.key = "ESCAPE";
    }
    else if (keyCode == SDLK_F2) {
        e.key = "F2";
    }

    else if (keyCode == SDLK_LEFT) {
        e.key = "LEFT";
    }
    else if (keyCode == SDLK_RIGHT) {
        e.key =  "RIGHT";
    }
    else if (keyCode == SDLK_UP) {
        e.key =  "UP";
    }
    else if (keyCode == SDLK_DOWN) {
        e.key = "DOWN";
    }
    else if (keyCode == SDLK_KP_PLUS) {
        e.key = "PLUS";
    }
    else if (keyCode == SDLK_KP_MINUS) {
        e.key = "MINUS";
    }
    else if (keyCode == SDLK_KP_4) {
        e.key = "KP_4";
    }
    else if (keyCode == SDLK_KP_6) {
        e.key = "KP_6";
    }
    else if (keyCode == SDLK_KP_8) {
        e.key = "KP_8";
    }
    else if (keyCode == SDLK_KP_2) {
        e.key = "KP_2";
    }
    else if (keyCode == SDLK_c) {
        e.key = 'c';
    }
    else if (keyCode == SDLK_a) {
        e.key = 'a';
    }
    else if (keyCode == SDLK_b) {
        e.key = 'b';
    }
    else if (keyCode == SDLK_d) {
        e.key = 'd';
    }
    else if (keyCode == SDLK_e) {
        e.key = 'e';
    }
    else if (keyCode == SDLK_f) {
        e.key = 'f';
    }
    else if (keyCode == SDLK_g) {
        e.key = "g";
    }
    else if (keyCode == SDLK_h) {
        e.key = 'h';
    }
    else if (keyCode == SDLK_i) {
        e.key = 'i';
    }
    else if (keyCode == SDLK_j) {
        e.key = 'j';
    }
    else if (keyCode == SDLK_k) {
        e.key = 'k';
    }
    else if (keyCode == SDLK_l) {
        e.key = 'l';
    }
    else if (keyCode == SDLK_m) {
        e.key = 'm';
    }
    else if (keyCode == SDLK_n) {
        e.key = 'n';
    }
    else if (keyCode == SDLK_o) {
        e.key = 'o';
    }
    else if (keyCode == SDLK_p) {
        e.key = 'q';
    }
    else if (keyCode == SDLK_q) {
        e.key = 'q';
    }
    else if (keyCode == SDLK_r) {
        e.key = 'r';
    }
    else if (keyCode == SDLK_s) {
        e.key = 's';
    }
    else if (keyCode == SDLK_t) {
        e.key = 't';
    }
    else if (keyCode == SDLK_u) {
        e.key = 'u';
    }
    else if (keyCode == SDLK_v) {
        e.key = 'v';
    }
    else if (keyCode == SDLK_w) {
        e.key = 'w';
    }
    else if (keyCode == SDLK_x) {
        e.key = 'x';
    }
    else if (keyCode == SDLK_y) {
        e.key = 'y';
    }
    else if (keyCode == SDLK_z) {
        e.key = 'z';
    }
    else if (keyCode == SDLK_RETURN) {
        e.key = "RETURN";
    }
    else if (keyCode == SDLK_SPACE) {
        e.key = "SPACE";
    }
    else if (keyCode == SDLK_BACKSPACE) {
        e.key = "BACKSPACE";
    }
    else if (keyCode == SDLK_DELETE) {
        e.key = "DELETE";
    }
    else if (keyCode == SDLK_F11) {
        e.keyCode = kx::KeyCode::f11;

    }

    else if (keyCode == SDLK_LEFTPAREN) {
        e.key = "(";
    }
    else if (keyCode == SDLK_RIGHTPAREN) {
        e.key = ")";
    }
}

kx::WinEvent kx::Window::pollWindow() const {

    kx::WinEventType t = kx::WinEventType::NONE;
    std::string keyPressed = "";
    kx::WinEvent winEvent;
    SDL_Event e;
    SDL_PollEvent(&e);
    if (e.type == SDL_QUIT) {
        t = kx::WinEventType::EXIT;
    }
    if (e.type == SDL_KEYDOWN) {
        t = kx::WinEventType::KEYDOWN;
        getCharFromKeyCode(e.key.keysym.sym, winEvent);
    }

    if (e.type == SDL_MOUSEBUTTONUP) {
        t = kx::WinEventType::MOUSEUP;
        if (e.button.button == SDL_BUTTON_LEFT) {
            winEvent.key = "LEFT";
        } else if (e.button.button == SDL_BUTTON_MIDDLE) {
            winEvent.key = "MIDDLE";
        } else {
            winEvent.key = "RIGHT";
        }
    }

    if (e.type == SDL_TEXTINPUT) {
        t = kx::WinEventType::TEXTINPUT;
        winEvent.key = e.text.text;
    }

    winEvent.type = t;

    return winEvent;
}

void kx::WindowPlain::present() {
    SDL_RenderPresent(renderer);
}




void *kx::Window::getHandle() {
    return win;
}

kx::WinDimension kx::Window::getDimension() {
    return {w, h};
}


void kx::Window::clearBackbuffer(float r, float g, float b, float a) {
    // TODO implement with render api
}

void kx::Window::init() {
    uint32_t windowFlags = provideWindowFlags();

    win = SDL_CreateWindow("game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, windowFlags);

    if (!win) {
        SDL_Log("no window! %s", SDL_GetError());
        exit(1);
    }
}

bool kx::Window::isFullscreen() {
    return fullScreen;
}

uint32_t kx::WindowPlain::provideWindowFlags() {
    uint32_t flags = SDL_WINDOW_SHOWN;

    if (fullScreen) {
        flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    return flags;
}

void kx::WindowPlain::clearBackbuffer(float r, float g, float b, float a) {
    SDL_SetRenderDrawColor(renderer, r*255, g*255, b*255, a*255);
    SDL_RenderClear(renderer);
}

void kx::WindowPlain::init() {
    Window::init();
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);


}

void kx::WindowPlain::renderPoint(int x, int y) {
    SDL_SetRenderDrawColor(renderer, 255, 2,2, 255);
    SDL_Rect r;
    r.x=x-1;
    r.y = y-1;
    r.w = 2;
    r.h = 2;
    //SDL_RenderDrawPoint(renderer, x, y);
    SDL_RenderFillRect(renderer, &r);
}

void kx::WindowPlain::renderLine(int xs, int xy, int endX, int endY, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    SDL_SetRenderDrawColor(renderer, r, g,b, a);
    SDL_RenderDrawLine(renderer, xs, xy, endX, endY);
}


