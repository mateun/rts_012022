//
// Created by mgrus on 22/01/2022.
//

#ifndef KOENIG2022_1_SCENE_H
#define KOENIG2022_1_SCENE_H

#include "Camera.h"
#include "Program.h"
#include "VertexArrayObject.h"
#include "Texture.h"
#include <vector>

namespace  kx {

    class Renderable {
    public:
        virtual glm::mat4 getWorldTransform() = 0;
        virtual VertexArrayObject* getVertexArrayObject() = 0;
        virtual Texture* getTexture() = 0;

    };

    class Renderable2D : public Renderable {
    public:
        virtual bool isTile() = 0;
        virtual float getTileOffsetLeft() = 0;
        virtual float getTileWidth() = 0;
        virtual float getTileOffsetBottom() = 0;
        virtual float getTileHeight() = 0;
    };

    class Scene {
    public:
        void setCamera(Camera* camera);
        void addRenderable(Renderable2D* renderable);
        void removeRenderable(Renderable2D* renderable);
        void render(Program* program);

    protected:
        Camera* cam;
        std::vector<Renderable2D*> renderables2D;

    };



};





#endif //KOENIG2022_1_SCENE_H
