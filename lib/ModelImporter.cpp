//
// Created by mgrus on 06/02/2022.
//

#include "ModelImporter.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector>
#include <cmath>
#include "stb_image.h"
#include "Texture.h"
#include "VertexBuffer.h"

#pragma comment(lib, "assimp-vc142-mt.lib")

static inline glm::mat4 ConvertMatrixToGLMFormat(const aiMatrix4x4& from)
{
    glm::mat4 to;
    //the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
    to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
    to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
    to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
    to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;
    return to;
}

static inline glm::vec3 GetGLMVec(const aiVector3D& vec)
{
    return glm::vec3(vec.x, vec.y, vec.z);
}

static inline glm::quat GetGLMQuat(const aiQuaternion& pOrientation)
{
    return glm::quat(pOrientation.w, pOrientation.x, pOrientation.y, pOrientation.z);
}


kx::VertexArrayObject *kx::ModelImporter::importFromFile(const std::string &modelFileName) {
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(modelFileName,
                                                 aiProcess_Triangulate |
                                                 aiProcess_CalcTangentSpace
                //	| aiProcess_FlipUVs
        );

        if (scene == nullptr) {
            printf("error in import of file %s\n", modelFileName.c_str());
            exit(1);
        }

        //for (int im = 0; im < scene->mNumMeshes; im++) {

            std::vector<float> positions;
            std::vector<float> uvs;
            std::vector<float> normals;
            std::vector<uint32_t> indices;
            std::vector<float> tangents;
            std::vector<float> biTangents;
            std::vector<float> weights;
            std::vector<int> boneIds;

            // We only import the first mesh currently!
            // TODO enable multiple meshes to be imported!
            auto* mesh = scene->mMeshes[0];

            for (int x = 0; x < mesh->mNumVertices; x++)
            {
                aiVector3D pos = mesh->mVertices[x];
                positions.push_back(pos.x);
                positions.push_back(pos.y);
                positions.push_back(pos.z);

                if (mesh->HasNormals())
                {
                    aiVector3D normal = mesh->mNormals[x];
                    normals.push_back(normal.x);
                    normals.push_back(normal.y);
                    normals.push_back(normal.z);
                }

                if (mesh->HasTextureCoords(0))
                {
                    aiVector3D uv = mesh->mTextureCoords[0][x];
                    uvs.push_back(uv.x);
                    uvs.push_back(uv.y);
                }

                if (mesh->HasTangentsAndBitangents()) {
                    aiVector3D tangent = mesh->mTangents[x];
                    tangents.push_back(tangent.x);
                    tangents.push_back(tangent.y);
                    tangents.push_back(tangent.z);

                    aiVector3D bitangent = mesh->mBitangents[x];
                    biTangents.push_back(bitangent.x);
                    biTangents.push_back(bitangent.y);
                    biTangents.push_back(bitangent.z);
                }


            }


            printf("num vertices: %d\n", mesh->mNumVertices);
            printf("numFaces: %d\n", mesh->mNumFaces);
            for (int f = 0; f < mesh->mNumFaces; f++)
            {
                aiFace face = mesh->mFaces[f];
                indices.push_back(face.mIndices[0]);
                indices.push_back(face.mIndices[1]);
                indices.push_back(face.mIndices[2]);
            }


            // We want to associate the bone weights
            // with the current vertex.
            // Assimp presents us with a different representation
            // - all bones and for each bone a list of weight entries
            // with a vertexId (an index into the vertex list, our current x counter)
            // and the respective weight.
            if (mesh->HasBones()) {
                bool isSkeletalMesh = true;
                int weightsPerVertex = 0;
                for (int b = 0; b < mesh->mNumBones; b++)
                {
                    aiBone* bone = mesh->mBones[b];
                    aiVertexWeight* vertexWeights = bone->mWeights;
                    for (int w = 0; w < bone->mNumWeights; w++) {
                        /*if (vertexWeights[w].mVertexId == x && weightsPerVertex < 4) {
                            weights.push_back(vertexWeights[w].mWeight);
                            weightsPerVertex++;
                            boneIds.push_back(b);
                        }*/
                    }
                }
                if (weightsPerVertex < 4) {
                    for (int wd = 0; wd < 4 - weightsPerVertex; wd++) {
                        weights.push_back(0);
                        boneIds.push_back(-1);
                    }
                }
            }

            // Alternatively
            // we could just reserve space for 4 bones
            // and initialize with boneId = -1 and weight = 0;
            // If later not all slots are used, we are fine.
            // If we need more than 4, this is avoided as we
            // only loop up until index = 4.
            if (mesh->HasBones()) {
               /* for (int b = 0; b < min(mesh->mNumBones, 4); b++) {

                    aiBone* bone = mesh->mBones[b];
                    aiVertexWeight* vertexWeights = bone->mWeights;
                    int numWeights = bone->mNumWeights;

                    printf("bone: %s with # weights: %u\n", bone->mName.C_Str(), numWeights);
                    for (int w = 0; w < numWeights; w++) {
                        printf("\tweight: vId: %u : %f\n", vertexWeights[w].mVertexId, vertexWeights[w].mWeight);

                    }
                }*/


            }




            if (scene->HasAnimations()) {
                printf("Number of animations: %d\n", scene->mNumAnimations);
                for (int i = 0; i < scene->mNumAnimations; i++) {
                    aiAnimation* animation = scene->mAnimations[i];
                    printf("Animation #%d %s\n", i, animation->mName.C_Str());
                }
            }

            // Material and texture import
            if (scene->HasMaterials()) {

                aiMaterial* aimat = scene->mMaterials[mesh->mMaterialIndex];

                printf("Material: %s\n", aimat->GetName().C_Str());
                printf("\t|-- # properties: %d\n", aimat->mNumProperties);
                for (int p = 0; p < aimat->mNumProperties; p++) {
                    printf("\t\t|__ %s: %d\n", aimat->mProperties[p]->mKey.C_Str(), aimat->mProperties[p]->mDataLength);
                }
                printf("\t|-- # of diffuse textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_DIFFUSE));
                printf("\t|-- # of base color textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_BASE_COLOR));
                printf("\t|-- # of normal map textures: %d\n", aimat->GetTextureCount(aiTextureType::aiTextureType_NORMALS));
                aiString texturePath;
                aiTextureMapMode textureMapMode;
                aiTextureMapping textureMapping;
                aiReturn ret = aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &texturePath, &textureMapping, NULL, NULL, NULL, &textureMapMode);
                if (ret == aiReturn::aiReturn_SUCCESS)
                {
                    printf("Texture Path: %s\n", texturePath.C_Str());
                    kx::Image img(std::string(texturePath.C_Str()));
                    if (img.getPixels()) {
                        kx::Texture* texture = new kx::Texture(&img);       // TODO where to store this?
                    }
                }
            }

            // VertexArray creation
            {
                VertexArrayObject* vao = new VertexArrayObject();
                vao->bind();

                PositionBuffer vbPos(positions);
                vbPos.enableAttribute(0);

                if (!uvs.empty()) {
                    UVBuffer uvBuffer(uvs);
                    uvBuffer.enableAttribute(1);
                }

                if (!normals.empty()) {
                    NormalBuffer normalBuffer(normals);
                    normalBuffer.enableAttribute(2);
                }

                IndexBuffer indexBuffer(indices);
                vao->setNumberOfIndices(indices.size());
                vao->unbind();
                return vao;

            }



}