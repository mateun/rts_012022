//
// Created by mgrus on 22/01/2022.
//

#include "Camera.h"
#include "Window.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL_system.h>

kx::PerspectiveCamera::PerspectiveCamera(glm::vec3 position, kx::PerspectiveData data) : Camera(position) {
    proj = glm::perspective(data.fovyRad, data.ar, data.nearZ, data.farZ);

}


kx::OrthoCamera::OrthoCamera(glm::vec3 position, kx::OrthoData orthoData) : Camera(position) {
    proj = glm::ortho(orthoData.left,
                      orthoData.right,
                      orthoData.bottom,
                      orthoData.top,
                      orthoData.nearClip,
                      orthoData.farClip);


}

kx::Camera::Camera(glm::vec3 position) : pos(position) {
    lookAt = {0, 0, 0};
    updateView();
}

kx::Camera::Camera(glm::vec3 position, glm::vec3 at) :pos(position) {
    lookAt = at;
    updateView();


}

void kx::Camera::setPosition(glm::vec3 p) {
    pos = p;
    updateView();

}

void kx::Camera::setLookAt(glm::vec3 at) {
    lookAt = at;
    updateView();
}

void kx::Camera::updateView() {
    fwd = glm::normalize(lookAt - pos);
    view = glm::lookAt(pos, lookAt, {0, 1, 0});
}

glm::mat4 kx::Camera::getProj() {
    return proj;

}
glm::mat4 kx::Camera::getView() {
    return view;

}

glm::vec3 kx::Camera::getFwd() {
    return fwd;
}

glm::vec3 kx::Camera::getPos() {
    return pos;
}

void kx::Camera::updateMovement(std::vector<kx::WinEvent> winEvents) {

    const uint8_t* kb_state = SDL_GetKeyboardState(nullptr);

    glm::vec3 framePos = {0, 0, 0};

    if (kb_state[SDL_SCANCODE_LEFT]) {
        framePos.x -= .2;
    }
    if (kb_state[SDL_SCANCODE_RIGHT]) {
        framePos.x += .2;
    }
    if (kb_state[SDL_SCANCODE_UP] ) {
        framePos.z -= .2;
    }
    if (kb_state[SDL_SCANCODE_DOWN] ) {
        framePos.z += .2;
    }

    float pitch = 0;
    if (kb_state[SDL_SCANCODE_W]) {
        pitch += 0.2;
    }
    if (kb_state[SDL_SCANCODE_S]) {
        pitch -= 0.2;
    }

    float yaw = 0;
    if (kb_state[SDL_SCANCODE_A]) {
        yaw += 0.2;
    }
    if (kb_state[SDL_SCANCODE_D]) {
        yaw -= 0.2;
    }

    // First we rotate around the global y axis to
    // derive the new forward vector.
    glm::vec4 fwd = glm::vec4(getFwd(), 1.0);

    // We are rotating against the world up (0, 1, 0)
    glm::vec3 newFwd = glm::rotate(glm::mat4(1), glm::radians(yaw * 2), {0, 1, 0}) * fwd;
    glm::vec3 right = glm::normalize(glm::cross(newFwd, {0, 1, 0}));
    if (pitch) {
        newFwd = glm::rotate(glm::mat4(1), glm::radians(pitch), right) * glm::vec4{newFwd.x, newFwd.y, newFwd.z, 0};

    }
    glm::vec3 up = glm::cross(right, newFwd);


    glm::vec3 newFwdNoY = glm::normalize(glm::vec3{newFwd.x, .0f, newFwd.z});
    glm::vec3 newPos =  pos - (newFwdNoY * framePos.z * .6f);

    // Strafing
    newPos += right * framePos.x * .6f;

    setPosition(newPos);
    setLookAt(newPos + newFwd);

}

void kx::Camera::updatePosition(glm::vec3 p) {
    pos += p;
    updateView();
}

