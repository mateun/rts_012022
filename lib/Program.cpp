//
// Created by mgrus on 18/01/2022.
//

#include <vector>
#include <glm/gtc/type_ptr.hpp>
#include "Program.h"

kx::Program::Program(kx::Shader *vertexShader, kx::Shader *fragmentShader) {

    handle = glCreateProgram();

    glAttachShader(handle, vertexShader->getHandle());
    glAttachShader(handle, fragmentShader->getHandle());
    glLinkProgram(handle);

    GLint linkStatus;
    glGetProgramiv(handle, GL_LINK_STATUS, &linkStatus);
    if (GL_FALSE == linkStatus) {
        GLint logSize = 0;
        glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logSize);
        std::vector<GLchar> errorLog(logSize);
        glGetProgramInfoLog(handle, logSize, &logSize, &errorLog[0]);
        printf(&errorLog[0]);
        glDeleteShader(handle);
        exit(1);
    }

}

void kx::Program::use() {
    glUseProgram(handle);
}

void kx::Program::setBoolUniform(const std::string name, bool value) {
    use();
    int loc = getUniformLocation(name);
    glUniform1i(loc, value == true ? 1 : 0);
}

void kx::Program::setVec4Uniform(const std::string name, glm::vec4 value) {
    use();
    int loc = getUniformLocation(name);
    glUniform4fv(loc, 1, glm::value_ptr(value));
}

int kx::Program::getUniformLocation(const std::string name) {
    UniformLocation loc = locationCache[name];
    if (loc.value != -1) return loc.value;

    int val = glGetUniformLocation(handle, name.c_str());
    locationCache[name] = {val};
    return val;
}

void kx::Program::setVec3Uniform(const std::string& uniformName, glm::vec3 value) {
    use();
    int loc = getUniformLocation(uniformName);
    glUniform3fv(loc, 1, glm::value_ptr(value));
}

void kx::Program::setMat4Uniform(const char *uniformName, glm::mat4 matrix) {
    use();
    int loc = getUniformLocation(uniformName);
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(matrix));
}


