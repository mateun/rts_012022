//
// Created by mgrus on 22/01/2022.
//

#include "Scene.h"
#include "Program.h"
#include <glm/gtc/type_ptr.hpp>

void kx::Scene::setCamera(kx::Camera *camera) {
    cam = camera;

}

void kx::Scene::addRenderable(kx::Renderable2D *renderable) {
    renderables2D.push_back(renderable);
}

void kx::Scene::removeRenderable(kx::Renderable2D *renderable) {
    int idx = 0;
    for (auto r : renderables2D) {
        if (r == renderable) {
            break;
        }
        idx++;
    }
    renderables2D.erase(std::next(renderables2D.begin(), idx));
}

void kx::Scene::render(kx::Program* program) {
    // For now we just have a naive rendering function,
    // can be improved later.
    // e.g. sort by texture (tilemap)


    for (auto r : renderables2D) {
        program->use();
        r->getVertexArrayObject()->bind();
        r->getTexture()->bind();

        glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(r->getWorldTransform()));
        glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(cam->getView()));
        glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(cam->getProj()));
        if (r->isTile()) {
            glUniform1i(10, 1);
            glUniform1f(11, r->getTileOffsetLeft());
            glUniform1f(12, r->getTileOffsetBottom());
            glUniform1f(13, r->getTileWidth());
            glUniform1f(14, r->getTileHeight());

        } else {
            glUniform1i(10, 0);
        }

        glDrawElements(GL_TRIANGLES, r->getVertexArrayObject()->getNumberOfIndices(), GL_UNSIGNED_INT, 0);
    }

}

