//
// Created by mgrus on 19/02/2022.
//

#include <SDL.h>
#include "input.h"
#include <map>

static std::map<kx::KeyCode, SDL_KeyCode>* keymap = nullptr;

static void initKeyMap() {
    using namespace kx;
    keymap = new std::map<kx::KeyCode, SDL_KeyCode>();
    (*keymap)[a] = SDLK_a; (*keymap)[b] = SDLK_b; (*keymap)[c] = SDLK_c; (*keymap)[d] = SDLK_d; (*keymap)[e] = SDLK_e; (*keymap)[f] = SDLK_f; (*keymap)[g] = SDLK_g; (*keymap)[h] = SDLK_h; (*keymap)[i] = SDLK_i;
    (*keymap)[j] = SDLK_j; (*keymap)[k] = SDLK_k; (*keymap)[l] = SDLK_l; (*keymap)[m] = SDLK_m; (*keymap)[n] = SDLK_n; (*keymap)[o] = SDLK_o; (*keymap)[p] = SDLK_p; (*keymap)[q] = SDLK_q; (*keymap)[r] = SDLK_r;
    (*keymap)[s] = SDLK_s; (*keymap)[t] = SDLK_t; (*keymap)[u] = SDLK_u; (*keymap)[v] = SDLK_v; (*keymap)[w] = SDLK_w; (*keymap)[x] = SDLK_x; (*keymap)[y] = SDLK_y; (*keymap)[z] = SDLK_z;
}


static SDL_KeyCode translateKeyCodeToSDL(kx::KeyCode code) {
    if (!keymap) {
        initKeyMap();
    }
    return (*keymap)[code];
}


bool kx::KeyboardState::isDown(kx::KeyCode keyCode) {
    SDL_KeyCode sdlKeyCode = translateKeyCodeToSDL(keyCode);
    return state[sdlKeyCode];
}


