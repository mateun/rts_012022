//
// Created by mgrus on 15/02/2022.
//

#ifndef KOENIG2022_1_TEXT_H
#define KOENIG2022_1_TEXT_H

#include <string>
#include <glm/vec2.hpp>
#include "Renderer.h"
#include "Sprite.h"
#include "stb_truetype.h"


namespace kx {

    class Text {
    public:
        Text(const std::string& initialText, glm::vec2 position = {0, 0}, float pixelHeight = 14.0, const char* fontFileName="c:/windows/fonts/consola.ttf");
        void setText(const std::string& text);
        void setScreenPosition(int x, int y) { position.x = x; position.y = y;};
        RenderData getRenderData(Program *program, Camera *cam);
        RenderData getRenderDataDebug(Program *pProgram, Camera *pCamera);
        glm::vec2 getScreenDimensions();

    private:
        void renderText();

    private:
        Texture* texture = nullptr;
        Texture* fontTexture = nullptr;
        Sprite* sprite = nullptr;
        std::string text;
        glm::vec2 position;
        unsigned char ttf_buffer[1<<20];
        unsigned char temp_font_bitmap[512*512];
        stbtt_fontinfo fontinfo = {};
        uint8_t* pixels = nullptr;
        uint8_t* font_pixels = nullptr;
        int bakeResult  = 0;
        float pixelHeight = 25;
    };

}




#endif //KOENIG2022_1_TEXT_H
