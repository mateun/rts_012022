//
// Created by mgrus on 19/02/2022.
//

#ifndef KOENIG2022_1_RTSTEST_H
#define KOENIG2022_1_RTSTEST_H

#include <string>
#include "Window.h"
#include "input.h"
#include "Framebuffer.h"
#include "Renderer.h"
#include "TerrainDescription.h"
#include "PathNode.h"
#include <vector>
#include <map>

class game_state;
class PathNode;

namespace kx {
    class Program;
    class VertexArrayObject;
    class Camera;
    class Text;
    class Framebuffer;
    class RenderData;
    class MeshGrid;
    class DebugGrid;
    class PathCalculation;
}


enum ResourceType {
    Stone,
    Wood,
    Gold
};

enum UnitType {
    Worker,
    Soldier
};

enum BuildingType {
    House,
    Barrack, Castle, CoalMine, StoneMine, Farm, Field, Brunnen,
};

struct CreationCost {
    ResourceType resourceType;
    int amount;
};

struct ResourceCredit {
    ResourceType type;
    int amount;
};

// Something occupies place on the map.
struct Occupation {
    glm::vec2 tilePosition;
};

// Something that can move
// and is owned by a player.
// Normally spawned by buildings.
struct Unit {
    Unit(game_state* gs);
    UnitType type;
    glm::vec2 position;
    kx::VertexArrayObject* model = nullptr;
    int hp = 0;
    int mana = 0;
    int attackPower = 0;
    void* job= nullptr;
    float elapsedTime = 0;
    bool alreadyOnPath = false;
    // This is the time it takes for this unit
    // to re-consider any decisions, e.g.
    // which path to take, which job to do etc.
    float thinkTime = 10;

    void tick(float time, game_state* gs);

    kx::RenderData getRenderData(game_state* gameState);
    kx::PathCalculation* pathCalculation = nullptr;
    bool hasTarget = false;
    glm::vec2 targetTile;
    int pathWalkStep = -1;
};

struct Building {
    BuildingType buildingType;
    bool isSingleTile = true;
    glm::vec2 tilePosition;
    kx::VertexArrayObject* model= nullptr;
    long lastTick = 0;
    int tickIntervalInSeconds = 4;  // put this to more like 10 or 60 or whatever in production.
    float elapsedTime = 0;

    kx::RenderData getRenderData(game_state* gameState);
    void tick(float time, game_state* gameState);

    int unitsSpawned  = 0;
    int maxUnits = 10;
};

struct ResourceNode {
    int capacity = 0;
    int left = 0;
    glm::vec2 position = {0, 0};
};

struct Ray {
    glm::vec3 origin;
    glm::vec3 dir;
};

enum BuildState {
    None,
    Placement,
};

enum MenuState {
    Open,
    Closed
};

enum PopupInfoState {
    Show,
    Hide,
};

enum PlacementState {
    Allowed,
    Forbidden,
};

struct PopupInfo {
    std::string text;
    glm::vec2 position;
};


std::map<std::string, kx::Program*>* getShaderStore();


#define KB(value) value * 1204
#define MB(value) KB(value) *1024
#define GB(value) MB(value) * 1024

class game_state : public kx::RenderDataProvider {
public:
    kx::VertexArrayObject *quadVAO = nullptr;
    kx::VertexArrayObject* houseModel = nullptr;
    kx::VertexArrayObject *castleModel = nullptr;
    kx::VertexArrayObject *barrackModel = nullptr;
    kx::VertexArrayObject *coalResourceModel = nullptr;
    kx::VertexArrayObject *stoneResourceModel = nullptr;
    kx::VertexArrayObject *treeGroupModel = nullptr;
    kx::VertexArrayObject *singleTreeModel = nullptr;
    kx::VertexArrayObject *coalMineModel = nullptr;
    kx::VertexArrayObject *stoneMineModel = nullptr;
    kx::VertexArrayObject *citizenModel = nullptr;
    kx::VertexArrayObject *soldierModel = nullptr;
    kx::VertexArrayObject *horseModel = nullptr;
    kx::VertexArrayObject *warPigModel = nullptr;
    kx::VertexArrayObject *ballistaModel = nullptr;
    kx::VertexArrayObject *mountainModel = nullptr;
    kx::VertexArrayObject *farmModel = nullptr;
    kx::VertexArrayObject *cropModel = nullptr;
    kx::VertexArrayObject *cattleModel = nullptr;
    kx::VertexArrayObject *sheepModel = nullptr;
    kx::VertexArrayObject *pigModel = nullptr;
    kx::VertexArrayObject *bridgeModel = nullptr;
    kx::VertexArrayObject *workerModel = nullptr;
    kx::Texture* lowpolyAtlas = nullptr;
    kx::RenderData* fullScreenQuadRenderData = nullptr;
    kx::RenderData *houseRenderData = nullptr;
    kx::RenderData *cursorRenderData = nullptr;
    kx::Camera *uiCamera = nullptr;
    kx::Camera* defaultCamera = nullptr;
    kx::Camera* lightCam = nullptr;
    kx::Text* hpText = nullptr;
    kx::Text* resourcesText = nullptr;
    kx::Text* popupInfoText = nullptr;
    kx::Text *tileUnderMouseText = nullptr;
    kx::Text *unitCountText = nullptr;
    kx::Text *activePathCountText = nullptr;
    PopupInfo* currentPopupInfo = nullptr;
    kx::Program *defaultProgram = nullptr;
    kx::ColorBuffer *colorBuffer = nullptr;
    kx::DepthBuffer *shadowDepthBuffer = nullptr;
    kx::DebugGrid *debugGrid = nullptr;
    kx::MeshGrid *terrain = nullptr;
    BuildState buildState;
    glm::vec3 sunPos;
    glm::vec2 tileUnderMouse = {0, 0};
    MenuState buildMenuState = Closed;
    PlacementState placementState = Allowed;
    PopupInfoState popupInfoState = Hide;
    BuildingType currentBuildingToPlace = BuildingType::House;
    std::map<ResourceType, ResourceCredit> resources = { { ResourceType::Wood, {ResourceType::Wood, 100}}, { ResourceType::Stone, {ResourceType::Stone, 100}},{ ResourceType::Gold, {ResourceType::Gold, 50}}};
    std::vector<Building*> allBuildings;
    std::vector<Occupation*> undefinedOccupations;
    std::vector<Unit*> units;
    kx::TerrainDescription *terrainDescription = nullptr;
    kx::Image *terrainImage = nullptr;
    kx::Texture *terrainDebugTexture = nullptr;
    kx::RenderData *terrainDebugRenderData = nullptr;
    kx::Texture *miniMap = nullptr;
    glm::vec2 miniMapPos = { 0, 0};
    Building* buildingsArray = nullptr;
    float miniMapZoom;
    int camWidth = 20;
    bool isFullScreen = false;
    bool renderDebugGrid = false;
    int screenResX = 0;
    int screenResY = 0;
    int screenWidth = 0;
    int screenHeight = 0;
    int unitsCount = 0;
    int numberOfBuildings = 0;
    int maxBuildingsAllowed = 100;
    int activePathCount = 0;
    int maxUnitsAllowed = 1000;
    glm::mat4* buildingWorldMatrices = nullptr;
    glm::mat4* unitWorldMatrices = nullptr;
    kx::TerrainDescription *terrainDescriptions = nullptr;
    int terrainChunkDim = 5;
    int terrainCells = 100;

    kx::MeshGrid *meshGrids = nullptr;
    ResourceNode* resourceNodeArray = nullptr;
    int numberOfResourceNodes;


    glm::mat4* resourceNodeWorldMatrices = nullptr;
    Unit *unitsArray;

    kx::Camera * getLightCamera() override {
        return lightCam;
    }

    kx::Camera * getCamera() override {
        return defaultCamera;
    }

    glm::vec3 getLightPosition() override {
        return sunPos;
    }
    kx::Program * getProgram() override {
        return defaultProgram;
    }

    kx::RenderData* getCursorRenderData();

    std::vector<CreationCost> costForBuilding(BuildingType t) {
        switch (t){
            case BuildingType::House : return {{ResourceType::Wood, 10}, {ResourceType::Stone, 15}, {ResourceType::Gold, 5}};
            case BuildingType::Barrack : return {{ResourceType::Wood, 15}, {ResourceType::Stone, 35}, {ResourceType::Gold, 15}};
        }
        return {{}};
    }




    // O(n) currently... optimize later...
    bool isOverBuilding() {
        for (auto b : allBuildings) {
            if (b->tilePosition == tileUnderMouse) {
                return true;
            }
        }

        for (auto uo : undefinedOccupations) {
            if (uo->tilePosition == tileUnderMouse) {
                return true;
            }
        }

        return false;
    }


    void calculatePlacementState(BuildingType type) {
        if (!isOverBuilding() && hasEnoughResources(costForBuilding(type))) {
            placementState = PlacementState::Allowed;
        } else {
            placementState = PlacementState::Forbidden;
        }
    }

    bool hasEnoughResources(std::vector<CreationCost> costs) {
        for (auto cc : costs) {
            if (resources[cc.resourceType].amount < cc.amount) {
                return false;
            }
        }
        return true;
    }

    void createBuilding(BuildingType type) {
        // First reduce our resources by the cost
        for (auto c : costForBuilding(type)) {
            resources[c.resourceType].amount -= c.amount;
        }

        // Instantiate the building
        //auto b = new Building{ type, true, tileUnderMouse};
        //b->model = modelForBuildingType(type);
        //allBuildings.push_back(b);

        buildingsArray[numberOfBuildings].elapsedTime = 0;
        buildingsArray[numberOfBuildings].unitsSpawned = 0;
        buildingsArray[numberOfBuildings].lastTick = 0;
        buildingsArray[numberOfBuildings].maxUnits = 10;
        buildingsArray[numberOfBuildings].tickIntervalInSeconds = 15;
        buildingsArray[numberOfBuildings].tilePosition = tileUnderMouse;
        numberOfBuildings++;
    }

    // Might only want to tick once per second?!
    void tickBuildings(float time) {
        for (auto b : allBuildings) {
            b->tick(time, this);
        }
    }

    kx::VertexArrayObject* modelForBuildingType(BuildingType bt) {
        switch (bt) {
            case House:
                return houseModel;
            case Barrack:
                return barrackModel;
            case Castle:
                return castleModel;
            case Farm:
                return farmModel;
            case CoalMine:
                return coalMineModel;
            default:
                return houseModel;
        }
    }

    kx::PathCalculation *navigationGrid;

    void tickUnits(float d);
};

struct game_memory {
    bool isInitialized = false;
    uint64_t permanent_storage_size;
    uint8_t* permanent_storage;
    uint64_t transient_storage_size;
    uint8_t* transient_storage;

};

void endGame();
void gameUpdateAndRender(float time, kx::Window* window, kx::KeyboardState* keyboardState, kx::MouseState* mouseState, std::vector<kx::WinEvent> winEvents, game_memory* gameMemory);

std::string readFile(const std::string& fileName);

#endif //KOENIG2022_1_RTSTEST_H
