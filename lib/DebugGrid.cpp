//
// Created by mgrus on 22/02/2022.
//

#include "DebugGrid.h"
#include "VertexBuffer.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

kx::DebugGrid::DebugGrid(int cols, int cellSize) {

    std::vector<float> pos = {};
    std::vector<uint32_t> indices = {};

    float height = 0.01;
    int indexCounter = 0;
    for (int l = 0; l < cols; l++) {
        pos.push_back(l);
        pos.push_back(height);
        pos.push_back(0);
        pos.push_back(l);
        pos.push_back(height);
        pos.push_back(cols);

        indices.push_back(indexCounter++);
        indices.push_back(indexCounter++);

    }

    for (int l = 0; l < cols; l++) {
        pos.push_back(0);
        pos.push_back(height);
        pos.push_back(l);
        pos.push_back(cols);
        pos.push_back(height);
        pos.push_back(l);

        indices.push_back(indexCounter++);
        indices.push_back(indexCounter++);

    }

    vao = new VertexArrayObject();
    vao->bind();
    kx::PositionBuffer posBuffer(pos);
    posBuffer.enableAttribute(0);
    kx::IndexBuffer indexBuffer(indices);
    vao->setNumberOfIndices(indices.size());

    vao->unbind();

}

kx::RenderData kx::DebugGrid::getRenderData(kx::Program* program, kx::Camera* camera, glm::vec3 position, glm::vec4  color) {
    RenderData rd = {};
    if (!program)
    {
        //rd.prog = (*getShaderStore())["default2D"];
    }
    else
    {
        rd.prog = program;
    }
    rd.cam = camera;
    rd.vao =vao;
    rd.world = glm::translate(glm::mat4(1), {position.x, position.y, position.z});
    rd.diffuseTexture = nullptr;
    rd.lit = false;
    rd.transparent = false;
    rd.color = color;
    rd.primitiveType = GL_LINES;

    return rd;

}
