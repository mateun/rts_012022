//
// Created by mgrus on 30/01/2022.
//

#include "Window.h"
#include "GLContext.h"
#include <gl/glew.h>
#include <SDL.h>

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")


kx::WindowGL::WindowGL(int w, int h, bool fullscreen) : Window(w, h, fullscreen) {

}

void kx::WindowGL::clearBackbuffer(float r, float g, float b, float a) {
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

}

uint32_t kx::WindowGL::provideWindowFlags() {
    uint32_t windowFlags = SDL_WINDOW_OPENGL;;
    if (fullScreen) {
        windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    return windowFlags;
}

void kx::WindowGL::init() {
    Window::init();
    new kx::GLContext(this);
}

void kx::WindowGL::present() {
    SDL_GL_SwapWindow(win);
}
