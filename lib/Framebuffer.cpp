//
// Created by mgrus on 07/02/2022.
//

#include "Framebuffer.h"
#include "Texture.h"

kx::Framebuffer::Framebuffer(int width, int height) : width(width), height(height) {
    glGenFramebuffers(1, &handle);
    bind();
}

void kx::Framebuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, handle);
}

void kx::Framebuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

kx::Texture *kx::ColorBuffer::getColorTexture() {
    return colorTexture;
}

void kx::Framebuffer::clear() {
    GLfloat color[4] = { 0.0f, 0.0f, 0.0f, 1 };

    glClearBufferfv(GL_COLOR, 0, color);
    GLfloat d = 1;
    glClearBufferfv(GL_DEPTH, 0, &d);

}

kx::ColorBuffer::ColorBuffer(int w, int h) : Framebuffer(w, h) {

    colorTexture = new Texture(width, height);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture->getHandle(), 0);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        printf("fb error: %d", status);
        exit(1);
    }

    DepthTexture* depthTexture = new DepthTexture(width, height);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getHandle(), 0);

    const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, draw_buffers);

    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        printf("fb error: %d", status);
        exit(1);
    }

    unbind();
}

kx::DepthBuffer::DepthBuffer(int w, int h) : Framebuffer(w, h){
    depthTexture = new DepthTexture(width, height);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getHandle(), 0);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        printf("fb error: %d", status);
        exit(1);
    }

}

kx::DepthTexture *kx::DepthBuffer::getDepthTexture() {
    return depthTexture;
}

void kx::DepthBuffer::clear() {
    Framebuffer::clear();
}
