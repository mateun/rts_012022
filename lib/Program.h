//
// Created by mgrus on 18/01/2022.
//

#ifndef KOENIG2022_1_PROGRAM_H
#define KOENIG2022_1_PROGRAM_H

#include "Shader.h"
#include <glm/glm.hpp>
#include <map>

namespace  kx {

    struct UniformLocation {
        int value = -1;
    };

    class Program {
    public:
        Program(Shader* vertexShader, Shader* fragmentShader);

        void use();
        int getPositionIndex() { return glGetAttribLocation(handle, "pos");};
        int getNormalIndex() { return glGetAttribLocation(handle, "normal");};
        int getUVIndex() {return glGetAttribLocation(handle, "uv");};
        void setBoolUniform(const std::string name, bool value);
        void setVec4Uniform(const std::string name, glm::vec4 value);

        void setVec3Uniform(const std::string& uniformName, glm::vec3 value);

        void setMat4Uniform(const char *uniformName, glm::mat4 matrix);

    private:
        int getUniformLocation(const std::string name);

    private:
        GLuint handle;
        std::map<std::string, UniformLocation> locationCache;

    };

}



#endif //KOENIG2022_1_PROGRAM_H
