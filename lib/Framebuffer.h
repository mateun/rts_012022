//
// Created by mgrus on 07/02/2022.
//

#ifndef KOENIG2022_1_FRAMEBUFFER_H
#define KOENIG2022_1_FRAMEBUFFER_H

#include <gl/glew.h>
#include "Texture.h"

namespace kx {

    class Framebuffer {
    public:
        Framebuffer(int width, int height);
        void bind();
        void unbind();
        virtual void clear();

    protected:
        int width;
        int height;

    private:
        GLuint handle;
    };

    class ColorBuffer : public Framebuffer{
    public:
        ColorBuffer(int w, int h);
        Texture *getColorTexture();


    private:
        Texture *colorTexture;
    };

    class DepthBuffer : public Framebuffer {
    public:
        DepthBuffer(int w, int h);
        DepthTexture* getDepthTexture();
        void clear() override;

    private:
        DepthTexture *depthTexture;
    };

}



#endif //KOENIG2022_1_FRAMEBUFFER_H
