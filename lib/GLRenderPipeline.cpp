//
// Created by mgrus on 05/02/2022.
//

#include "RenderPipeline.h"
#include "VertexBuffer.h"
#include <glm/gtc/type_ptr.hpp>

void kx::GLPipeline::setVertexPositions(std::vector<float> positions) {
    initVAO();

    kx::PositionBuffer positionBuffer(positions);
    positionBuffer.enableAttribute(0);
    //positionBuffer.enableAttribute(shaderProgram->getPositionIndex());



    vao->unbind();
}

void kx::GLPipeline::setVertexUVs(std::vector<float> uvs) {
    initVAO();
    kx::UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    vao->unbind();
}

void kx::GLPipeline::setShaderProgram(kx::Program *program) {
    shaderProgram = program;
}

void kx::GLPipeline::initVAO() {
    if (!vao) {
        vao = new VertexArrayObject();

    }
    vao->bind();
}

void kx::GLPipeline::setIndicies(std::vector<uint32_t> indices) {
    initVAO();
    new kx::IndexBuffer(indices);
    vao->setNumberOfIndices(indices.size());
    vao->unbind();

}

bool kx::GLPipeline::checkRenderPrerequisites() {
    return vao && camera && shaderProgram;
}

void kx::GLPipeline::renderTriangles(glm::mat4 proj, glm::mat4 view, glm::mat4 world) {
    if (!checkRenderPrerequisites()) {
        printf("render prerequisites not fulfilled!\n");
        exit(1);
    }

    shaderProgram->use();

    vao->bind();
    if (pipelineData->pipelineState->isUseTexture()) {
        glUniform1i(40, 1);
        diffuseTexture->bind();
    } else {
        glUniform1i(40, 0);
        glUniform4fv(41, 1, pipelineData->pipelineState->getColor().data());
    }

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(proj));

    // Turn tile rendering off
    // TODO do not rely on the knowledge of this uniform index
    glUniform1i(10, 0);

    // TODO handle tile rendering

//    if (r->isTile()) {
//        glUniform1i(10, 1);
//        glUniform1f(11, r->getTileOffsetLeft());
//        glUniform1f(12, r->getTileOffsetBottom());
//        glUniform1f(13, r->getTileWidth());
//        glUniform1f(14, r->getTileHeight());
//
//    } else {
//        glUniform1i(10, 0);
//    }

    glDrawElements(GL_TRIANGLES, vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0);
}

void kx::GLPipeline::renderTriangles(glm::mat4 world) {
    renderTriangles(camera->getProj(), camera->getView(), world);
}

void kx::GLPipeline::setTexture(kx::Texture *t) {
    diffuseTexture = t;
}

void kx::GLPipeline::setNormalMap(kx::Texture *t) {
    normalMap = t;
}

kx::GLPipeline::GLPipeline(kx::GLPipelineData *data) {
    if (!data->isValid()) {
        exit(1);
    }

    pipelineData = data;
    shaderProgram = data->shaderProgram;
    diffuseTexture = data->initialDiffuseTexture;
    if (data->initialIndices.size() > 0)
        setIndicies(data->initialIndices);

    if (data->initialUVs.size() > 0)
        setVertexUVs(data->initialUVs);


    setVertexPositions(data->initialPositions);
    camera = data->camera;
}

void kx::GLPipeline::renderLines(glm::mat4 world) {
    renderLines(camera->getProj(), camera->getView(), world);
}

void kx::GLPipeline::renderLines(glm::mat4 proj, glm::mat4 view, glm::mat4 world) {
    if (!checkRenderPrerequisites()) {
        printf("render prerequisites not fulfilled!\n");
        exit(1);
    }
    shaderProgram->use();
    vao->bind();

    if (pipelineData->pipelineState->isUseTexture()) {
        shaderProgram->setBoolUniform("useTexture", true);
        diffuseTexture->bind();
    } else {
        shaderProgram->setBoolUniform("isLit", false);
        shaderProgram->setBoolUniform("useTexture", false);
        shaderProgram->setVec4Uniform("staticColor", {.6, .6,.6,1});
    }



    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(world));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(proj));

    // Tiles off
    glUniform1i(10, 0);



    glDrawElements(GL_LINES, vao->getNumberOfIndices(), GL_UNSIGNED_INT, 0);


}

void kx::PipelineState::setViewport(kx::Viewport vp) {
    viewport = vp;
    glViewport(0, 0, vp.w, vp. h);
}

void kx::PipelineState::enableWireFrame() {
    wireFrame = true;
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
}

void kx::PipelineState::disableWireFrame() {
    wireFrame = false;
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void kx::PipelineState::enableTransparancy() {
    transparency = true;
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
}

void kx::PipelineState::disableTransparancy() {
    transparency = false;
    glDisable(GL_BLEND);
}

void kx::PipelineState::enableColor(kx::Color col) {
    useTexture = false;
    color = col;
}

void kx::PipelineState::disableColor() {
    useTexture = true;
    color = {};
}




