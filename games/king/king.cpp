//
// Created by mgrus on 10.04.2022.
//

#include "king.h"
#include "../../lib/core/FileUtils.h"
#include "../../lib/VertexArrayObject.h"
#include "../../lib/VertexBuffer.h"
#include "../../lib/Camera.h"
#include "../../lib/Program.h"
#include "../../lib/Shader.h"
#include "../../lib/Renderer.h"
#include "../../lib/MeshGrid.h"
#include "../../lib/Text.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL.h>

static bool checkExit(const std::vector<kx::WinEvent>& events) {
    auto result = false;
    for (auto e : events) {
        if (e.type == kx::WinEventType::EXIT) {
            result = true;
        } else if(e.type == kx::WinEventType::KEYDOWN && e.key == "ESCAPE") {
            result = true;
        }
    }
    return result;
}

static void render(float time, KingGameState& gameState) {
    auto renderer = kx::Renderer();
    renderer.renderSingleColor(gameState.meshGrid->getRenderDataSingleColor(&gameState), nullptr);

    renderer.renderTextured(gameState.cameraPositionText->getRenderData(gameState.prog, gameState.uiCamera));

}

void testCrossProduct() {
    // Right hand rule:
    // Index finger  = v1
    // Middle finger = v2
    // Thumb = v3 (result of the cross product)
    auto v1 = glm::vec3(0, 0, -1);
    auto v2 = glm::vec3(-1, 0, 0);
    auto v3 = glm::cross(v1, v2);
    assert(v3.y == 1);
}

static void doInit(KingGameState& kingGameState, kx::Window& window) {

    testCrossProduct();

    auto vsSource = kx::FileUtils::readFile("../assets/shaders/universal_vs.glsl");
    auto fsSource = kx::FileUtils::readFile("../assets/shaders/universal_fs.glsl");
    kx::Shader vshader(vsSource, kx::ShaderType::VERTEX);
    kx::Shader fshader(fsSource, kx::ShaderType::FRAGMENT);
    kingGameState.prog = new kx::Program(&vshader, &fshader);

    float camWidth = 220;
    float cw = camWidth/2;
    float ar = (float) window.getDimension().w/ (float) window.getDimension().h;
    kx::Camera* orthoCamGameplay = new kx::OrthoCamera({0, 10, 10}, {-cw , cw, -cw /ar, cw / ar, .1, 5000});
    orthoCamGameplay->setPosition({30, 20, 180});
    orthoCamGameplay->setLookAt({0, 0, 0});
    kingGameState.camera = orthoCamGameplay;

    auto pd = kx::PerspectiveData{};
    pd.ar = ar;
    pd.farZ = 500;
    pd.nearZ = 1;
    pd.fovyRad = glm::radians(45.0);
    auto perspectiveCam = new kx::PerspectiveCamera({0, 50, 50}, pd);
    kingGameState.camera = perspectiveCam;

    kingGameState.screenWidth = window.getDimension().w;
    kingGameState.screenHeight = window.getDimension().h;

    kx::Image heightMap (std::string(SDL_GetBasePath()) + "../assets/textures/heightMap.png");
    kingGameState.meshGrid = new kx::MeshGrid({0, 0, 0}, 1, 100, (uint8_t*)heightMap.getPixels(), true);

    kx::Camera* lightCam = new kx::OrthoCamera({0, 10, 10}, {-10, 10, -10, 10, 1, 300});
    lightCam->setPosition({100, 100, -30});
    lightCam->setLookAt({0, 0, 0});
    kingGameState.lightCam = lightCam;

    kingGameState.cameraPositionText = new kx::Text("Unit#:", {5, kingGameState.screenHeight-100}, 14);

    kx::Camera* uiCam = new kx::OrthoCamera({0, 0, 5}, {0, (float)window.getDimension().w, 0, (float)window.getDimension().h, 0.1, 100});
    uiCam->setPosition({0, 0, 5});
    uiCam->setLookAt({0, 0, 0});
    kingGameState.uiCamera = uiCam;

}

static bool keyDown(const std::string& key, std::vector<kx::WinEvent>& events) {
    for (auto e : events) {
        if (e.type == kx::WinEventType::KEYDOWN && e.key == key) {
            return true;
        }
    }
    return false;
}



void update(float time, KingGameState &gameState, std::vector<kx::WinEvent> winEvents, kx::MouseState* mouseState) {

    gameState.mouseCoords = mouseState->mouseCoords;

    gameState.camera->updateMovement(winEvents);

    // Render camera position text
    {
        char buf[200];
        sprintf_s(buf, 200, "cam pos: %.2f/%.2f/%.2f", gameState.camera->getPos().x, gameState.camera->getPos().y, gameState.camera->getPos().z);
        gameState.cameraPositionText->setText(buf);
    }
}

void kingGameLoop(float time, kx::Window *window, kx::KeyboardState *keyboardState, kx::MouseState *mouseState,
                std::vector<kx::WinEvent> winEvents, bool& shallStop) {

    shallStop = checkExit(winEvents);
    if (shallStop) return;

    static bool inited = false;
    static auto gameState = KingGameState {};
    if (!inited) {
        doInit(gameState, *window);
        inited= true;
    }

    // Update
    update(time, gameState, winEvents, mouseState);




    window->clearBackbuffer(0.0, 0, 0.1, 1);
    render(time, gameState);
    window->present();

}

kx::Camera *KingGameState::getCamera() {
    return this->camera;
}

kx::Camera *KingGameState::getLightCamera() {
    return this->lightCam;
}

glm::vec3 KingGameState::getLightPosition() {
    return this->lightCam->getPos();
}

kx::Program *KingGameState::getProgram() {
    return this->prog;
}
