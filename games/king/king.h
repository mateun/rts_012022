//
// Created by mgrus on 10.04.2022.
//

#ifndef KOENIG2022_1_KING_H
#define KOENIG2022_1_KING_H

#include "../../lib/Window.h"
#include "../../lib/Renderer.h"
#include <vector>

namespace  kx {
    class Camera;
    class Program;
    class VertexArrayObject;
    class PositionBuffer;
    class MeshGrid;
    class Text;
}

struct KingGameState : public kx::RenderDataProvider {
    int screenWidth;
    int screenHeight;
    glm::vec2 mouseCoords;

    kx::Program* prog = nullptr;
    kx::Camera* camera = nullptr;
    kx::MeshGrid *meshGrid = nullptr;

    kx::Camera* getCamera() override;
    kx::Camera* getLightCamera() override;
    glm::vec3 getLightPosition() override;
    kx::Program* getProgram() override;

    kx::Camera *lightCam = nullptr;
    kx::Text *cameraPositionText = nullptr;
    kx::Camera *uiCamera = nullptr;
};

void kingGameLoop(float time, kx::Window* window, kx::KeyboardState* keyboardState,
                kx::MouseState* mouseState, std::vector<kx::WinEvent> winEvents, bool& shallStop);

#endif //KOENIG2022_1_KING_H
