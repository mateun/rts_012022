#include <SDL.h>
#include "lib/Window.h"
#include "lib/Program.h"
#include "lib/VertexArrayObject.h"
#include "lib/Texture.h"
#include "lib/stb_image.h"
#include "lib/Image.h"
#include "lib/Camera.h"
#include "lib/Sprite.h"
#include "lib/Terrain.h"
#include "lib/Console.h"
#include "lib/RenderPipeline.h"
#include "lib/Renderer.h"
#include "lib/core/FileUtils.h"
#include "lib/ModelImporter.h"
#include "lib/Framebuffer.h"
#include "lib/MeshGrid.h"
#include "lib/input.h"
#include "games/king/king.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <map>


int screenWidth = 1920;
int screenHeight = 1024;

extern std::map<std::string, kx::Program*>* getShaderStore();

kx::WinEvent handleKeyEvents(kx::Terrain &terrain, glm::vec3& camPos, float& camYaw, bool &running, const kx::Window& win, bool& showConsole);
void renderTerrain(const kx::MeshGrid& meshGrid, kx::Camera* cam, kx::Texture* diffuseTexture);

void renderFullScreenQuad(kx::VertexArrayObject *quadVAO, kx::Framebuffer framebuffer);

void beginDraw(kx::Framebuffer &primaryRenderTarget);

void endDraw(kx::Framebuffer &primaryRenderTarget);




void bindTransforms(kx::Camera* camera)  {
    glm::mat4 mv = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    glm::mat4 mp = glm::ortho<float>(-2.5, 2.5, -2.5, 2.5, 0.1, 50);
    glm::mat4 ms = glm::scale<float>(glm::mat4(1), glm::vec3(64, 64, 1));
    glm::mat4 mt = glm::translate(glm::mat4(1), {350, 220, 0});

    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(mt * ms));
    glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(camera->getView()));
    glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(camera->getProj()));
}

kx::GLPipeline* createGridPL(kx::Camera* cam, kx::Program* program) {


    kx::PipelineState* plState = new kx::PipelineState();
    plState->setViewport({screenWidth, screenHeight});
    plState->enableTransparancy();
    plState->disableWireFrame();
    plState->enableColor({ 1, 1,1, 1});

    kx::GLPipelineData* plData = new kx::GLPipelineData();
    plData->pipelineState = plState;
    plData->shaderProgram = program;
    plData->camera = cam;

    std::vector<float> pos = {};
    std::vector<uint32_t> indices = {};

    int indexCounter = 0;
    for (int l = -100; l < 100; l++) {
        pos.push_back(l);
        pos.push_back(0);
        pos.push_back(100);
        pos.push_back(l);
        pos.push_back(0);
        pos.push_back(-100);

        indices.push_back(indexCounter++);
        indices.push_back(indexCounter++);

    }

    for (int l = -100; l < 100; l++) {
        pos.push_back(-100);
        pos.push_back(0);
        pos.push_back(l);
        pos.push_back(100);
        pos.push_back(0);
        pos.push_back(l);

        indices.push_back(indexCounter++);
        indices.push_back(indexCounter++);

    }

    plData->initialPositions = pos;
    plData->initialIndices = indices;

    kx::GLPipeline* gridPL = new kx::GLPipeline(plData);
    return gridPL;

}

void testAllRenderMethods(kx::VertexArrayObject *vao, kx::Camera *cam, kx::Texture *diffuseTexture,
                          kx::VertexArrayObject *model) {
    kx::RenderData rd;
    rd.vao = vao;
    rd.cam = cam;
    rd.diffuseTexture = diffuseTexture;
    rd.prog =  (*getShaderStore())["default2D"];
    rd.world = glm::mat4(1);
    rd.color = {1, 1, 0, 1};
    rd.transparent = true;

    kx::Renderer renderer;
    //renderer.renderTextured(rd);

    rd.transparent = false;
    rd.world = glm::translate(glm::mat4(1), {-2, 0, 0});
    renderer.renderTextured(rd);

    rd.world = glm::translate(glm::mat4(1), {3, 1, -2});
    rd.vao = model;
    rd.lit = true;
    renderer.renderTextured(rd);
}

// Small utility function to update the camera position and rotation
// based on the latest inputs
void updateCamera(kx::Camera* cam, float camYaw, glm::vec3 pos) {
    // First we rotate around the global y axis to
    // derive the new forward vector.
    glm::vec4 fwd = glm::vec4(cam->getFwd(), 1.0);

    // We are rotating against the world up (0, 1, 0)
    glm::vec3 newFwd = glm::rotate(glm::mat4(1), glm::radians(camYaw * 2), {0, 1, 0}) * fwd;
    glm::vec3 right = glm::cross(newFwd, {0, 1, 0});
    glm::vec3 up = glm::cross(right, newFwd);


    glm::vec3 newFwdNoY = {newFwd.x, 0, newFwd.z};
    glm::vec3 newPos = cam->getPos() - (newFwdNoY * pos.z * 0.2f);

    // Strafing
    newPos += right * pos.x * 2.0f;

    cam->setPosition(newPos);
    cam->setLookAt(newPos + newFwd);
}

std::string readFile(const std::string& fileName) {
    return kx::FileUtils::readFile(fileName);
}

std::vector<kx::WinEvent> collectWindowEvents(kx::Window& window) {
    std::vector<kx::WinEvent> events;
    kx::WinEvent event;
    while ((event = window.pollWindow()).type != kx::WinEventType::NONE) {
        events.push_back(event);
    }
    return events;

    // todo(martin): handle keyboard state
    // todo(martin): handle mouse state
    //const uint8_t* kb_state = SDL_GetKeyboardState(nullptr);
    int mouseX, mouseY;
    SDL_GetMouseState(&mouseX, &mouseY);
}

kx::KeyboardState getKeyBoardState() {
    return {SDL_GetKeyboardState(nullptr)};
}

kx::MouseState getMouseState() {
    int mouseX, mouseY;
    SDL_GetMouseState(&mouseX, &mouseY);
    return {{mouseX, mouseY}};
}

bool running = true;
void endGame() {
    running = false;
}

bool isFullScreen(int argc, char** args) {
    for (int i = 0; i < argc; i++) {
        std::string arg = args[i];
        if (arg == "fullscreen") {
            return true;
        }
    }

    return false;
}

void legacyInitThings() {
    kx::Image imgGrass(std::string(SDL_GetBasePath()) + "../assets/textures/grass64.png");
    kx::Image imgProps(std::string(SDL_GetBasePath()) + "../assets/textures/TX Props.png");
    kx::Image imgWood (std::string(SDL_GetBasePath()) + "../assets/textures/wood64.png");
    kx::Image imgGrassRealistic (std::string(SDL_GetBasePath()) + "../assets/textures/forrest_ground_01_diff_1k.png");
    kx::Texture propsTex(&imgProps);
    kx::Texture grassTex(&imgGrass);
    kx::Texture woodTex(&imgWood);
    kx::Texture grassTexHD(&imgGrassRealistic);

    kx::Sprite grassSprite(400, 300, &grassTex);
    kx::Sprite vase1Sprite(600, 230, &propsTex, {160, 192}, {192, 192 + 32});
    kx::Sprite vase2Sprite(50, 130, &propsTex, {160, 128}, {192, 192});

    kx::Scene mainScene2D;
    mainScene2D.addRenderable(&grassSprite);
    mainScene2D.addRenderable(&vase1Sprite);
    mainScene2D.addRenderable(&vase2Sprite);

    kx::Terrain terrain(32);

    kx::MeshGrid meshGrid({0, 0, 0}, 1, 40);

    kx::Console console;

    float camYaw = 0;
    glm::vec3 camPos = {0, 10, 10};
    kx::Camera* perspectiveCam = new kx::PerspectiveCamera(camPos, {glm::radians(45.0), (float)screenWidth/(float)screenHeight, 0.1, 100});
    perspectiveCam->setLookAt({0, 0, -2});

    kx::Camera* orthoCamGameplay = new kx::OrthoCamera({0, 10, 10}, {-10, 10, -10, 10, 1, 100});
    orthoCamGameplay->setPosition({-6, 12, 6});
    orthoCamGameplay->setLookAt({0, 0, 0});

    kx::PipelineState plState = {};
    plState.setViewport({screenWidth, screenHeight});
    plState.enableTransparancy();
    plState.disableWireFrame();

    /*kx::GLPipelineData plData = {};
    plData.pipelineState = &plState;
    plData.shaderProgram = &program;
    plData.camera = perspectiveCam;
    plData.initialPositions = { -0.5, -0.5, 0, 0.5, -0.5, 0, 0.5, 0.5, 0, -0.5, 0.5, 0 };
    plData.initialIndices = { 0,1,2, 0,2, 3 };
    plData.initialUVs = { 0, 0, 1, 0,1, 1, 0, 1 };
    plData.initialDiffuseTexture = &grassTex;
    kx::GLPipeline* quadRP = new kx::GLPipeline(&plData);
    kx::RenderPipeline* gridPL = createGridPL(orthoCamGameplay, &program);*/

    kx::VertexArrayObject* modelVAO =

            kx::ModelImporter::importFromFile(std::string(SDL_GetBasePath()) + "../assets/models/uvsphere1.dae");

    kx::Renderer renderer;
    bool showConsole = false;

}

int main(int argc, char** args) {

    kx::WindowGL win(screenWidth, screenHeight, isFullScreen(argc, args) );
    win.init();


#ifdef legacy_init_things
    legacyInitThings
#endif


    uint64_t freq = SDL_GetPerformanceFrequency();
    uint64_t start = 0;
    uint64_t end = 0;
    float diff = 0;

//    game_memory gameMemory;
//    gameMemory.permanent_storage_size = MB(64);
//    gameMemory.permanent_storage = (uint8_t*) malloc(gameMemory.permanent_storage_size);
//    gameMemory.transient_storage_size = GB(2);
//    gameMemory.transient_storage = (uint8_t*) malloc(gameMemory.transient_storage_size);

    SDL_StartTextInput();
    while (running) {
        start = SDL_GetPerformanceCounter();

        std::vector<kx::WinEvent> winEvents = collectWindowEvents(win);
        kx::KeyboardState keyboardState = getKeyBoardState();
        kx::MouseState mouseState = getMouseState();

        //gameUpdateAndRender(diff, &win, &keyboardState, &mouseState, winEvents, &gameMemory);
        bool shallStop = false;
        kingGameLoop(diff, &win, &keyboardState, &mouseState, winEvents, shallStop);
        running = !shallStop;

        end = SDL_GetPerformanceCounter();
        diff = (float)(end - start) / (float)freq;
    }
    return 0;
}

void endDraw(kx::Framebuffer &primaryRenderTarget) { primaryRenderTarget.unbind(); }

void beginDraw(kx::Framebuffer &primaryRenderTarget) {
    primaryRenderTarget.bind();
    primaryRenderTarget.clear();
}


void renderFullScreenQuad(kx::VertexArrayObject *quadVAO, kx::ColorBuffer* frameBuffer) {
    kx::RenderData rd;
    rd.vao = quadVAO;
    kx::Camera* orthoCam = new kx::OrthoCamera({0, 0, 5}, {0, (float)screenWidth, 0, (float)screenHeight, 0.1, 100});
    orthoCam->setPosition({0, 0, 5});
    orthoCam->setLookAt({0, 0, 0});
    rd.cam = orthoCam;
    rd.diffuseTexture = frameBuffer->getColorTexture();
    rd.prog =  (*getShaderStore())["default2D"];
    rd.world = glm::translate(glm::mat4(1), {400, 300, -1}) *
            glm::scale(glm::mat4(1), {800, 600, 1});
    rd.transparent = false;
    rd.lit = false;

    kx::Renderer renderer;

    renderer.renderTextured(rd);
    delete(orthoCam);

}

kx::WinEvent handleKeyEvents(kx::Terrain &terrain, glm::vec3& camPos, float& camYaw, bool &running, const kx::Window& win, bool& console ) {
    kx::WinEvent event = win.pollWindow();

    const uint8_t* kb_state = SDL_GetKeyboardState(nullptr);

    if (kb_state[SDL_SCANCODE_LEFT]) {
        camPos.x -= .02;
    }
    if (kb_state[SDL_SCANCODE_RIGHT]) {
        camPos.x += .02;
    }
    if (kb_state[SDL_SCANCODE_UP] || kb_state[SDL_SCANCODE_W]) {
        camPos.z -= .2;
    }
    if (kb_state[SDL_SCANCODE_DOWN] || kb_state[SDL_SCANCODE_S]) {
        camPos.z += .2;
    }

    if (kb_state[SDL_SCANCODE_A]) {
        camYaw += 0.2;
    }
    if (kb_state[SDL_SCANCODE_D]) {
        camYaw -= 0.2;
    }

    if (event.type == kx::WinEventType::EXIT) {
        running = false;
    }

    if (event.type == kx::WinEventType::KEYDOWN) {
        if (event.key == "ESCAPE") {
            running = false;
        }
        else if (event.key == "F2") {
            console = !console;
        }


        else {
            terrain.debugUpdateTerrain();
        }
    }

    int mouseX, mouseY;
    SDL_GetMouseState(&mouseX, &mouseY);


    return event;
}

void renderTerrain(const kx::MeshGrid &meshGrid, kx::Camera *cam, kx::Texture *diffuseTexture) {

    kx::RenderData rd;
    rd.vao = meshGrid.getVAO();
    rd.cam = cam;
    rd.diffuseTexture = diffuseTexture;
    rd.prog =  (*getShaderStore())["default2D"];
    rd.world = glm::mat4(1);
    rd.color = {1, 1, 0, 1};
    rd.transparent = false;
    rd.lit = true;
    rd.world = glm::translate(glm::mat4(1), meshGrid.getPosition());

    kx::Renderer renderer;
    renderer.renderTextured(rd);

}
