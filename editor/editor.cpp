//
// Created by mgrus on 09.04.2022.
//

#include "editor.h"
#include "../lib/core/FileUtils.h"
#include "../lib/VertexArrayObject.h"
#include "../lib/VertexBuffer.h"
#include "../lib/Camera.h"
#include "../lib/Program.h"
#include "../lib/Shader.h"
#include "../lib/Renderer.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

void renderQuad(EditorState& editorState);

bool checkExit(const std::vector<kx::WinEvent>& events) {
    auto result = false;
    for (auto e : events) {
         if (e.type == kx::WinEventType::EXIT) {
             result = true;
         } else if(e.type == kx::WinEventType::KEYDOWN && e.key == "ESCAPE") {
             result = true;
         }
    }
    return result;
}

void render(float time, EditorState& editorState) {
    renderQuad(editorState);
}


static kx::VertexArrayObject* createQuadVAO(EditorState& editorState) {
    kx::VertexArrayObject* vao = new kx::VertexArrayObject();
    vao->bind();
    vao->setNumberOfIndices(6);
    std::vector<float> positions = {
            -0.5, -0.5, 0,
            0.5, -0.5, 0,
            0.5, 0.5, 0,
            -0.5, 0.5, 0

    };

    std::vector<float> normals = {
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
    };

    std::vector<float> uvs = {
            0, 0,
            1, 0,
            1, 1,
            0, 1
    };

    std::vector<uint32_t> indices = {
            0,1,2,
            0,2, 3
    };

    auto positionBuffer = new kx::PositionBuffer(positions);
    positionBuffer->enableAttribute(0);
    editorState.positionBuffer = positionBuffer;
    kx::UVBuffer uvBuffer(uvs);
    uvBuffer.enableAttribute(1);
    kx::NormalBuffer normalBuffer(normals);
    normalBuffer.enableAttribute(2);
    kx::IndexBuffer indexBuffer(indices);
    vao->unbind();
    return vao;
}


void renderQuad(EditorState &editorState) {
    auto renderer = kx::Renderer();
    auto qrd = kx::RenderData();
    qrd.prog = editorState.prog;
    qrd.diffuseTexture = nullptr;
    qrd.shadowTexture = nullptr;
    qrd.lightcam = nullptr;
    qrd.cam = editorState.camera;
    qrd.lit = false;
    qrd.color = {1, 0, 0, 1};
    qrd.vao = editorState.quadVAO;
    qrd.primitiveType = GL_TRIANGLES;

    if (editorState.transformState == TransformState::move) {
        float x = (2.0f * editorState.mouseCoords.x) / editorState.screenWidth - 1.0f;
        float y = (2.0f * editorState.mouseCoords.y) / editorState.screenHeight - 1.0f;
        float z = 1.0f;
        glm::vec3 ray_nds = glm::vec3(x, y, z);
        glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
        glm::vec4 ray_eye = glm::inverse(editorState.camera->getProj()) * ray_clip;
        ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
        qrd.world = glm::translate(glm::mat4(1), {ray_eye.x, -ray_eye.y, -5});
    } else {
        qrd.world = glm::translate(glm::mat4(1), {0, 0, -5});
    }




    renderer.renderSingleColor(qrd, nullptr);

}

void doInit(EditorState& editorState, kx::Window& window) {
    auto vsSource = kx::FileUtils::readFile("../assets/shaders/universal_vs.glsl");
    auto fsSource = kx::FileUtils::readFile("../assets/shaders/universal_fs.glsl");
    kx::Shader vshader(vsSource, kx::ShaderType::VERTEX);
    kx::Shader fshader(fsSource, kx::ShaderType::FRAGMENT);
    editorState.prog = new kx::Program(&vshader, &fshader);

    float camWidth = 20;
    float cw = camWidth/2;
    float ar = (float) window.getDimension().w/ (float) window.getDimension().h;
    kx::Camera* orthoCamGameplay = new kx::OrthoCamera({0, 10, 10}, {-cw , cw, -cw /ar, cw / ar, 1, 100});
    orthoCamGameplay->setPosition({0, 0, 10});
    orthoCamGameplay->setLookAt({orthoCamGameplay->getPos().x, 0, orthoCamGameplay->getPos().z -4});
    editorState.camera = orthoCamGameplay;

    editorState.quadVAO = createQuadVAO(editorState);

    editorState.screenWidth = window.getDimension().w;
    editorState.screenHeight = window.getDimension().h;
}

bool keyDown(const std::string& key, std::vector<kx::WinEvent>& events) {
    for (auto e : events) {
        if (e.type == kx::WinEventType::KEYDOWN && e.key == key) {
            return true;
        }
    }
    return false;
}

void editorLoop(float time, kx::Window *window, kx::KeyboardState *keyboardState, kx::MouseState *mouseState,
                std::vector<kx::WinEvent> winEvents, bool& shallStop) {


    static bool inited = false;
    static auto editorState = EditorState {};
    if (!inited) {
        doInit(editorState, *window);
        inited= true;
    }

    // Update
    static float rightEdge= 0.5;
    {
        shallStop = checkExit(winEvents);
        editorState.mouseCoords = mouseState->mouseCoords;
        if (keyDown("g", winEvents)) {
            editorState.transformState = TransformState::move;
        }

        if (keyDown("d", winEvents)) {
            // switchero of the vao! moving the right edge farther out
            rightEdge += 0.05f;
            editorState.quadVAO->bind();
            std::vector<float> newPos = {
                    -0.5, -0.5, 0,
                    rightEdge, -0.5, 0,
                    rightEdge, 0.5, 0,
                    -0.5, 0.5, 0

            };
            editorState.positionBuffer->updateData(newPos);
        }
    }

    window->clearBackbuffer(0.0, 0, 0.1, 1);
    render(time, editorState);
    window->present();

}

