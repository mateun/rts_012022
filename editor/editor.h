//
// Created by mgrus on 09.04.2022.
//

#ifndef KOENIG2022_1_EDITOR_H
#define KOENIG2022_1_EDITOR_H

#include "../lib/Window.h"
#include <vector>

namespace  kx {
    class Camera;
    class Program;
    class VertexArrayObject;
    class PositionBuffer;
}

enum TransformState {
    move,
    scale,
    rotate,
    select
};

enum PrimitiveState {
    vertex,
    edge,
    face
};

struct EditorState {
    int screenWidth;
    int screenHeight;
    kx::VertexArrayObject* quadVAO;
    kx::PositionBuffer* positionBuffer;
    kx::Program* prog = nullptr;
    kx::Camera* camera = nullptr;
    glm::vec2 mouseCoords;
    TransformState transformState = select;
    PrimitiveState primitiveState = face;

};

void editorLoop(float time, kx::Window* window, kx::KeyboardState* keyboardState,
                kx::MouseState* mouseState, std::vector<kx::WinEvent> winEvents, bool& shallStop);

#endif //KOENIG2022_1_EDITOR_H
